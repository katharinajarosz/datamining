'''
errorTable

@author: RaHe
@organization: ADVITOS GmbH

This script contains the DataMining procedures that are Error-oriented (csv files)
(as opposed to the time-oriented 'tdms files')
'''

import pandas as pd
import numpy as np
import os
import datetime

from datetime import timedelta

import sys
sys.path.append("..") # Adds higher directory to python modules path.
    

from FileHandling.DMLoggers import log
import MachineDay
from MainMining.HeaderMining import tryGetProperty
from Util import removeGermanUTF8Chars
from Constants import target_dir_path, source_path
import Datamining


columns_csv = ['index','machine','date','year','month','day','hour',
                   'locationERP','locationMD',
                   'software','software-control','software-host','software-protect',
                   'System_State','error_id','error_code','error_weight'
                   ]

# load the error weights table
try:
    error_weights_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'X:/EvaluationData_LK2001/resources/errorTable_weights.csv')
    error_weights = pd.read_csv(
        error_weights_file,
        index_col='Error Number (Polarion)',
        encoding='ISO-8859-1',
        sep='\t',decimal=',')
except Exception as e:
    error_weights = None
    log.error('errorTable_weights:%s'%e)
    
def get_weight(weight_table,error_number):
    '''
    Looks up the error table and returns the value for 'Gewichtung' column
    that corresponds to the error number
    @param weight_table: the table with the weights in pandas DataFrame format
    @param error_number: the number of the error to lookup
    @return the corresponding weight or 1 if not found or empty
    '''
    try:
        weight = error_weights.loc[error_number,'Gewichtung']
        if weight == weight: # tests for NaN
            return weight
        else:
            return 1.0
    except:
        return 1.0


def createErrorTable(md,limitHours=1,limitMinutes=0):
    '''
    Takes a MachineData object and creates a table for all the errors
    omitting errors that happen within the limitHours and limitMinutes time frame
    @param md: machine data to read
    @param listOfErrors: the table "list of errors" to correspond 'Error-ID'->'Error Number'
    @param limit: time intervall in which the error will be muted after occurring
    @return: pandas DataFrame with the columns below
    '''
    # definitions that happen just once
    global columns_csv, error_weights
    
    # final table
    filtered_errors = pd.DataFrame()

    # holds the errors that have already be added to the final table
    # so they dont get added again for 'timeLimit' time
    muted_errors = {}
    timeLimit = timedelta(hours=limitHours, minutes=limitMinutes)

    # iterate through all rows of active errors
    errorTable = md.CSVdata
    for index, dfRow in errorTable.iterrows():
        try:
            errorNr = int(dfRow['Error Number'])
        except:
            continue
        
        timeStamp = pd.to_datetime(dfRow['Time'], format='%d.%m.%Y %H:%M:%S')

        # if the error not muted
        if errorNr not in muted_errors.keys():
            # mute it
            muted_errors[errorNr] = timeStamp#dfRow.name

            # create a new row in the final table for the error
            new_row = pd.DataFrame(data=np.array([np.zeros(len(columns_csv[1:]))]), columns=columns_csv[1:])

            # fill the data
            new_row['machine'] = md.machine
            new_row['date'] = md.date
            new_row['day'] = pd.to_datetime(md.day,format='%Y_%m_%d',errors='ignore')
            new_row['month'] = md.month
            new_row['year'] = md.year
            new_row['locationERP'] = md.locationERP
            locationMD = removeGermanUTF8Chars(tryGetProperty(md,'Machine location'))
            new_row['locationMD'] = locationMD
            new_row['software'] = md.softwareVersion

            new_row['software-control'] = md.TDMSproperties['Control version string']
            new_row['software-host'] = md.TDMSproperties['Host version string']
            new_row['software-protect'] = md.TDMSproperties['Protect version string']

            new_row['error_id'] = errorNr
            new_row['error_code'] = dfRow['Error ID']
            if error_weights is not None:
                new_row['error_weight'] = get_weight(error_weights,errorNr)
            
            new_row['hour'] = datetime.datetime.strftime(timeStamp,'%H:%M:%S')
            #dfRow.name#str(dfRow.name.hour)+':'+str(dfRow.name.minute)+':'+str(dfRow.name.second)

            new_row['System_State'] = dfRow['Source Module']
            filtered_errors = filtered_errors.append(new_row)

        for mut in list(muted_errors):
            if (timeStamp - muted_errors[mut]) > timeLimit:
                muted_errors.pop(mut)
                
    return filtered_errors

def mineErrorTable(machine_data):
    
    global columns_csv
    
    target_file = os.path.join(target_dir_path,'errorTable.csv')
    
    try:
        file_data = pd.read_csv(target_file,decimal=',',sep='\t',index_col='index')
        if not(np.array_equal(file_data.columns,columns_csv[1:])):
            os.remove(file_data)
        file_data = pd.read_csv(target_file,decimal=',',sep='\t',index_col='index')  
    except Exception as e:
        print(e)
        print("File does not exists or its structure not up to date")
        fo = open(target_file, "a")
        fo.close()
        file_data = pd.DataFrame(data=np.array([np.zeros(len(columns_csv[1:]))]), columns=columns_csv[1:])
        file_data.to_csv(target_file,decimal=',',sep='\t', index_label="index",columns=columns_csv[1:])
        file_data = pd.read_csv(target_file,decimal=',',sep='\t',index_col='index')
        file_data.drop(file_data.index[0],inplace=True)
        file_data.to_csv(target_file,decimal=',',sep='\t', index_label="index",columns=columns_csv[1:])
    try:
        new_Data = createErrorTable(machine_data)
    except Exception as e:
        log.error('mineErrorTable: %s'%e)
    
    file_data = file_data.append(new_Data)
    
    file_data.to_csv(target_file,decimal=',',sep='\t',index_label="index",columns=columns_csv[1:])
    fo = open(target_file, "a")
    fo.close()
    
def mine_all(source_path,target_dir_path):
        ## @cond INTERNAL

    log.info('Start errorTable mine_all. Src:%s Target:%s'%(source_path,target_dir_path) )
    
    # for refference
    script_init_time = datetime.datetime.now()
    
    # get the machine list
    machine_list = sorted([m for m in os.listdir(source_path) if Datamining.is_valid_machine(source_path,m)])

    log.info('%d Valid machines found'%len(machine_list))
    
    print('Calculate to total number of folders...')

    # calculate total number of folders
    total_folders = 0
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if os.path.isdir(os.path.join(*[source_path,machine,d]))])
        total_folders += len(date_list)

    # analyse the folders
    analyzed_folder = 0
    file_data = pd.DataFrame(columns=columns_csv[1:])
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if os.path.isdir(os.path.join(*[source_path,machine,d]))])
        for date in date_list:
            analyzed_folder +=1
            log.info('Analyse %s %s. %d of %d (%d%%)'%(machine,date,analyzed_folder,total_folders,100*analyzed_folder/total_folders))
            
            # prevision of the end of the run
            log.info("ETA: %s"%datetime.datetime.strftime(
                (script_init_time + (total_folders/analyzed_folder)*(datetime.datetime.now()-script_init_time) )
                ,format='%Y-%m-%d %H:%M:%S'))
            
            # - - - - - - - - - - - - - - - - -
            #        MINING SCRIPTS
            # - - - - - - - - - - - - - - - - -
            
            try:
                md = MachineDay.load_MD(source_path,machine,date,load_tdms=False)
                if not md:
                    log.error('FALSE MD, NOT LOADED %s %s'%(machine,date))
                    continue
    
                # Handle errorTable.csv (report)
                for mine in [createErrorTable]:
                    log.debug('Mine: '+mine.__name__)
                    new_Data = mine(md)
                    file_data = file_data.append(new_Data)

            except Exception as e:
                log.error('MD NOT LOADED %s %s %s'%(machine,date,e))
                        
            
            
        log.info('Datamining Script finished on machine %s', machine)
    log.info('Finish ErrorTable, saving...')
    target_file = os.path.join(target_dir_path,'errorTable.csv')
    file_data.to_csv(target_file,decimal=',',sep='\t',index_label="index",columns=columns_csv[1:])
    log.info('Finish All')

if __name__ == '__main__':
    mine_all(source_path,os.path.join(target_dir_path,'autorun'))