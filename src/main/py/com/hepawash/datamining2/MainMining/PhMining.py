"""
@Author: Juan Fernandez

"""
import os
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import multiprocessing

import MachineDay as MachineData
from FileHandling.DMLoggers import log

def getStatsErrorPhReservoir(machine_data):
    """
    Returns the average and the std of the difference between goal pH and real pH in Reservoir
    """
    if not machine_data.time_in_state(4):
        return {'mean': 0, 'std': 0}
    actual_res_ph = machine_data.TDMSdata['C_PH_RES_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]
    goal_res_ph = machine_data.TDMSdata['CL_PH_RES_DEM'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]
    arr_ph_res_bypass_pcon = actual_res_ph - goal_res_ph
    
    return {'mean': round(arr_ph_res_bypass_pcon.mean(),2), 'std': round(arr_ph_res_bypass_pcon.std(),2)}

def get_Disinf_Drifts(tdmsData):
    ## Gets 'Disinfection drifts', i.e. std deviation of pH in:
    # Disinfection, substate 1 or 3, active subsequence 4
    # @return Acid,Base,Mon,Res drifts in that order
    try:
        channels = ['C_PH_ACID_CD','C_PH_BASE_CD','C_PH_MON_CD','C_PH_RES_CD']
        
        filtered = tdmsData[channels][
        (tdmsData['System_State'] == 6) &
        ((tdmsData['Disinfection_State'] == 1) | (tdmsData['Disinfection_State'] == 3) ) &
        (tdmsData['Active Subsequence'] == 4)]
        
        if not filtered.empty:
            return filtered.std()
        else:
            return 0,0,0,0
    except Exception as e:
        print(e)
        return 0,0,0,0

def getStatsGoalPhReservoir(machine_data):
    """
    Returns the average and the std of the goal pH in Reservoir
    """
    if not machine_data.time_in_state(4):
        return {'mean': 0, 'std': 0}
    goal_res_ph = machine_data.TDMSdata['CL_PH_RES_DEM'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]
    return {'mean': round(goal_res_ph.mean(),2), 'std': round(goal_res_ph.std(),2)}

def getStatsPhReservoir(machine_data):
    """
    Returns the average and the std of the real pH in Reservoir
    """
    if not machine_data.time_in_state(4):
        return {'mean': 0, 'std': 0}
    actual_res_ph = machine_data.TDMSdata['C_PH_RES_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]
    return {'mean': round(actual_res_ph.mean(),2), 'std': round(actual_res_ph.std(),2)}

def getStartingPHReservoir(md):
    '''
    Returns the mean ph value during treatment substate 0
    '''
    if not md.time_in_state(4):
        return {'mean': 0, 'std': 0}
    
    try:
        mean = md.TDMSdata['C_PH_RES_CD'][((md.TDMSdata['System_State'] == 4) & (md.TDMSdata['Treatment_State']==0))].mean()
        std = md.TDMSdata['C_PH_RES_CD'][((md.TDMSdata['System_State'] == 4) & (md.TDMSdata['Treatment_State']==0))].std()
        if (float(mean) > 0):
            return {'mean': mean, 'std': std}
    except Exception as e:
        print(e)
    return {'mean': 0, 'std': 0}

def saveTreatmentPhGraph(machine_data,target_dir_path):
    """
    NOT WORKING - Save the Graph of the ph during treatment in a jpg image
    """
    fig = plt.figure()
    ax = fig.gca()
    #fig, ax = plt.subplots(1, 1)
    t = np.arange(0,len(machine_data.TDMSdata['C_PH_RES_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]),1)
    y_mean = round(machine_data.TDMSdata['C_PH_RES_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)].mean(),2)
    y_std = round(machine_data.TDMSdata['C_PH_RES_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)].std(),2)
    
    ax.plot(t,machine_data.TDMSdata['C_PH_RES_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)],color='green')
    ax.plot(t,machine_data.TDMSdata['C_PH_BASE_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)],color='blue')
    ax.plot(t,machine_data.TDMSdata['C_PH_ACID_CD'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)],color='red')
    ax.plot(t,machine_data.TDMSdata['CL_PH_RES_DEM'][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)],color='black')
    ax.set_xlabel('Time')
    ax.set_ylabel('pH')
    ax.set_ylim(0,12)
    ax.axhline(y=y_mean, linewidth=2, color='b',ls='--')
    ax.axhline(y=y_mean+y_std, linewidth=2, color='r',ls='--')
    ax.axhline(y=y_mean-y_std, linewidth=2, color='r',ls='--')
    ax.set_yticks(np.arange(0, 12.5,0.5))
    ax.yaxis.grid(which="major", color='black', linestyle='-', linewidth=0.5)
    
    plt.savefig(target_dir_path+'\\images\\ph_control\\'+machine_data.date+machine_data.machine+'_ph_control')
    #plt.clf()
    plt.close(fig)
    
    return True

def savePhTreatmentData(machine_data,target_dir_path):
    """
    Save a CSV File with the treatment information of the sensors
    Note: this function is used to save data that will be analyzed after with CrossSwitchEvaluation
    the whole implementation should be done here avoiding saving the data See JIRA SDM-50
    """
    target_dir_path_data = target_dir_path + '/data/ph_control/'
    if not(os.path.isdir(target_dir_path_data)):
        if not(os.path.isdir(target_dir_path + '/data/')):
            os.mkdir(target_dir_path + '/data/')
            os.mkdir(target_dir_path + '/data/ph_control/')
        else:
            os.mkdir(target_dir_path + '/data/ph_control/')
    
    #df = machine_data.TDMSdata[(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]
    df = machine_data.TDMSdata[(machine_data.TDMSdata['System_State'] == 4)]
    
    df = df[['C_PH_RES_CD','C_PH_BASE_CD','C_PH_ACID_CD','C_PH_MON_CD',
             'C_PH_RES_R','C_PH_BASE_R','C_PH_ACID_R','C_PH_MON_R',
             
             'CL_PH_RES_DEM','CL_SUPPLY_REL_DEM','CL_SUPPLY_FLOW_E',
             'CL_BALANCE_PP_WASTE_Y','CL_BALANCE_PP_WASTE_E',
             
             'S_F_HW_ACID_CD','S_F_HW_BASE_CD',
             'S_GP_HW_ACID_FB_CD','S_GP_HW_BASE_FB_CD',
             
             'S_P_HW_ACID_CD','S_P_HW_BASE_CD','S_P_DIA_OUT_CD','S_P_DIA_IN_CD',
             
             'S_P_BLOOD_ART_CD','S_P_BLOOD_PRE_CD','S_P_BLOOD_VENOUS_CD',
             
             'BLOOD_PUMP.Meas','CL_FLOW_BLOOD_PUMP_DEM','S_BLOOD_PUMP_FB_CD',
             
             
             'A_V3_CROSS_DEM', 'A_V3_AIRSWITCH_DEM',
             
             'Sond_Acid.UTemp','Sond_Base.UTemp','Sond_Reservoir.UTemp',
             
             'Treatment_State','Disinfection_State','Active Subsequence','Subsequence_State']]

    if not(df.empty):
        df.to_csv(target_dir_path_data+
                  machine_data.date+'_'+
                  machine_data.machine+'_'+
                  machine_data.location+'_'+
                  machine_data.software+
                  '.csv',',')

def getSensorErrors(machine_data,sensorChannel):
    """
    Return the amount of errors values sent by pH Sensors
    The error values are defined when the value of the temperature = -1 and it is not in service state
    """
    data = machine_data.TDMSdata
    return float(data[sensorChannel][(data[sensorChannel] == -1) & (data['System_State'] != 2)].size)

def getSensorSN(machine_data,phSensor):
    """
    Return the serial number of the pH sondes read from the PHV file of the folder machine/date
    """
    
    serial_numbers = {}
    for file in os.listdir('/'.join([machine_data.source_path,machine_data.machine,machine_data.date])):        
        if file[-3:] == 'phv':
            try:
                ph_data = pd.read_csv('/'.join([machine_data.source_path,machine_data.machine,machine_data.date,file]),'\t')
                serial_numbers['acid'] = str(ph_data['Serial Number'][0])  
                serial_numbers['base'] = str(ph_data['Serial Number'][1]) 
                serial_numbers['monitor'] = str(ph_data['Serial Number'][2]) 
                serial_numbers['reservoir'] = str(ph_data['Serial Number'][3])
            except:
                serial_numbers = {}
            break

    if not(serial_numbers):
        return 0
    else:
        return serial_numbers[phSensor]
    
    return 22222

def getLengthMakeAlkalineState(machine_data,substate=-1):
    """
    Return the total amount of values (seconds) where machine is in MakeAlkalineState or MakeAlaklineState and concrete substate
    """
    if not machine_data.time_in_state(4):
        return 0
    
    if substate > 0:
        stateLength = machine_data.TDMSdata['Active Subsequence'][(machine_data.TDMSdata['Active Subsequence'] == 39) &
                                        (machine_data.TDMSdata['Subsequence_State'] == substate)
                                        ].size
    else:
        stateLength = machine_data.TDMSdata['Active Subsequence'][machine_data.TDMSdata['Active Subsequence'] == 39].size
    
    return stateLength


def getPhMakeAlkalineState(machine_data,substate=-1):
    """
    Return the average of the average ph where machine is in MakeAlkalineState or MakeAlaklineState and concrete substate
    """
    if not machine_data.time_in_state(4):
        return 0
    
    machine_data.TDMSdata['C_PH_MEAN_CD'] = (machine_data.TDMSdata['C_PH_ACID_CD'] + 
                                         machine_data.TDMSdata['C_PH_BASE_CD'] + 
                                         machine_data.TDMSdata['C_PH_RES_CD']) / 3
    
    if substate > 0:
        statePh = machine_data.TDMSdata['C_PH_MEAN_CD'][(machine_data.TDMSdata['Active Subsequence'] == 39) &
                                        (machine_data.TDMSdata['Subsequence_State'] == substate)
                                        ].mean()
    else:
        statePh = machine_data.TDMSdata['C_PH_MEAN_CD'][machine_data.TDMSdata['Active Subsequence'] == 39].mean()
    
    return statePh

def getWaterAmountMakeAlkalineState(machine_data):
    """
    Return the total amount of water pump in the machine where machine is in MakeAlkalineState 
    """
    if not machine_data.time_in_state(4):
        return 0
    amountWater = machine_data.TDMSdata['S_F_RO_WATER_CD'][(machine_data.TDMSdata['Active Subsequence'] == 39)].sum()/60                                 
    return amountWater
    
def minePh(machine_data):
    

    new_row = pd.DataFrame(index=[machine_data.machine+' '+machine_data.date] )
    try:
        if machine_data.analyzed:
            new_row['analyzed'] = 1
    
            new_row['acid_sensor_errors'] = getSensorErrors(machine_data,'Sond_Acid.UTemp')
            new_row['base_sensor_errors'] = getSensorErrors(machine_data,'Sond_Base.UTemp')
            new_row['reservoir_sensor_errors'] = getSensorErrors(machine_data,'Sond_Reservoir.UTemp')
            new_row['monitor_sensor_errors'] = getSensorErrors(machine_data,'Sond_Monitor.UTemp')
            
            new_row['acid_sensor_sn'] = getSensorSN(machine_data,'acid')
            new_row['base_sensor_sn'] = getSensorSN(machine_data,'base')
            new_row['reservoir_sensor_sn'] = getSensorSN(machine_data,'monitor')
            new_row['monitor_sensor_sn'] = getSensorSN(machine_data,'reservoir')
            
            drift_a,drift_b,drift_m,drift_r = get_Disinf_Drifts(machine_data.TDMSdata)
            new_row['acid_dis_drift'] = drift_a
            new_row['base_dis_drift'] = drift_b
            new_row['monitor_dis_drift'] = drift_m
            new_row['reservoir_dis_drift'] = drift_r
           
            new_row['error_ph_res_treatment_mean'] = getStatsErrorPhReservoir(machine_data)['mean']
            new_row['error_ph_res_treatment_std'] = getStatsErrorPhReservoir(machine_data)['std']
            
            new_row['goal_ph_res_treatment_mean'] = getStatsGoalPhReservoir(machine_data)['mean']
            new_row['goal_ph_res_treatment_std'] = getStatsGoalPhReservoir(machine_data)['std']
            
            new_row['ph_res_treatment_mean'] = getStatsPhReservoir(machine_data)['mean']
            new_row['ph_res_treatment_std'] = getStatsPhReservoir(machine_data)['std']
            
            new_row['ph_res_treatment_start_mean'] = getStartingPHReservoir(machine_data)['mean']
            new_row['ph_res_treatment_start_std'] = getStartingPHReservoir(machine_data)['std']
            
            new_row['make_alkaline_state_total_length'] = getLengthMakeAlkalineState(machine_data)
            new_row['make_alkaline_state_step3_length'] = getLengthMakeAlkalineState(machine_data,substate=2)
            new_row['make_alkaline_state_step3_ph'] = getPhMakeAlkalineState(machine_data,substate=2)
            new_row['make_alkaline_state_amount_water'] = getWaterAmountMakeAlkalineState(machine_data)
    
        else:
            new_row['analyzed'] = 0
    except Exception as e:
        log.error('minePh',exc_info=True)
        
    return new_row

if __name__ == '__main__':
         
    target_dir_path = '../../../../../../target/'
    source_path = 'X:/LogData_LK2001'
    
    machines = ['LK2001-043']
    
    for machine in machines:
        analyzed_folder = 0
        ls_date = os.listdir(source_path+'\\'+machine+'\\')
        for date in [date for date in ls_date if '2018_08' in date]:
            analyzed_folder = analyzed_folder + 1
            print(machine+' '+date+' Percentage of Machine elapsed: '+str(int(100*analyzed_folder/len(ls_date)))+'%')
            machine_data = MachineData.machineData(source_path,machine,date)
            machine_data.debug = True  
            machine_data.readData()
            
            minePh(machine_data,target_dir_path)
            
    print("PhMining.py Finished")
  