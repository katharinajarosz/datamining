'''

This files mines the properties from the tdms header and ERP system

author: rahe

'''

import os
import sys
import pandas as pd
import numpy as np
from datetime import datetime as dt

filename = 'header_data.csv'
from FileHandling.DMLoggers import log
from ExternalConnections.FileDownload import download_file

sys.path.append('..')
from Util import removeGermanUTF8Chars, getsize_filelist
from Constants import rawData_path, filepath_ErpConnection
from Util import getsize_filelist

#%% init

ErpFile = download_file(filepath_ErpConnection, overwrite=True)
Erp_SvcRepairs = pd.read_excel(ErpFile, sheet_name='svc_repairs',index_col=0)
Erp_mReallocations = pd.read_excel(ErpFile, sheet_name='mReallocations',index_col=0)
Erp_locations = pd.read_excel(ErpFile, sheet_name='locations',index_col=0)

#%% state variable

# this stores the rawfiles for the current machine so that i
rawfiles_list = pd.Series()

#%%
def getVersionfromFileName(md):
    '''
       Get Software Version from the TDMS File String (The Host Software Version)
    '''
    sw_ls = []
    software_version = ''
    for file in [f for f in sorted(os.listdir(md.folder)) if f.endswith('.tdms')]:
        try:
            txt = file.split('LK2001V')[-1][0:7]
            sw_ls.append(txt)
        except:
            sw_ls = ''       
    try:
        software_version = sw_ls[-1]
    except:
        pass
    return software_version

def tryGetProperty(md,property_):
    ## Wrapper that avoids crashing if property is not found
    p = ''
    try:
        p = md.TDMSproperties[property_]
    except Exception:
        log.warning('property not found ({})'.format(property_))
    return p

def eval_MDfolder(folder_path):
    ## Evaluates the size of the files and subfolder in a MD folder
    #
    # @param: absolute path to folder
    # @return total_files, total_subfolder: size (bytes) of the files directly
    #  in the folder and size of files in subfolders 
    #
    # @detail: just analyzes folder and subfolders (not sub-sub-folders)
    
    total_files = 0
    total_subfolder = 0
    
    for sub_path in os.listdir(folder_path):
        abs_subpath = os.path.join(folder_path,sub_path)
        if os.path.isfile(abs_subpath):
            ## eval file
            total_files += os.path.getsize(abs_subpath)
        else:
            ## eval subfolder
            for file_in_subfolder in os.listdir(abs_subpath):
                file_abspath = os.path.join(abs_subpath, file_in_subfolder)
                if os.path.isfile(file_abspath):
                    total_subfolder += os.path.getsize(file_abspath)
        
    return total_files, total_subfolder
    
def eval_rawData_size(machine,date):
    ## Evaluates size of the raw data for a specific machine-date
    #
    # machine: in format LK2001-XXX
    # date: YYYY_MM_DD
    global rawfiles_list
    
    if (rawfiles_list.name != machine):
        rawfiles_list = pd.Series(os.listdir(os.path.join(rawData_path,machine.split('-')[1])))
        rawfiles_list.name = machine
    
    rawFiles = [os.path.join(rawData_path,machine.split('-')[1],f) for f in rawfiles_list if date in f]
    return getsize_filelist(rawFiles)
    

def getClinicID(self,*nameslist): # nameslist = 'Klinikum rechts der Isar, Ismaninger Str. 22, 81675 Muenchen','Klinikum rechts der Isar TUM'
    ## Returns the ID corresponding to one of the passed names
    #
    # Gets a list of names in the parameters and tryies to find the
    # correspondent ID. Returns the last one found otherwise NaN
    found_ids = []
    for name in nameslist: # name ='Universitaetsklinikum Schleswig-Holstein'; self= erpInterface
        # remove comma if present, lowercase
        if ',' in name:
            location = name.split(',')[0]
        else:
            location = name
        location = location.lower()
        names = Erp_locations['company'].str.lower()
        
        # find the ids, if not possible, try removing the german characters
        # and find again
        rest = names[names == location]
        if not rest.empty:
            found_ids.append(rest.index[-1])
        else:
            names = names.apply(removeGermanUTF8Chars)
            rest = names[names == removeGermanUTF8Chars(location)]
            if not rest.empty:
                found_ids.append(rest.index[-1])
    
    if len(found_ids) >0  :
        return found_ids[-1].split('.')[0] if '.' in found_ids[-1] else found_ids[-1]
    else:
        return float('nan')

#%% mineHeader
def mineHeader(md):
    '''
    Gathers the information from tdms header and ERP system
    '''
    columns_csv = ['index','analyzed',
                   'machine',
                   'date', 'day', 'month', 'year',
                   'locationMD','locationERP','locationID',
                   'filename_version','host_version','control_version','protect_version','parameter_file']
        
    new_row = pd.DataFrame(
        data=np.array([np.zeros(len(columns_csv[1:]))]),
        columns=columns_csv[1:],
        index=[md.machine+' '+md.date] )
    
    try:
        new_row['analyzed'] = len(md.analyzed)
        new_row['machine'] = md.machine
        new_row['date'] = dt.strptime(md.date,'%Y_%m_%d')
        new_row['day'] = md.day
        new_row['month'] = md.month
        new_row['year'] = md.year
        locationMD = removeGermanUTF8Chars(tryGetProperty(md,'Machine location'))
        new_row['locationMD'] = locationMD
        new_row['locationERP'] = md.locationERP 
        new_row['locationID'] = getClinicID(locationMD,md.locationERP)
        new_row['filename_version'] = md.softwareVersion
        new_row['host_version'] = tryGetProperty(md,'Host version string')
        new_row['control_version'] = tryGetProperty(md,'Control version string')
        new_row['protect_version'] = tryGetProperty(md,'Protect version string')
        new_row['parameter_file'] = tryGetProperty(md,'Parameter file name')
        
        #file sizes
        size_files, size_subfolders = eval_MDfolder(md.folder)
        size_rawData = eval_rawData_size(md.machine, md.date)
        
        new_row['size_files'] = size_files
        new_row['size_subfolders'] = size_subfolders
        new_row['size_rawData'] = size_rawData        
        
    except Exception as e:
        log.error('mineHeader: %s'%e,exc_info=True)
    
    return new_row