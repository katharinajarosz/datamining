"""
@author: Juan Fernandez
@organization: ADVITOS GmbH
Script to extract time information of the data files of the LK2001
"""
import os
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from FileHandling.DMLoggers import log

import MachineDay as MachineData

def calculateThreshold(df):
    """
    Calculates the threshold in the timestamp channel of the datafile
    Function will be used to calculate the time
    """
    if df.index.size > 4:
        try:
            delta = []
            delta.append((df.index[1] - df.index[0]).total_seconds())
            delta.append((df.index[2] - df.index[1]).total_seconds())
            #The Threshold is the average of the difference between timestamps of the 2 first values
            threshold = (sum(delta)/len(delta))*1.25
        except Exception as e:
            print('Error %s'%e)
            threshold = 1.5 #default
    else:
        threshold = 1.5 #default
    return threshold

def getTotalTime(machine_data):
    """
    Returns the total amounts of seconds on machine_data.TDMSdata DataFrame
    """
    df = machine_data.TDMSdata
    threshold_time_jump = calculateThreshold(df)
    
    if df.size > 0:
        df['epoch'] = (df.index.astype(np.int64) // 10**7)/100
        diff = (df['epoch']-df['epoch'].iloc[0]).diff()
        delta = diff[diff<threshold_time_jump].sum()
        df.drop('epoch',inplace=True,axis=1)
        if abs(delta-df['System_State'].size)>500:
            #Error with Timestampcalculation - Maybe Host Time changed
            return df['System_State'].size
        else:
            return delta
    else:
        return 0
    

def getStateTime(machine_data,state):
    """
    Returns the total amounts of seconds in a define state state on machine_data.TDMSdata DataFrame
    """
    df = machine_data.TDMSdata
    threshold_time_jump = calculateThreshold(df)
    
    if df[df['System_State'] == state].size > 0:
        df['epoch'] = (df.index.astype(np.int64) // 10**7)/100
        df_state = df[df['System_State'] == state]
        diff = (df_state['epoch']-df_state['epoch'].iloc[0]).diff()
        delta = diff[diff<threshold_time_jump].sum()
        df.drop('epoch',inplace=True,axis=1)
        if abs(delta-df_state['System_State'].size)>500:
            #Error with Timestampcalculation - Maybe Host Time changed
            return df_state['System_State'].size
        else:
            return delta
    else:
        return 0

def getTimeOverTemperature(machine_data,temperature):
    """
    Returns the total amounts of seconds where reservoir temperature is above a define temperature on machine_data.TDMSdata DataFrame
    """
    df = machine_data.TDMSdata[machine_data.TDMSdata['Sond_Reservoir.UTemp'] >= temperature*100]
    threshold_time_jump = calculateThreshold(df)

    if df.size > 0:
        machine_data.TDMSdata['epoch'] = (machine_data.TDMSdata.index.astype(np.int64) // 10**7)/100
        df = machine_data.TDMSdata[machine_data.TDMSdata['Sond_Reservoir.UTemp'] >= temperature*100]
        diff = (df['epoch']-df['epoch'].iloc[0]).diff()
        delta = diff[diff<threshold_time_jump].sum()
        machine_data.TDMSdata.drop('epoch',inplace=True,axis=1)
        
        if abs(delta-df['System_State'].size)>500:
            #Error with Timestampcalculation - Maybe Host Time changed
            return df['System_State'].size
        else:
            return delta
    else:
        return 0
 
def getTimeBetweenPhRange(machine_data,ph_range):
    """
    Returns the total amounts of seconds where the configure ph is between a defined ph range on machine_data.TDMSdata DataFrame
    """
    df = machine_data.TDMSdata[(machine_data.TDMSdata['CL_PH_RES_DEM'] >= ph_range[0]) &
                           (machine_data.TDMSdata['CL_PH_RES_DEM'] < ph_range[1]) &
                           (machine_data.TDMSdata['Treatment_State'] == 6)]
    threshold_time_jump = calculateThreshold(df)

    if df.size > 0:
        machine_data.TDMSdata['epoch'] = (machine_data.TDMSdata.index.astype(np.int64) // 10**7)/100
        df = machine_data.TDMSdata[(machine_data.TDMSdata['CL_PH_RES_DEM'] >= ph_range[0]) &
                           (machine_data.TDMSdata['CL_PH_RES_DEM'] < ph_range[1]) &
                           (machine_data.TDMSdata['Treatment_State'] == 6)]
        diff = (df['epoch']-df['epoch'].iloc[0]).diff()
        delta = diff[diff<threshold_time_jump].sum()
        machine_data.TDMSdata.drop('epoch',inplace=True,axis=1)
        
        if abs(delta-df['System_State'].size)>500:
            #Error with Timestampcalculation - Maybe Host Time changed
            return df['System_State'].size
        else:
            return delta
    else:
        return 0
    
def getTimeAtConcentrateFlow(machine_data,concentrate_flow):
    """
    Returns the total amounts of seconds where the configured concentrate flow is equal to a defined concentrate flow on machine_data.TDMSdata DataFrame
    """
    df = machine_data.TDMSdata[(machine_data.TDMSdata['CL_SUPPLY_FLOW_E'] == concentrate_flow) &
                           (machine_data.TDMSdata['Treatment_State'] == 6)]
    threshold_time_jump = calculateThreshold(df)

    if df.size > 0:
        machine_data.TDMSdata['epoch'] = (machine_data.TDMSdata.index.astype(np.int64) // 10**7)/100
        df = machine_data.TDMSdata[(machine_data.TDMSdata['CL_SUPPLY_FLOW_E'] == concentrate_flow) &
                           (machine_data.TDMSdata['Treatment_State'] == 6)]
        diff = (df['epoch']-df['epoch'].iloc[0]).diff()
        delta = diff[diff<threshold_time_jump].sum()
        machine_data.TDMSdata.drop('epoch',inplace=True,axis=1)
        
        if abs(delta-df['System_State'].size)>500:
            #Error with Timestampcalculation - Maybe Host Time changed
            return df['System_State'].size
        else:
            return delta
    else:
        return 0
       
def getNegativeTimeInState(machine_day,chan,state):
    ## Returns the time in which the channel has a negative value
    return machine_day.TDMSdata[chan][
        (machine_day.TDMSdata[chan] < 0) & (machine_day.TDMSdata['System_State'] == state)
        ].count()

def getDisinfectionSubStTime(machine_data,state):
    """
    Returns 
    """
    df = machine_data.TDMSdata
    threshold_time_jump = calculateThreshold(df)
    
    if df[df['Disinfection_State'] == state].size > 0:
        df['epoch'] = (df.index.astype(np.int64) // 10**7)/100
        df_state = df[df['Disinfection_State'] == state]
        diff = (df_state['epoch']-df_state['epoch'].iloc[0]).diff()
        delta = diff[diff<threshold_time_jump].sum()
        df.drop('epoch',inplace=True,axis=1)
        if abs(delta-df_state['Disinfection_State'].size)>500:
            #Error with Timestampcalculation - Maybe Host Time changed
            return df_state['Disinfection_State'].size
        else:
            return delta
    else:
        return 0
    
#%%
    
def mineTime(machine_data):
    """
    Calculate the time information of the machine_data.TDMSdata Dataframe and save it in a CSV file
    """
    
    new_row = pd.DataFrame(index=[machine_data.machine+' '+machine_data.date] )
    try:
        if machine_data.analyzed:
            new_row['total_time'] = getTotalTime(machine_data)
            
            new_row['startup_time'] = getStateTime(machine_data,0)
            new_row['idle_time'] = getStateTime(machine_data,1)
            new_row['service_time'] = getStateTime(machine_data,2)
            new_row['preparation_time'] = getStateTime(machine_data,3)
            new_row['treatment_time'] = getStateTime(machine_data,4)
            new_row['postprocess_time'] = getStateTime(machine_data,5)
            new_row['disinfection_time'] = getStateTime(machine_data,6)
            
            new_row['dis_citrichot_time'] = getDisinfectionSubStTime(machine_data,1)
            new_row['dis_repeatcitric_time'] = getDisinfectionSubStTime(machine_data,2)
            new_row['dis_standard_time'] = getDisinfectionSubStTime(machine_data,3)
            new_row['sporotal_time'] = getDisinfectionSubStTime(machine_data,4)
            
            new_row['time_over_temperature_60'] = getTimeOverTemperature(machine_data,60)
            new_row['time_over_temperature_80'] = getTimeOverTemperature(machine_data,80)
            
            new_row['time_at_cf_320'] = getTimeAtConcentrateFlow(machine_data,320)
            new_row['time_at_cf_220'] = getTimeAtConcentrateFlow(machine_data,220)
            new_row['time_at_cf_160'] = getTimeAtConcentrateFlow(machine_data,160)
            
            new_row['time_goal_ph_res_00_70'] = getTimeBetweenPhRange(machine_data,[0,7])
            new_row['time_goal_ph_res_70_80'] = getTimeBetweenPhRange(machine_data,[7,8])
            new_row['time_goal_ph_res_80_85'] = getTimeBetweenPhRange(machine_data,[8,8.5])
            new_row['time_goal_ph_res_85_90'] = getTimeBetweenPhRange(machine_data,[8.5,9])
            new_row['time_goal_ph_res_90_140'] = getTimeBetweenPhRange(machine_data,[9,14])
            
            # 'time_neg_HWA','time_neg_HWB','time_neg_DIAIN','time_neg_DIAOUT',
            #startup
            new_row['time_startup_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',0)
            new_row['time_startup_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',0)
            new_row['time_startup_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',0)
            new_row['time_startup_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',0)

            # idle
            new_row['time_idle_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',1)
            new_row['time_idle_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',1)
            new_row['time_idle_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',1)
            new_row['time_idle_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',1)
            
            # service
            new_row['time_service_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',2)
            new_row['time_service_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',2)
            new_row['time_service_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',2)
            new_row['time_service_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',2)

            # preparation
            new_row['time_preparation_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',3)
            new_row['time_preparation_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',3)
            new_row['time_preparation_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',3)
            new_row['time_preparation_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',3)

            # treatment
            new_row['time_treatment_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',4)
            new_row['time_treatment_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',4)
            new_row['time_treatment_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',4)
            new_row['time_treatment_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',4)
            
            # postprocess
            new_row['time_postprocess_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',5)
            new_row['time_postprocess_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',5)
            new_row['time_postprocess_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',5)
            new_row['time_postprocess_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',5)
            
            # disinfection
            new_row['time_disinfection_neg_HW_A'] = getNegativeTimeInState(machine_data,'S_P_HW_ACID_CD',6)
            new_row['time_disinfection_neg_HW_B'] = getNegativeTimeInState(machine_data,'S_P_HW_BASE_CD',6)
            new_row['time_disinfection_neg_DIA_IN'] = getNegativeTimeInState(machine_data,'S_P_DIA_IN_CD',6)
            new_row['time_disinfection_neg_DIA_OUT'] = getNegativeTimeInState(machine_data,'S_P_DIA_OUT_CD',6)
                        
        else:
            new_row['analyzed'] = 0
    except Exception as e:
        log.error('mineTime: %s'%e)
    
    return new_row

if __name__ == '__main__':
         
    target_dir_path = '../../../../../../../target/'
    source_path = 'X:/LogData_LK2001'
    
   # machines = ['LK2001-021', 'LK2001-033']
    timeMining = pd.DataFrame()
    
    for machine in [m for m in os.listdir(source_path) if 'LK2001' in m]:
        analyzed_folder = 0
        ls_date = os.listdir(source_path+'\\'+machine+'\\')
        for date in [d for d in ls_date if '2019' in d]:
            #analyzed_folder = analyzed_folder + 1
            #print(machine+' '+date+' Percentage of Machine elapsed: '+str(int(100*analyzed_folder/len(ls_date)))+'%')
            
            #machine_data.debug = True  
            #machine_data.readData()
            try:
                machine_data = MachineData.load_MD(source_path,machine,date)
                timeMining = timeMining.append(mineTime(machine_data))
            except:
                pass
            
    print("TimeMining.py Finished")
    timeMining.to_excel("timeMining.xlsx")
  