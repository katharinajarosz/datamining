"""
@Author: Juan Fernandez

"""
import os, sys
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

sys.path.append('..')
import MachineDay as MachineData
from FileHandling.DMLoggers import log

from Constants import source_path, filepath_AdvosBIConnection
from ExternalConnections.FileDownload import download_file

#%% init

AdvosBIFile = download_file(filepath_AdvosBIConnection)
AdvosBI_listOfSystemErrors = pd.read_excel(AdvosBIFile,sheet_name="list_of_system_errors", index_col=0)
AdvosBI_keyword_description = pd.read_excel(AdvosBIFile,sheet_name="keyword_description", index_col=0)

#%%
def getListOfFatalErrors():
    """
    Return a List with the Fatal Errors taken from the List of System Errors file
    Fatal Errors are the errors marked with treatment abortion as JA
    """
    # TODO this will be implemented in the database
    return AdvosBI_listOfSystemErrors\
        [AdvosBI_listOfSystemErrors['abortion'] == 1].index.tolist()
    
    #error_list_file = '../../../../resources/LK2001_List_Of_System_Errors.csv'
    #try:
    #    error_file = pd.read_csv(error_list_file,',',encoding = "ISO-8859-1")
    #    return error_file['Error Number'][error_file['abortion'] == 'Ja'].tolist()
    #except:
    #    print('No List of System Errors found!')
    #    return pd.DataFrame()
    

def split_errors(row,value):
        try:
            if (row.find(value) != -1):
                return 1
            else:
                return -1
        except:
            return 'Error'

def evaluate_number_of_errors(data,state,fatal_errors=[111111]):
    active_errors = []
    
    #Select Only the Errors in the desired state
    if state == 99:
        errors_in_state = data['ActiveErrors (Strings)']
    else:
        errors_in_state = data['ActiveErrors (Strings)'][data['System_State'] == state]
    #Generate the list of active errors during the state
    for item in errors_in_state.unique():
        for error_num in item.split(','):
            if (error_num != ''):
                if (int(error_num) not in active_errors):
                    active_errors.append(int(error_num.strip()))             
    #Count the number of times that an error happend
    total_errors=0
    for item in active_errors:
        error_step = errors_in_state.apply(split_errors,args=(str(item),))        
        old_value = -1
        amount_errors = 0
        for value in error_step.iteritems():
            if (old_value != value[1]): #Value Changed
                if (value[1] == 1):
                    amount_errors = amount_errors + 1
                old_value = value[1]     
        total_errors = total_errors + amount_errors
    
    #Evaluate if there was an abortion and the cause
    if state==4: #Evaluate only in Treatment State
        last_active_errors=[]
        try: 
            last_data = data[-1800:] #last 30 Minutes
        except:
            last_data = data[int(3*len(data.index)/4):] #last 25% of the treatment
        #Select Only the Errors in treatment
        last_errors_in_state = last_data['ActiveErrors (Strings)'][last_data['System_State'] == state]
        #Generate the list of active errors during the state
        for item in last_errors_in_state.unique():
            for error_num in item.split(','):
                if (error_num != ''):
                    if (int(error_num) not in last_active_errors):
                        last_active_errors.append(int(error_num.strip()))
        
        #If the treatment was not longer than 2 Minutes do not evaluate it
        if data['System_State'][data['System_State'] == state].size > 120:
            abortion = 'NO'
        else:
            abortion = ''
        
        #If there was a fatal error in the last quarter of the treatment -> Abortion
        abortion_cause = ''
        for item in last_active_errors:
            if item in fatal_errors:
                abortion = 'YES'
                if item in [500053, 511001, 511002, 511046]:
                    abortion_cause = abortion_cause+'Bilanzierungsfehler-'+str(item)+'-'
                elif item in [511095, 511097]:
                    abortion_cause = abortion_cause+'pH Control-'+str(item)+'-'
                elif item in [511033, 511034]:
                    abortion_cause = abortion_cause+'Luft in Konzentrate-'+str(item)+'-'
                elif item in [510008, 510177, 510178, 510179, 510180, 510181, 510182]:
                    abortion_cause = abortion_cause+'Voltage Problem-'+str(item)+'-'
                else:
                    abortion_cause = abortion_cause+'Unknown Cause-'+str(item)+'-'

        last_active_errors=[]
        last_data = data 
        #Select Only the Errors in treatment
        last_errors_in_state = last_data['ActiveErrors (Strings)'][last_data['System_State'] == state]
        #Generate the list of active errors during the state
        for item in last_errors_in_state.unique():
            for error_num in item.split(','):
                if (error_num != ''):
                    if (int(error_num) not in last_active_errors):
                        last_active_errors.append(int(error_num.strip()))

        
        occurred_errors = '-'.join([str(x) for x in last_active_errors])
    
    elif state == 99:
        last_active_errors=[]
        last_data = data
        
        for item in active_errors:
            error_step = errors_in_state.apply(split_errors,args=(str(item),))
            old_value = -1
            for value in error_step.iteritems():
                if (old_value != value[1]): #Value Changed
                    if (value[1] == 1):
                        last_active_errors.append(item)
                    old_value = value[1] 
        
        
        abortion = ''
        abortion_cause = ''
        occurred_errors = '-'.join([str(x) for x in last_active_errors])


    else: #If not Treatment State leave it empty
        last_active_errors=[]
        last_data = data

        #Select Only the Errors in treatment
        last_errors_in_state = last_data['ActiveErrors (Strings)'][last_data['System_State'] == state]
        #Generate the list of active errors during the state
        for item in last_errors_in_state.unique():
            for error_num in item.split(','):
                if (error_num != ''):
                    if (int(error_num) not in last_active_errors):
                        last_active_errors.append(int(error_num.strip()))
        
        abortion = ''
        abortion_cause = ''
        occurred_errors = '-'.join([str(x) for x in last_active_errors])
       
    df_errors = {
        'total_errors': total_errors,
        'no_active_errors': len(active_errors),
        'abortion': abortion,
        'abortion_cause':abortion_cause,
        'occurred_errors': occurred_errors
        }
    
    return df_errors

def mineError(machine_data):
    
    new_row = pd.DataFrame(index=[machine_data.machine+' '+machine_data.date] )
    
    try:
        new_row['software-control'] = machine_data.TDMSproperties['Control version string']
        new_row['software-host'] = machine_data.TDMSproperties['Host version string']
        new_row['software-protect'] = machine_data.TDMSproperties['Protect version string']
        
        if machine_data.analyzed:
            new_row['analyzed'] = 1
            
            fatal_errors = getListOfFatalErrors()
                
            total_errors = evaluate_number_of_errors(machine_data.TDMSdata, 99)
            new_row['total_errors_number']= total_errors['total_errors']
            new_row['total_errors_ids'] = total_errors['occurred_errors']
            
            startup_errors = evaluate_number_of_errors(machine_data.TDMSdata, 0)
            new_row['startup_errors_number']= startup_errors['total_errors']
            new_row['startup_errors_ids'] = startup_errors['occurred_errors']
            
            service_errors = evaluate_number_of_errors(machine_data.TDMSdata, 2)
            new_row['service_errors_number']= service_errors['total_errors']
            new_row['service_errors_ids'] = service_errors['occurred_errors']
            
            preparation_errors = evaluate_number_of_errors(machine_data.TDMSdata, 3)
            new_row['preparation_errors_number']= preparation_errors['total_errors']
            new_row['preparation_errors_ids'] = preparation_errors['occurred_errors']
            
            treatment_errors = evaluate_number_of_errors(machine_data.TDMSdata, 4,fatal_errors)
            new_row['treatment_errors_number'] = treatment_errors['total_errors']
            new_row['treatment_errors_ids'] = treatment_errors['occurred_errors']
            new_row['abortion'] = treatment_errors['abortion']
            new_row['abortion_cause'] = treatment_errors['abortion_cause']
            
            postprocess_errors = evaluate_number_of_errors(machine_data.TDMSdata, 5)
            new_row['postprocess_errors_number']= postprocess_errors['total_errors']
            new_row['postprocess_errors_ids'] = postprocess_errors['occurred_errors']
            
            disinfection_errors = evaluate_number_of_errors(machine_data.TDMSdata, 6)
            new_row['disinfection_errors_number']= disinfection_errors['total_errors']
            new_row['disinfection_errors_ids'] = disinfection_errors['occurred_errors']
    
            
        else:
            new_row['analyzed'] = 0
    except Exception as e:
        log.error('mineError: %s'%e)
    
    return new_row

if __name__ == '__main__':
        
    target_dir_path = '../../../../../../target/'
    source_path = 'X:/LogData_LK2001'
    
    machines = ['LK2001-033', 'LK2001-031','LK2001-015','LK2001-016']
    
    for machine in machines:
        analyzed_folder = 0
        ls_date = os.listdir(source_path+'\\'+machine+'\\')
        for date in ls_date:
            analyzed_folder = analyzed_folder + 1
            print(machine+' '+date+' Percentage of Machine elapsed: '+str(int(100*analyzed_folder/len(ls_date)))+'%')
            machine_data = MachineData.machineData(source_path,machine,date)
            machine_data.debug = True  
            machine_data.readData()
            
            mineError(machine_data,target_dir_path)
            
    print("ErrorMining.py Finished")
  