"""
@Author: Juan Fernandez

"""
import os
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import multiprocessing
from scipy.stats import linregress

import MachineDay as MachineData
from FileHandling.DMLoggers import log

def getStatsTreatmentPressure(machine_data,pressure):
    """
    Returns average std and slope of the selected pressure during treatment (patient connected and bypass)
    """
    if not machine_data.time_in_state(4):
        return {'mean': 0, 'std': 0, 'slope': 0}
    
    treatment_pressure = machine_data.TDMSdata[pressure][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)]
    try:
        slope = linregress(machine_data.TDMSdata[(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)].index.astype(np.int64) // 10**9, 
                           machine_data.TDMSdata[pressure][(machine_data.TDMSdata['Treatment_State'] == 5) | (machine_data.TDMSdata['Treatment_State'] == 6)])[0]
    except:
        slope = 0
    return {'mean': round(treatment_pressure.mean(),2), 'std': round(treatment_pressure.std(),2), 'slope': round(slope,2)}


def getOperatingHours(machine_data, pump_channel):
    ## Returns a dictionary:
    # 'time_on': the time this channel is >0 (seconds)
    # 'mean': the mean value of the channel when its >0
    # 'fluid': total fluid pumped (speed*time, Liters)
    
    data = machine_data.TDMSdata[pump_channel][machine_data.TDMSdata[pump_channel] > 0]
    
    return {'time_on': len(data),
            'mean': round(data.mean(),2),
            'fluid': round(data.sum(),2)/60/1000,# ml/min -> ml/s -> L
            }    

def minePumps(machine_data): # machine_data = md
    
    new_row = pd.DataFrame(index=[machine_data.machine+' '+machine_data.date] )
        
    try:
        if machine_data.analyzed:
            new_row['analyzed'] = 10
            
            tmp = getOperatingHours(machine_data,'CL_FLOW_BLOOD_PUMP_DEM')
            new_row['bloodPump_time_on'] = tmp['time_on']
            new_row['bloodPump_mean'] = tmp['mean']
            new_row['bloodPump_fluid'] = tmp['fluid']
            
             # if its in treatment
            tmp = getStatsTreatmentPressure(machine_data,'S_P_HW_ACID_CD')
            new_row['p_hw_a_mean'] = tmp['mean']
            new_row['p_hw_a_std'] = tmp['std']
            new_row['p_hw_a_slope'] = tmp['slope']
            
            tmp = getStatsTreatmentPressure(machine_data,'S_P_HW_BASE_CD')
            new_row['p_hw_b_mean'] = tmp['mean']
            new_row['p_hw_b_std'] = tmp['std']
            new_row['p_hw_b_slope'] = tmp['slope']
            
            tmp = getStatsTreatmentPressure(machine_data,'S_P_DIA_IN_CD')
            new_row['p_dia_in_mean'] = tmp['mean']
            new_row['p_dia_in_std'] = tmp['std']
            new_row['p_dia_in_slope'] = tmp['slope']
            
            tmp = getStatsTreatmentPressure(machine_data,'S_P_DIA_OUT_CD')
            new_row['p_dia_out_mean'] = tmp['mean']
            new_row['p_dia_out_std'] = tmp['std']
            new_row['p_dia_out_slope'] = tmp['slope']
            
            tmp = getStatsTreatmentPressure(machine_data,'S_P_BLOOD_VENOUS_CD')
            new_row['p_venous_mean'] = tmp['mean']
            new_row['p_venous_std'] = tmp['std']
            new_row['p_venous_slope'] = tmp['slope']
            
            temp = getStatsTreatmentPressure(machine_data,'S_P_BLOOD_PRE_CD')
            new_row['p_pre_mean'] = tmp['mean']
            new_row['p_pre_std'] = tmp['std']
            new_row['p_pre_slope'] = tmp['slope']
            
            temp = getStatsTreatmentPressure(machine_data,'S_P_BLOOD_ART_CD')
            new_row['p_art_mean'] = tmp['mean']
            new_row['p_art_std'] = tmp['std']
            new_row['p_art_slope'] = tmp['slope']
                
            
        else:
            new_row['analyzed'] = 0
    except Exception as e:
        log.error('minePumps: %s'%e)
        return None
    return new_row

if __name__ == '__main__':
         
    target_dir_path = '../../../../../../target/'
    source_path = 'X:/LogData_LK2001'
    
    machines = ['LK2001-033', 'LK2001-031','LK2001-015','LK2001-016']
    
    for machine in machines:
        analyzed_folder = 0
        ls_date = os.listdir(source_path+'\\'+machine+'\\')
        for date in ls_date:
            analyzed_folder = analyzed_folder + 1
            print(machine+' '+date+' Percentage of Machine elapsed: '+str(int(100*analyzed_folder/len(ls_date)))+'%')
            machine_data = MachineData.machineData(source_path,machine,date)
            machine_data.debug = False  
            machine_data.readData()
            
            minePumps(machine_data,target_dir_path)
            
    print("PhMining.py Finished")
  