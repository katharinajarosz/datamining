"""
@author: Juan Fernandez
@organization: ADVITOS GmbH
Script to extract time information of the data files of the LK2001
"""
import os
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import MachineDay as MachineData
from FileHandling.DMLoggers import log

def getRisingEdges(md,signal):
    """
    Calculates how many times the digital input signal changed from 0 to 1
    """
    return (md.TDMSdata[signal].diff()==1).sum()

def getFallingEdges(md,signal):
    """
    Calculates how many times the digital input signal changed from 1 to 0
    """
    return (md.TDMSdata[signal].diff(periods=-1)==1).sum()
    
def getMotorTimeOn(md):
    """
    calculates the time that the lift motor run
    """
    return md.TDMSdata['A_CONT_MOTOR_PWR_DEM'][md.TDMSdata['A_CONT_MOTOR_PWR_DEM'] == 1].sum()
    
def getMotorfeedback(md):
    """
    Calculates the mean value of the motor feedback when it is not zero
    """
    val = (md.TDMSdata['S_CONT_MOTOR_FB_CD'][md.TDMSdata['S_CONT_MOTOR_FB_CD']>0]).mean()
    if (val) > 0:
        return val
    else:
        return 0

      
      
# def get65Volt(md):
#     """
#     Calculates the mean value of the 6.5V 
#     """
#     val = (md.TDMSdata['S_VOLT_65_2_CD'][md.TDMSdata['S_VOLT_65_2_CD']>0]).mean()
#     if (val) > 0:
#         return val
#     else:
#         return 0      

def scalesAbsDiff(md):
    """
    Calculates the mean value of the absolute difference from S_SCALE 1 and 2,
    when container in the upper position and in treatment
    """
    md.TDMSdata['abs_scales_diff'] = md.TDMSdata['S_SCALE_1_CD']-md.TDMSdata['S_SCALE_2_CD']
    try:
        val = (
            md.TDMSdata['abs_scales_diff'][(md.TDMSdata['S_CONT_MOTOR_FB_CD']>0) & (md.TDMSdata['System_State']==4)]
            ).abs().mean()
        return val
    except:
        return 0
    
def containsError(md,errNr):
    """
    Verifies if the machineData contains a specific error
    """
    if (md.TDMSdata['ActiveErrors (Strings)']).str.contains(str(errNr)).max():
        return 1
    else:
        return 0
    
def mineLiftMotor(machine_data):
    """
    Calculate the time information of the machine_data.data Dataframe and save it in a CSV file
    """
    
    new_row = pd.DataFrame(index=[machine_data.machine+' '+machine_data.date] )
    
    try:
        if machine_data.analyzed:
            new_row['analyzed'] = 1
            
            new_row['lift_system_reach_up'] = getRisingEdges(machine_data,'S_CONT_MOTOR_UP_CD')
            new_row['lift_system_leave_up'] = getFallingEdges(machine_data,'S_CONT_MOTOR_UP_CD')
            new_row['lift_system_reach_down'] = getRisingEdges(machine_data,'S_CONT_MOTOR_DOWN_CD')
            new_row['lift_system_leave_down'] = getFallingEdges(machine_data,'S_CONT_MOTOR_DOWN_CD')
            new_row['lift_motor_time_on'] = getMotorTimeOn(machine_data)
            new_row['lift_motor_feedback'] = getMotorfeedback(machine_data)
            
            #new_row['S_VOLT_65_2_CD'] = get65Volt(machine_data)
            new_row['scales_diff'] = scalesAbsDiff(machine_data)
                        
        else:
            new_row['analyzed'] = 0
    except Exception as e:
        log.error('mineLiftMotor: %s'%e)
    return new_row

if __name__ == '__main__':
         
    target_dir_path = '../../../../../../target/'
    source_path = 'X:/LogData_LK2001'
    
    range_start = '01.01.2018'
    range_end = '31.12.2020'
        
    machines = ['LK2001-042']
    #machines = sorted([machine for machine in os.listdir(source_path) if (machine[-3:] not in "00x 001 002 003 004 005 007 099 MP1 MP2 -P1 -P2") & (machine[:2] == "LK")])
    
    print('Machines:',machines)
    print('Evaluate total folders number...')
    analyzed_folder = 0
    total_folders = 0
    for machine in machines:
        ls_date = os.listdir(source_path+'\\'+machine+'\\')
        total_folders += len(ls_date)
    
    for machine in machines:
        ls_date = sorted(os.listdir(source_path+'\\'+machine+'\\'))
        ls_date = [date for date in ls_date if 
                   (pd.to_datetime(date,format='%Y_%m_%d') >= pd.to_datetime(range_start,format='%d.%m.%Y')) & 
                   (pd.to_datetime(date,format='%Y_%m_%d') <= pd.to_datetime(range_end,format='%d.%m.%Y'))]
        #ls_date = ['2018_04_23']
        for date in ls_date:
            analyzed_folder = analyzed_folder + 1
            print(machine+' '+date+' >>> '+str(analyzed_folder)+' of '+str(total_folders)+' = '+str(int(100*analyzed_folder/total_folders))+'%')
            machine_data = MachineData.machineData(source_path,machine,date)
            
            machine_data.channels.append(['Numeric Inputs','S_VOLT_65_2_CD'])
            
            machine_data.debug = True  
            try:
                machine_data.readData()
            except:
                pass
            
            mineLiftMotor(machine_data,target_dir_path)
            
    print("LiftMotorMining.py Finished")
  