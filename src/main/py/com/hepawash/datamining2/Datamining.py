## @package Datamining.py
# Script that reads analyzed folders inside 'source_path'
# that correspond to valid machines and valid dates and
# creates summary files using the different mining scripts
#
# Before running Datamining:
# ReadUSB copies from USB-Drives and convert the data to the right folders,
# Preprocessing is used to pre-analyze the folders and
# Splitfiles is used to split the big TDMS files

#%% imports
# libraries
import os
import datetime
import pandas as pd

# function imports
from shutil import copyfile

# project imports
from FileHandling.DMLoggers import log
import DataPreparation.Preprocessing
import MachineDay

from MainMining import TimeMining
from MainMining import PumpsMining
from MainMining import PhMining
from MainMining import LiftMotorMining
from MainMining import errorTable
from MainMining import ErrorMining
from MainMining import HeaderMining

from Constants import source_path ## Folder in which the machine data is stored
from Constants import target_dir_path ## Folder in which the mined data is saved
from Constants import DAYSRANGE ## number of days to analyse, default=365
from Constants import excluded_machines
from Constants import LASTDATE 

#%% Globals

## Folder in which the old mined data is to be stored
archive_folder = os.path.join(target_dir_path,'_archive/')

## these files are merged together
summaryFiles = ['mineHeader.csv',
                'mineTime.csv',
                'mineError.csv',
                'minePh.csv',
                'minePumps.csv',
                'mineLiftMotor.csv']

## these files have data but are not merged
datafiles = [
            'data.csv',
            'errorTable.csv',
            'crossswitch_data.csv'
            ]

# these funtions will be called in the datamining loop
# if they are marked with "True" they will be merger at the end
mainMining_functions = {HeaderMining.mineHeader:True,
                    TimeMining.mineTime:True,
                    ErrorMining.mineError:True,
                    PhMining.minePh:True,
                    PumpsMining.minePumps:True,
                    LiftMotorMining.mineLiftMotor:True,
                    errorTable.mineErrorTable:False
                    }

# Selects if the old temporary files should be deleted at the beginning
remove_tmp_files = True

# defines if the machines will be preprocessed before mining
preprocess = True

#%%functions

def mergeData():
    ## Merges all the predefined summary files of the Mining modules into data.csv
    df_master = pd.DataFrame()
    
    for mine in mainMining_functions.keys():

        if not mainMining_functions[mine]:
            log.debug('do not merge {}'.format(mine.__name__)) 
            continue
            
        file_path = os.path.join(target_dir_path,mine.__name__ + '.csv')
        log.debug('read {}'.format(file_path))

        df_concat = pd.read_csv(file_path,
                                sep='\t',decimal=',',index_col=0,skip_blank_lines=True, error_bad_lines=False)
        
        df_concat = df_concat[df_concat.index.notnull()]
        
        log.debug('merge {} ({} lines)'.format(file_path,len(df_concat)))
        
        if df_master.empty:
            df_master = df_concat.copy()
        else:
            df_master = pd.concat([df_master, df_concat], axis=1, join='inner')
    try:
        data_csv = os.path.join(target_dir_path,'data.csv')
        df_master.to_csv(data_csv ,sep='\t',decimal=',',index_label="index")
    except Exception as e:
        log.error(e)

#%%
## Moves the old summary files to the predefined archive_folder
def backUpSummaryFiles():
    timestamp = str(datetime.datetime.strftime(datetime.datetime.now(),format="%Y%m%d_%H%M%S"))

    for file in (summaryFiles + datafiles):
        log.debug('Move %s'%file)
        data_source = os.path.join(target_dir_path,file)
        data_target = os.path.join(archive_folder,timestamp+file)    
            
        try:
            copyfile(data_source, data_target)
            os.remove(data_source)
        except FileNotFoundError:
            log.info('File not found: %s'%(data_source))
        except Exception as e:
            
            log.error('backup Error: %s'%e,exc_info=True)

def findAll_machineDates(src_path, machine_list = ''):
    ## finds all valid machine-date pairs
    # @return: dict in format {machine: dates[] }
    # @return: total number of folders found
    
    all_mds = {}
    log.info('findAll_machineDates')
    total_dates = 0
    # get the machine list
    if not machine_list:
        machine_list = sorted([m for m in os.listdir(src_path) if is_valid_machine(src_path,m)])
    
    # calculate total number of folders
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if is_valid_date(source_path,machine,d)])
        all_mds[machine] = date_list
        total_dates += len(date_list)
    log.info('{} folders and {} machines'.format(total_dates,len(all_mds)))
    return all_mds, total_dates

## verifies if a string corresponds to a valid machine in the source_path
# - Must be a folder inside the source path
# - Must have a number after 'LK2001-'
# - The part after 'LK2001-' must not be in the predefined excluded_machines list
# @return True for valid and False for invalid
def is_valid_machine(source,m):

    # must be a directory    
    if not os.path.isdir(os.path.join(source,m)):
        return False
    
    # must have 'LK2001-', a number afterwars
    # and not be in the excluded list
    try:
        number = m.split('LK2001-')[1]

        if number not in excluded_machines:
            return True
    except:
        return False
        
    return False

def calc_dates_range(date_ref= datetime.datetime.now(),end_of_lastMonth=True):
    ## Calculates a one dates intervall ending in the last day of the previous month
    # @param (opt): date to find the 'previous month', standard is current date
    # @return: a touple (date_start, date_end)
    if end_of_lastMonth:
        date_end = datetime.date(date_ref.year,date_ref.month,1)-datetime.timedelta(days=1)
    else:
        date_end = datetime.date(date_ref.year,date_ref.month,date_ref.day)-datetime.timedelta(days=1)
    date_start = date_end-datetime.timedelta(days=DAYSRANGE)
    return (date_start,date_end)
#%%
## Checks if the machine has the date
# - The date must be in the specified range
# - Must be a directory inside the machine directory
# @return True for valid and false for invalid
def is_valid_date(source,m,d,include_thismonth=False):
    
    if not os.path.isdir(os.path.join(*[source,m,d])):
        return False
    
    if LASTDATE:
        date_start, date_end = calc_dates_range(pd.to_datetime(LASTDATE,format='%Y_%m_%d'),
                                                end_of_lastMonth=(not include_thismonth))
    else:
        date_start, date_end = calc_dates_range(end_of_lastMonth=(not include_thismonth))
        
    if include_thismonth:
        date_end = pd.datetime.now().date()
    try:
        date = datetime.datetime.strptime(d,'%Y_%m_%d').date()
    
        if (date >= date_start) & (date <= date_end):
            return True
    except:
        pass
    return False

#%% __name__ == '__main__'
if __name__ == '__main__':
	## @cond INTERNAL
    
    # used if just the merge procedure is necessary
    if False:
        log.info('Start Merge')
        mergeData()
        log.info('Merged')
        raise Exception('exit')

    log.info('Start Datamining')
    
    # create a backup of old files
    log.info('Starting - Generate Backup Files')
    backUpSummaryFiles()
    
    # get the machine list
    machine_list = sorted([m for m in os.listdir(source_path) if is_valid_machine(source_path,m)])
    #machine_list = ['LK2001-051']
    log.info('%d Valid machines found'%len(machine_list))
    
    log.debug('Calculate to total number of folders...')
    analyzed_folder = 0
    total_folders = 0
    success_reads = 0
    
    # calculate total number of folders
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if is_valid_date(source_path,machine,d)])
        total_folders += len(date_list)

    # for refference
    script_init_time = datetime.datetime.now()
        
    # create results dict with one Dataframe for each function
    for func in [f.__name__ for f in mainMining_functions.keys()]:
        print(func)
        target_file = os.path.join(target_dir_path, func + '.csv')
        if os.path.isfile(target_file) and remove_tmp_files:
            log.info('Removing {}'.format(target_file))
            os.remove(target_file)
    
    # analyse the folders
    for machine in machine_list:
        

        # preprocess machine before Mining

        if preprocess:
            DataPreparation.Preprocessing.preprocess_machine(source_path, machine)
        
        # create results dict with one Dataframe for each function
        results = {}
        for func in [f.__name__ for f in mainMining_functions.keys()]+['mineHeader']:
            log.debug('Add mining function {}'.format(func))
            results[func] = pd.DataFrame()

        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if is_valid_date(source_path,machine,d)])
        for date in date_list:
            
            analyzed_folder +=1
            log.info('Analyse %s %s. %d of %d (%d%%)'%(machine,date,analyzed_folder,total_folders,100*analyzed_folder/total_folders))
            log.info('Success rate %.2f: %d of %d'%(success_reads/analyzed_folder,success_reads,analyzed_folder) )
            
            
            # prevision of the end of the run
            log.info("ETA: %s"%datetime.datetime.strftime(
                (script_init_time + (total_folders/analyzed_folder)*(datetime.datetime.now()-script_init_time) )
                ,format='%Y-%m-%d %H:%M:%S'))
            
            # saves the errors during run for logging
            error_scripts = ''
            # - - - - - - - - - - - - - - - - -
            #        MINING SCRIPTS
            # - - - - - - - - - - - - - - - - -
            try:
                #
                # If Load and Header have errors move to next machine-day
                md = MachineDay.load_MD(source_path,machine,date)
                if not md:
                    log.error('RUN FAIL: md-load %s %s'%(machine,date),exc_info=True)
                    continue
                #
                # Run each script in a try-catch block
                for mine in mainMining_functions.keys():
                    try:
                        # Handle time_data.csv (summary file)
                        log.debug('Mine {} {}: {}'.format(machine, date, mine.__name__))
                        results[mine.__name__] = results[mine.__name__].append(mine(md))
                    except Exception as e:
                        log.error('Mine Error:'+mine.__name__+('%s'%e),exc_info=True)
                        error_scripts += mine.__name__
                
                if not error_scripts:
                    log.info('RUN SUCCESSFUL %s %s'%(machine, date))
                    success_reads += 1
                else:
                    log.info('RUN FAIL: mining %s %s : %s'%(machine, date, error_scripts))

            except Exception as e:
                log.error('RUN FAIL: load error: %s %s %s'%(machine,date,e),exc_info=True)
            
        log.info('Datamining Script finished on machine %s', machine)
        
        
        for r in results:
            if results[r].empty:
                log.info('Empty {}, skip'.format(r))
                continue
            
            # append the new data from every DataFrame in results-dict
            target_file = os.path.join(target_dir_path, r + '.csv')
            log.info('Append {} lines to file:{}'.format(len(results[r]),target_file))
                 
            results[r].to_csv(target_file,
                       sep='\t',decimal=',', mode='a', date_format ='%Y.%m.%d')
            
            results[r] = None # free memory
        del results

    
    log.info('Finish mining, merge data')
    mergeData()
    log.info('Success rate %.2f: %d of %d'%(success_reads/analyzed_folder,success_reads,analyzed_folder) )

    log.info('Finish All')
	
	## @endcond 
