# -*- coding: utf-8 -*-

# This file manages the "imports" of external data sources that must be
# downloaded from intern folders in the server or Sharepoint site
#
## @author: RaHe
# @organization: ADVITOS GmbH


import subprocess
import os
import errno

import sys
sys.path.append('..')

from FileHandling.DMLoggers import log
from Constants import target_dir_path,\
    filepath_ErpConnection, filepath_AdvosBIConnection, filepath_listOA, filepath_GFL, filepath_ListOfSystemErrors

print(__file__)
os.path.dirname(os.path.dirname(__file__))

def download_file(url,dest='',overwrite=False):
    ## Downloads a file to a destination.
    # Uses external powershell script for user authentication.
    # If the destination is not given, a resources folder is created
    # and the file is downloaded there
    datamining_folder = os.path.dirname(os.path.dirname(__file__))
    res_folder = os.path.join(datamining_folder,'..\\..\\..\\..\\resources\\')
    if not dest:
        os.makedirs(res_folder,exist_ok=True)
        dest = res_folder + os.path.basename(url)
        
    log.debug('Start download:{}'.format(dest))
        
    # if there is a file and overwrite is not active
    if (os.path.isfile(dest)) and (overwrite == False):
        log.info('Not downloaded, file already exists. Use overwrite=True to overwrite')
        return dest
    
    CMDstring = 'powershell '+datamining_folder+'\\ExternalConnections\\FileDownload.ps1 '+url+' '+dest
    log.debug('Download command:{}'.format(CMDstring) )
    subprocess.call(CMDstring)
    
    if os.path.isfile(dest):
        log.debug('Download size:{:.3f}MB'.format(os.path.getsize(dest)/1024/1024))
    else:
        log.error('File was not downloaded')
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), url)
        
    return dest
    
def download_all(overwriteFile=True):
    ## Dowloads all Files with Path defined in the Constants file
    
    for dwFile in [filepath_ErpConnection, filepath_AdvosBIConnection, filepath_listOA, filepath_GFL, filepath_ListOfSystemErrors]:
        log.debug('Downloaded:'+
            download_file(dwFile,
            overwrite=overwriteFile))
    
if __name__ == '__main__':
    download_all()