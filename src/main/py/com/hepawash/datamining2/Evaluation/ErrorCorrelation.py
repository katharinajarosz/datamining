# -*- coding: utf-8 -*-

#%% imports
import pandas as pd
import sys
from os.path import join

sys.path.append('..')
from ExternalConnections.AdvosBI import advosBIinterface

from Constants import dm_target, source_path, target_dir_path
from Datamining import findAll_machineDates

from FileHandling.DMLoggers import log
from MachineDay import load_MD

#%% constants

# minimum correlation between the errors for it to be listed
MIN_CORR = 0.01

# minimum absolute number of errors for it to be listed
MIN_ERRORCOUNT = 10

# creates a table showing the time between albumin intake and the 
# piston pumps error
time_from_albumin = False

#%% init

data = pd.read_csv(join(target_dir_path,'data.csv'),sep='\t',decimal=',',index_col=0)

#%% main

if __name__ == '__main__':
    # create error-correlation table
    log.info('Start Error Correlation')
    
    #get the error column
    error_col = data['total_errors_ids']
    
    # drop all cases where there was a service or is not in the field
    error_col = error_col.drop(error_col[
            (data['locationMD'].str.contains('ADVITOS')) |\
            (data['locationMD'].str.contains('Hepa Wash')) |\
            (data['service_time'] > 0)].index)
    error_col.dropna(inplace=True)
    
    # generate a table with "dummy" columns, where 1 represents it occurred
    # also translated errorNr to errorCode
    data_table = pd.DataFrame()
    log.info('Create dummy columns')
    for rowNr in range(len(error_col.index)):
        for error in error_col[rowNr].split('-'):
            if sum(error_col.str.contains(error)) < MIN_ERRORCOUNT:
                continue
            
            errCode = advosBIinterface.getValues_byErrNr(error,'Error-ID')[0]
            if errCode:
                data_table.loc[error_col.index[rowNr], errCode] = 1
            else:
                pass#data_table.loc[error_col.index[rowNr], error] = 1
    data_table = data_table.fillna(0)
    
    # create correlations table 
    log.info('Create Correlation')
    corr_table = data_table.corr()
    
    # create a table with descending error correlations for each error
    log.info('Sort Correlation')
    corr_sorted = pd.DataFrame()
    notxt = set()
    for err in corr_table.index:
        
        err_index = '{}:{}'.format(err,advosBIinterface.getValues_byErrCode(err,'Error Text (Service)'))    
        colnr = 1 # keeps track of the current column
        
        for relatedErr in corr_table[err].sort_values(ascending=False).keys():
            if err != relatedErr and corr_table[err][relatedErr] > MIN_CORR:
                svcTxt = advosBIinterface.getValues_byErrCode(relatedErr,'Error Text (Service)')
                if not svcTxt:
                    notxt.add(err)
                corr_sorted.loc[err_index,'{}-related_error'.format(colnr)] = '{}:{}'.format(relatedErr,svcTxt)
                corr_sorted.loc[err_index,'{}-correlation'.format(colnr)] = corr_table[err][relatedErr]
                colnr +=1 
    corr_sorted.sort_values(by=['1-correlation'],ascending=False,inplace=True)
    
    # save
    log.info('Save')
    wr = pd.ExcelWriter(join(dm_target,'ErrorCode-correlation.xlsx'))
    
    corr_sorted.to_excel(wr,sheet_name='A_Error-Correlation')
    corr_table.to_excel(wr,sheet_name='B_Error-Correlation')
    wr.save()
    
#%% Time between errors and albumin intake

machineDates , nrOfMDs = findAll_machineDates(source_path)
        
def findLast(entry,table):
    #entry= error
    # table= albuminintakes
    
    # entry = PPerrors.iloc[0]
    # table = Albintakes
    if True:#try:
        entry_time = pd.to_datetime(entry['Time'],dayfirst=True)
        
        table_times = pd.to_datetime(table['Time'],dayfirst=True)
        
        lastIntake = table_times[table_times < entry_time].max()
        
        return (entry_time - lastIntake).total_seconds()/60/60
    #except Exception as e:
    #    print(e)
     #   return float('Nan')

if time_from_albumin:

    final_list = pd.DataFrame()
    
    csv_errors = []
    csv_columns = ['Time', 'Error Number', 'Error ID', 'Error Text for User',
                   'Error Solution for User', 'Error Text for Service',
                   'Error Solution for Service', 'Source Module']
    
    for machine in machineDates:
        
        Allerrors = pd.DataFrame()
        
        for date in machineDates[machine]:
            # machine, date = 'LK2001-010','2019_08_20'
            mday = load_MD(source_path,machine,date,load_tdms=False)
            
            if (not mday) or (mday.CSVdata.empty):
                continue
            
            csv_data = mday.CSVdata
                
            try:
                csv_data['Time']
            except:
                continue

            idx_string = '{} {}'.format(mday.machine, mday.date)
            
            csv_data.index = [idx_string for _ in csv_data.index]
            
            Allerrors = Allerrors.append(csv_data)

        # separate all piston pump errors  
        isPPerror = \
        (Allerrors['Error ID'] == 'IH-19') | \
        (Allerrors['Error ID'] == 'IH-20') | \
        (Allerrors['Error ID'] == 'IH-21') | \
        (Allerrors['Error ID'] == 'IH-22') | \
        (Allerrors['Error ID'] == 'IH-31') | \
        (Allerrors['Error ID'] == 'IH-32')
    
        if not sum(isPPerror) > 0:
            continue
    
        # Albumin intakes
        isAlbIntk = (Allerrors['Error ID'] == 'BI-20')
        
        # Postprocess finished
        isPostProc = (Allerrors['Error ID'] == 'BI-23')
    
        # get the whole entries for abl.intake and pp.errors
        Albintakes = Allerrors[isAlbIntk]
        PPerrors = Allerrors[isPPerror]
        PostProc = Allerrors[isPostProc]
        
        if not PPerrors.empty:
            # get time between error and last albumin intake
            PPerrors['timeFromAlbItk(h)'] = PPerrors.apply(lambda x: findLast(x,Albintakes), axis=1)
            PPerrors['timeFromPostProc(h)'] = PPerrors.apply(lambda x: findLast(x,PostProc), axis=1)
            
            #remove redundancies
            for uniq_idx in PPerrors['timeFromAlbItk(h)'].index.unique():
                
                day_errors = PPerrors.loc[uniq_idx]
                
                try:
                    len(day_errors['timeFromAlbItk(h)']) # test if this is a list
                
                    final_list = final_list.append(\
                       day_errors[day_errors['timeFromAlbItk(h)'] == day_errors['timeFromAlbItk(h)'].min()])
                except:
                    # there is only one value
                    final_list = final_list.append(day_errors)

    final_list = final_list[['Error ID','Error Number','Time','timeFromAlbItk(h)','timeFromPostProc(h)']]
    final_list.to_excel('timeToAlbItk.xlsx')