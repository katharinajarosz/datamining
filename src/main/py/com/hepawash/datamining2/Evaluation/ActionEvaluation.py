# -*- coding: utf-8 -*-
#
# This is a evaluation of implemented actions
#
# @author: RaHe
# @organization: ADVITOS GmbH

import pandas as pd
# register the converters explicity
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import sys
import os
import datetime
import matplotlib.pyplot as plt

sys.path.insert(0,'..')
from ExternalConnections.AdvosBI import advosBIinterface
from FileHandling.DMLoggers import log
from ExternalConnections.FileDownload import download_file
from Constants import target_dir_path, filepath_listOA, filepath_GFL

log.info('Load resources...')
#% RESOURCES
listOSE = advosBIinterface.list_of_system_errors

# download and read list of actions
list_of_actions = pd.read_excel(
        download_file(filepath_listOA,overwrite=True)
        ,sheet_name='listOA').fillna('')

# ERP service Table
from ExternalConnections.ErpConnection import ERP_Interface
db_erp = ERP_Interface('ProdRead','SRVDB01\SQLSRV02','APplusProd6')
svc_tb = db_erp.svc_repairs

# Gesamtfehlerliste Datei
log.info('Load GFL...')
GFL_tb = pd.read_excel(
        download_file(filepath_GFL,overwrite=True),
        sheet_name='GFL',header=5,index_col='Pos.')

# ErrorTable
path_errorTable = download_file(os.path.join(target_dir_path,'errorTable.csv'), overwrite=True)

# Treatments
# WORKAROUND to include the treatment count
CStatistics_path = 'X:/EvaluationData_LK2001/resources/treatments.xlsx'

# folder in which the data will be saved
AE_root_folder = target_dir_path

# defines if the scripts erases old evaluations at the beginning
# note: existing evaluations are otherwise NOT overwirtten
CLEAN_ALL = False

#%%
#
#   Class definition
class action_effectiveness():
    ## This class defines every 'Action Effectiveness'
    
    # static fields configured for all the instances
    root_folder = AE_root_folder # where to save the outputs
    errorTable = None # the errorTable as pd.Dataframe
    df = pd.DataFrame()
    treatments = pd.DataFrame()
    created_folder_name = 'ActionEffectiveness' # folder that this class creates

    def __init__(self,name_,eval_,date_start_,date_end_,description_):
        self.name = name_
        self.eval = eval_
        self.date_start = date_start_
        self.date_end = date_end_
        self.description = description_
        self.isErrorCode = self.eval_isErrorCode(eval_)
        
        self.folder = self.create_folder()
        log.debug('Init Name:%s Eval:%s IsErrCode:%s Dates:%s %s'%(name_,eval_,self.isErrorCode,date_start_,date_end_))

    def __str__(self):
        return 'Name: '+self.name + '\nEval: ' + self.eval + ' ' + self.date_start + ' '+ self.date_end + '\nDescription: ' + self.description

    def eval_isErrorCode(self,string):
        ## Returns true if the string is a error code, i.e. if the string:
        # Is five characters long, char[0,1] are letters, ch[3] is '-' 
        # ch[4,5] are numbers
        s = string.strip()
        if (len(s) == 5) and (s[2] == '-') and (s[0:1].isalpha()) and (s[3:].isdigit()):
            return True    
        return False

    def create_folder(self):
        ## creates a folder for the actionEffectiveness if necessary
        path = os.path.join(*[self.root_folder,action_effectiveness.created_folder_name,self.name])
        os.makedirs(path, exist_ok=True)
        return path
    
    def clean_folder():
        ## cleans all the folders in actionEffectiveness folder
        # detail: ignores files in the first-level folder
        # detail: ignores folders starting with '_' like '_archiv' 
        ac_folder = os.path.join(
                action_effectiveness.root_folder,
                action_effectiveness.created_folder_name)
        log.debug('Clean folder {}'.format(ac_folder))

        for subDir_name in [d for d in os.listdir(ac_folder)
                if os.path.isdir(os.path.join(ac_folder,d))]:
            if subDir_name[0] == '_':
                log.debug('Skipped clean folder {}'.format(subDir_name))
                continue
            
            subDir = os.path.join(ac_folder,subDir_name)# subDir_name = 'EV1609-013-AC-11'
            
            for file in os.listdir(subDir):
                log.debug('delete {} {}'.format(subDir,file))
                try:
                    os.remove(os.path.join(subDir,file))
                except:
                    log.error('',exc_info=True)
            try:
                log.debug('delete {}'.format(subDir))
                os.rmdir(subDir)
            except:
                log.error('',exc_info=True)
            
            #log.debug('clean folder {}'.format(subDir))
            #print( subDir)
        
        
    def filterErrorTb(self):
        df = pd.DataFrame()
        if self.isErrorCode:
            # if it is a error code, filter table and plot graph
            log.debug('ErrorCode Select %s'%(self.eval) )
            df = action_effectiveness.errorTable[action_effectiveness.errorTable['error_code'] == self.eval]
            self.create_plots(df,self.eval)
        elif self.eval != 'TOTAL' and ('*' not in self.eval):
            # if it is a keyword, filter a list and plot graph
            errCodes = listOSE[listOSE['keyword'] == self.eval]['Error-ID'].tolist()
            log.debug('Keyword Select %s (%s)'%(self.eval,str(errCodes)) )
            df_idx = action_effectiveness.errorTable['error_code'].isin(errCodes)
            df = action_effectiveness.errorTable[df_idx]
            self.create_plots(df,self.eval)
            
            for code in set(errCodes):
                df2 = pd.DataFrame()
                log.debug('ErrorCode Select %s'%(code) )
                df2 = action_effectiveness.errorTable[action_effectiveness.errorTable['error_code'] == code]
                self.create_plots(df2,code)
        elif '*' in self.eval:
            # This handles cases with the wildcard in the end of '*' error-codes
            df = action_effectiveness.errorTable[action_effectiveness.errorTable['error_code'].str.startswith(self.eval[:-1])]
            self.create_plots(df,self.eval)
            
        else:
            log.debug('Creating the total statistics')
            # create the plots for all states
            self.create_plots(action_effectiveness.errorTable,'TOTAL')
            
            # create plots for the different states
            for sState in action_effectiveness.errorTable['System_State'].unique():
                self.create_plots(
                action_effectiveness.errorTable[action_effectiveness.errorTable['System_State']==sState],sState)
            
        df.to_excel(os.path.join(self.folder,self.eval.replace('*','')+'_'+self.date_end+'_all.xlsx'))
        return
    
    def create_plots(self,df_,select):
        log.debug('Create plots for {}. LEN= {}'.format(select,len(df_)))
        self.plot_graph(df_,select,relative=True)
        self.plot_graph(df_,select,relative=False)
        

    def plot_graph(self,df_,select,relative=True):
        log.debug('PLOT Name:%s select:%s Date:%s %s'%(self.name,select,self.date_start,self.date_end))
        
        fig_filepath = os.path.join(self.folder,select.replace('*','')+'_'+str('rel_' if relative else 'abs_')+self.date_end+'.png')
        if os.path.isfile(fig_filepath):
            log.info('Skip plot: Figure already exists. ({})'.format(fig_filepath))
            return
       
        log.debug('Lines selected: %d'%len(df_))
        df = df_.copy()
        
        # # # # # # # # # # # # # # # 
        # - - - - - Calculations - - - - - - 
        # # # # # # # # # # # # # # # 
        # convert date format
        df['date'] = pd.to_datetime(df['date'],format='%Y_%m_%d')
        
        # group by month
        abs_data = df.groupby([pd.Grouper(key='date', freq='MS')]).count()['index']
        fig_data = pd.Series()
        for idx in abs_data.index:
            try:
                if relative:
                    t = action_effectiveness.treatments.loc[datetime.datetime.strftime(idx,'%Y-%m'),'02_Anzahl Behandlungen']
                    fig_data.loc[idx] = abs_data[idx] / t
                else:
                    fig_data.loc[idx] = abs_data[idx]
            except Exception as e:
                log.error(e,exc_info=True)
                
        # define time intervall
        lastDate = pd.Timestamp(df['date'].max(), freq='MS')
        firstDate = lastDate - datetime.timedelta(days=365)
        for limit in [firstDate,pd.Timestamp(lastDate, freq='MS')]:
            try:
                fig_data[limit]
            except:
                fig_data[limit] = 0
        
        # calculate the implementation, before and after statistics
        try:
            implem_start = pd.to_datetime(self.date_start,format='%Y_%m')
            implem_end = pd.to_datetime(self.date_end,format='%Y_%m')
        except:
            implem_start = fig_data.index.max()
            implem_end = implem_start
            log.warning('Invalid date range ({} to {}). Using last date {}'.format(self.date_start,self.date_end,implem_end))
            
            
        before = fig_data[fig_data.index <= implem_start].describe()
        after = fig_data[fig_data.index >= implem_end].describe()

        # save data
        wr = pd.ExcelWriter(os.path.join(
                self.folder,
                str(select.replace('*','')) + \
                '_'+str('rel_' if relative else 'abs_') + \
                self.date_end+'.xlsx'))
        
        
        fig_data.to_excel(wr,sheet_name='fig_data')
        df.to_excel(wr,sheet_name='errors')
        action_effectiveness.treatments.to_excel(wr,sheet_name='treatments')
        abs_data.to_excel(wr,sheet_name='monthly')
        
        wr.save()
        
        # # # # # # # # # # # # # # # 
        # - - - - - Plots - - - - - - 
        # - - - - - Errors - - - - - 
        # # # # # # # # # # # # # # # 
        log.debug('Plot...')
        
        fig, ax1 = plt.subplots()
        # bars
        log.debug('Errors... %d'%len(fig_data.index))
        if (len(fig_data.index) == 0):
            log.debug('No entries found')
            return

        
        bar1 = ax1.bar(fig_data.index,fig_data.values,width=len(fig_data.index)*1.5,edgecolor='k',color='white')
        plt.xticks(fig_data.index, fig_data.index.strftime('%Y-%m'), rotation='vertical')
        if relative:
            bar1.set_label('Errors per Treatment')
            plt.ylabel('Errors per Treatment')
        else:
            bar1.set_label('Errors count')
            plt.ylabel('Errors count')
        plt.ylim([0,fig_data.values.max()*1.1])
        
        # horizontal lines before
        top, mid, bot = before['mean']+before['std'], before['mean'], before['mean']-before['std']
        alpha_ = 0.6
        #plt.plot([implementation,implementation,implementation],[bot,mid,top],
        #          marker='<', markersize=10, markerfacecolor='k',markeredgecolor='k')
        plt.plot([fig_data.index.min(),implem_start], [top,top],color='k',linestyle=':',alpha=alpha_)
        plt.plot([fig_data.index.min(),implem_start], [mid,mid],color='k',alpha=alpha_)
        plt.plot([fig_data.index.min(),implem_start], [bot,bot],color='k',linestyle=':',alpha=alpha_)
        
        # horizontal lines after
        top, mid, bot = after['mean']+after['std'],after['mean'], after['mean']-after['std']
        plt.plot([implem_end,fig_data.index.max()], [top,top],color='k',linestyle=':',alpha=alpha_)
        plt.plot([implem_end,fig_data.index.max()], [mid,mid],color='k',alpha=alpha_)
        plt.plot([implem_end,fig_data.index.max()], [bot,bot],color='k',linestyle=':',alpha=alpha_)

        # textbox with statistics results
        text = ''
        for stat in ['count', 'mean', 'std', 'min', '25%', '50%', '75%', 'max']:
            text += stat + ': %.3f'%before[stat] + ' | ' + '%.3f'%after[stat]+'\n'
        plt.gcf().text(1.05,0.2,text)

        # implementation marker
        plt.plot(implem_start,(before['mean']),
             marker='<', markersize=10, markerfacecolor='k',markeredgecolor='k',alpha=alpha_)
        plt.plot(implem_end,(after['mean']),
             marker='>', markersize=10, markerfacecolor='k',markeredgecolor='k',alpha=alpha_)
        
        # # # # # # # # # # # # # # # 
        # - - - - - - svc - - - - - -
        # # # # # # # # # # # # # # # 
        log.debug('Svc...')
        ax2 = ax1.twinx()
        svc_filtered = svc_tb[svc_tb['schlagwoerter'].str.contains(select)].groupby([pd.Grouper(key='Datum Auftrag', freq='MS')]).size()
        svc_data = pd.Series()
        for ix in fig_data.index:
            if ix in svc_filtered.index:
                svc_data[ix] = svc_filtered[ix]
            else:
                svc_data[ix] = 0
        #bar2, = ax2.plot(svc_data.index,svc_data.values,
        #                 'bs--',alpha=0.6,# marker=4,markeredgecolor='k',markerfacecolor='y',markersize=8,
        #                 label='Service Operations')
        bar2 = ax2.bar(svc_data.index,svc_data.values,width=len(fig_data.index)/2,color='y',label='Service Operations')
        
        # # # # # # # # # # # # # # # 
        # - - - - - - GFL - - - - - -
        # # # # # # # # # # # # # # # 
        log.debug('GFL...')
        gfl_filtered = GFL_tb[(GFL_tb['Kategorie-Schlagwort\nCatchword'] == select)&(GFL_tb['BA'] == 'Ja')].groupby([pd.Grouper(key='Wann', freq='MS')]).size()
        gfl_data = pd.Series()
        for ix in fig_data.index:
            if ix in gfl_filtered.index:
                gfl_data[ix] = gfl_filtered[ix]
            else:
                gfl_data[ix] = 0
        #bar3, = ax2.plot(gfl_data.index,gfl_data.values,
        #                 'ro--',alpha=0.6,#linestyle='None', marker='X',markeredgecolor='r', markerfacecolor='r',markersize=8,alpha=alpha_,
        #                 label='Treatment Abortions')
        bar3 = ax2.bar(gfl_data.index,gfl_data.values,width=len(fig_data.index)/3,color='r',label='Treatment Abortions')
        
        ax2_yMax = max([svc_data.max(),gfl_data.max()])
        ax2.set_ylim([0,1.1*ax2_yMax if ax2_yMax else 1])
        ax2.set_ylabel('Service and Abortions')
        
        # # # # # # # # # # # # # # # 
        # - - - Descriptions - - - -
        # # # # # # # # # # # # # # # 
        # legend
        plt.legend(handles=[bar1,bar2,bar3],bbox_to_anchor=(1.15, 1), loc='upper left', borderaxespad=0.)
        
        # add title with error code information
        if self.eval == 'TOTAL':
            title = '%s'%self+' \nKeyword:'+select
        else:
            title = '%s'%self+' \nKeyword:'+select+' ('+\
            ' '.join(df['error_code'].unique()) + ')'
        plt.title(title)
        
        # save figure
        log.debug('Save...{}'.format(fig_filepath))
        plt.savefig(fig_filepath,bbox_inches='tight')
        #plt.show()
        plt.clf()


#%%
log.info('Read errorTable and treatments...')
# MachineData errorTable
error_Table = pd.read_csv(path_errorTable,decimal=',',sep='\t')

action_effectiveness.errorTable = \
error_Table[~(error_Table['locationMD'].str.contains('Hepa Wash',na=True, case=False)\
 | error_Table['locationMD'].str.contains('Advitos',na=True, case=False))]

action_effectiveness.treatments = pd.read_excel(CStatistics_path,index_col=0)

if CLEAN_ALL:
    action_effectiveness.clean_folder()
#action = action_effectiveness(
#        'TestAction','Luftsensoren','2018_09','test')  
#action.filterErrorTb()  

#%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#
#       ACTIONS FUNCTIONS DEFINITION
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#   Functions that evaluate the data that was mined for the action
#   Every function is defined here
#   !AND NEEDS TO BE ADDED IN THE DICTIONARY!

def EV18018_AC_01():
    ## This action needs more evaluation, not just the errorTable filter
    log.debug('Function Evaluate EV18018_AC_01')

# Dictionary for the functions, signs '-' must be replaced by '_'
func_dict = {
        'EV18018-AC-01':EV18018_AC_01}

#%% - - - - - - - - - - - - -
#   Evaluation
# executes the evaluation after the data is already there
if __name__ == '__main__':
    
    
    done = 0
    
    #create total
    action_effectiveness(
        'Total','TOTAL',
        '2020_03','2021_04','All errors').filterErrorTb()
    
    for idx, ac in list_of_actions.iterrows():
        total = len(list_of_actions)
        try:
            log.info('%d of %d (%.2f) done'%(done,total,done/total))
            # create the 'action' abject
            action = action_effectiveness(
                    ac['action_name'],ac['evaluation'],
                    ac['implementation_start'],ac['implementation_end'],ac['description'])
            
            # call extra function if is implemented
            if action.name in func_dict:
                func_dict[action.name]()
            
            # create graph
            action.filterErrorTb()
            
            done +=1
            
        except Exception as e:
            log.error(('%s %s'%(ac,e)).replace('\n',' '),exc_info=True)
