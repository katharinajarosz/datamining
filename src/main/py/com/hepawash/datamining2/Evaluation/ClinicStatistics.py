# -*- coding: utf-8 -*-
#
# This is the main report giving an overview of how and which clinics use
# the machines more 
#
# @author: RaHe
# @organization: ADVITOS GmbH


#%% imports
import pandas as pd
import sys
import datetime

# import other Datamining Modules
sys.path.append('..')
from os.path import join
from Constants import target_dir_path, res_dir_path, performances_file, filepath_GFL,\
    filepath_ListOfSystemErrors, filepath_ErpConnection
from FileHandling.DMLoggers import log
from Util import try_GetVal

from ExternalConnections.FileDownload import download_file

from ErrorPriority import evalErrorPriority

#%% Globals

# name of the file with all merged data from the MainMining Scripts
dataFile = 'data.csv'

# treatments longer than this are considered 'long treatments'
limit_longtreatment = 8*60*60 # seconds

# where the data-file is saved, defined elsewhere
target_dir_path

# name of the ouput file
clinicStatistics_file = 'ClinicStatistics.xlsx'

# minimum correlation between the errors to show in the correlation table
MIN_CORR = 0.1

# minimum absolute number of errors for it to be listed
MIN_ERRORCOUNT = 10

# URL to download the GFL


#%% functions

#df = data_df[((data_df['locationID'] == clinicID) &(data_df['year-month']==month)) ]
def treatment_stats(df):
    entry = {}
    
    entry['10_Anzahl Behandlungen'] = df[(df['treatment_time']>0)].count()[0]
    
    # Monday is 1 and Sunday is 7
    iso_days = pd.to_datetime(df[(df['treatment_time']>0)]['date']).apply(datetime.date.isoweekday)
    entry['10a_Anzahl wochenende Behandlungen'] = sum(iso_days == 6) + sum(iso_days == 7)
    
    entry['11_Anzahl kurze Behandlungen'] = df[(df['treatment_time']<limit_longtreatment) & 
                                                       (df['treatment_time']>0)].count()[0]
                                                       
    entry['12_Anzahl lange Behandlungen'] = df[(df['treatment_time']>=limit_longtreatment)].count()[0]
    
    entry['13_Anzahl Behandlungsabbruche'] = 0 # replaced by GFL
    #df[(df['abortion']=='YES') ].count()[0]
    
    entry['14_0800-Anrufe'] = 0
    entry['15_Service_Einsatze'] = df[df['service_time']>0].count()[0]
    entry['16_Anzahl Kundenruckmeldung'] = 0

    entry['17_Anzahl Mittelwert Ziel pH > 8.5'] = df[(df['goal_ph_res_treatment_mean']>8.5)].count()[0]
    
    entry['18_Anzahl Mittelwert Ziel pH > 8'] = df[(df['goal_ph_res_treatment_mean']>8)].count()[0]
    
    entry['19_Anzahl Mittelwert Ziel pH > 7.75'] = df[(df['goal_ph_res_treatment_mean']>7.75)].count()[0]
    return entry

#%% init
    
log.info('Load data')

# loads the main data csv
data_df = pd.read_csv(join(target_dir_path,dataFile),'\t',decimal=',')

# creates a new column with the year and month for grouping/filtering
data_df['year-month'] = pd.to_datetime(data_df.loc[:,'date'],format='%Y.%m.%d').dt.strftime('%Y_%m')

# a list of the existing months for iterations
months_list = list(data_df['year-month'].unique())

# a filtered version of the main DataFrame without data in ADVITOS (or older name Hepa Wash)
field_df = data_df[ 
                    (data_df['locationMD'] != "") &
                    (~(data_df['locationMD'].str.contains('Hepa Wash',na=True, case=False)\
                       | data_df['locationMD'].str.contains('Advitos',na=True, case=False)))].copy()

# 
sheets_list = {}

# Gesamtfehlerliste Datei
log.info('Load GFL...')
GFL_tb = pd.read_excel(
        download_file(filepath_GFL,overwrite=True),
        sheet_name='GFL',header=5,index_col='Pos.')
GFL_BA =  GFL_tb[GFL_tb['Behandlungsabbruch'] == 'Ja']

# List of system errors
log.info('Load List of system errors...')
ListOSE = pd.read_excel(
        download_file(filepath_ListOfSystemErrors,overwrite=True),
        header=1,index_col=0)

# ERP Tables
log.info('Load ERP Tables...')
ERP_file = download_file(filepath_ErpConnection,overwrite=True)
ERP_locations = pd.read_excel(
        ERP_file, sheet_name='locations',
        header=0,index_col=0)
ERP_mReallocations = pd.read_excel(
        ERP_file, sheet_name='mReallocations',
        header=0,index_col=0)
ERP_svcRepais = pd.read_excel(
        ERP_file, sheet_name='svc_repairs',
        header=0,index_col=0)

#%% Monatsbericht Gesamt
log.info('Start Monatsbericht')
Monatsbericht = pd.DataFrame()

for gr in months_list:

    stats = treatment_stats(field_df[field_df['year-month']==gr])
    
    year, month  = gr.split('_')
    year, month  = int(year) , int(month)
    
    stats['13_Anzahl Behandlungsabbruche'] = sum(
        (GFL_BA['Wann'].dt.year == year) & (GFL_BA['Wann'].dt.month == month))
        
    for key in stats.keys():
        Monatsbericht.loc[key,gr] = stats[key]

for key in list(stats.keys()):#+['15a_Service_reparaturen (ERP)']
    Monatsbericht.loc[key,'total'] = Monatsbericht.loc[key].sum()

Monatsbericht.sort_index(inplace=True)
sheets_list['01_Monatsbericht'] = Monatsbericht

#%% Clinic Totals
log.info('Start Clinictotals')
Clinictotals = pd.DataFrame()

for clinicID in [l for l in data_df['locationID'].unique() if l==l]:
    for month in months_list + ['total']:
        #month = '2019_06'

        if month != 'total':
            stats = treatment_stats(data_df[
            ((data_df['locationID'] == clinicID) &
            (data_df['year-month']==month)) ])
        
            stats['03_Jahr'], stats['02_Monat']  = month.split('_')
        else:
            stats = treatment_stats(data_df[data_df['locationID'] == clinicID])
            
            stats['03_Jahr'], stats['02_Monat']  = ('total','total')

        try:
            strClinicID = '{:04}.001'.format(clinicID)
            stats['00_Clinic_Name'] = ERP_locations[\
             strClinicID]['company']
        except:
            stats['00_Clinic_Name'] = ''
        stats['01_Clinic_ID'] = strClinicID
        
        Clinictotals = Clinictotals.append(stats,ignore_index=True)

Clinictotals.sort_values(['00_Clinic_Name','03_Jahr','02_Monat'], inplace=True)
sheets_list['02_Clinictotals'] = Clinictotals

#%% Machine-SN totals
log.info('Start SNTotals')
SNTotals = pd.DataFrame()

for machine in data_df['machine'].unique():
    
    for month in months_list + ['total']:
        
        if month != 'total':
            stats = treatment_stats(data_df[
                    (data_df['machine'] == machine) & (data_df['year-month']==month)])
        else:
            stats = treatment_stats(data_df[
                    data_df['machine'] == machine])

        stats['00_SN'] = machine
        stats['01_month'] = month
        SNTotals = SNTotals.append(stats,ignore_index=True)

SNTotals.sort_values(['00_SN','01_month'], inplace=True)
sheets_list['03_SNTotals'] = SNTotals

#%% USB-Status
log.info('Start USB-Status')
USB_status = pd.DataFrame()

for machine in data_df['machine'].unique():
    machine_df = data_df[data_df['machine'] == machine]
    newline = pd.Series()
    # machine = 'LK2001-031'
    first_date = machine_df['date'].min()
    end_date = machine_df['date'].max()
    
    newline.loc['MachineSN'] = machine
    newline.loc['Last Location ERP'] = machine_df['locationMD'][machine_df['date'] == end_date].values[0]
    newline.loc['Last Location MD'] = machine_df['locationERP'][machine_df['date'] == end_date].values[0]
    newline.loc['first date'] = first_date
    newline.loc['last date'] = end_date
    
    last_date = datetime.datetime.strptime(end_date,'%Y.%m.%d')
    max_date = pd.to_datetime(data_df.loc[:,'date'],format='%Y.%m.%d').max().date()
    #date_ref = datetime.date(max_date.year,max_date.month,1)-datetime.timedelta(days=1)
    
    if last_date.date() > (max_date - datetime.timedelta(days=5)):
        newline.loc['status'] = 'up-to-date'
    elif last_date.date() > (max_date - datetime.timedelta(days=10)):
        newline.loc['status'] = 'slightly outdated'
    elif last_date.date() > (max_date - datetime.timedelta(days=30)):
        newline.loc['status'] = 'outdated'
    else:
        newline.loc['status'] = 'very outdated'    
    
    USB_status = USB_status.append(newline, ignore_index=True)

USB_status.sort_values(['last date'],inplace=True,ascending=False)
sheets_list['05_USB-Status'] = USB_status
#%% Error Overview
log.info('Start errorOverview')
errorOverview = pd.DataFrame()

for error in ListOSE[ListOSE['Severity'] != 'Info'].index:

    # describe the error
    for nr, desc in  enumerate(ListOSE.columns):
        errorOverview.loc[error,str(nr) + '_'+desc] = ListOSE.loc[error][desc]
    
    # go through every '*error_ids*' column and count occurrences
    for colNr, error_col in enumerate([x for x in data_df.columns if 'errors_ids' in x],start=10):
        errorOverview.loc[error,'{:02d}_{}'.format(colNr,error_col[:-4])] = \
        sum(data_df[error_col].astype(str).str.contains(str(error)).fillna(False))    

# sort and save in the sheets_list
total_col = (errorOverview.columns[errorOverview.columns.str.contains( 'total_errors')])[0]
errorOverview.sort_values([total_col],ascending=False,inplace=True)
sheets_list['06_Error-Overview'] = errorOverview

#%% Error Correlation
log.info('Start errorOverview')
errorCorrelation = pd.DataFrame()

error_col = field_df['total_errors_ids']

# drop all cases where there was a service or is not in the field
error_col = error_col.drop(error_col[
        (field_df['locationMD'].str.contains('ADVITOS')) |\
        (field_df['locationMD'].str.contains('Hepa Wash')) |\
        (field_df['service_time'] > 0)].index)
error_col.dropna(inplace=True)

# generate a table with "dummy" columns, where 1 represents it occurred
# also translated errorNr to errorCode
data_table = pd.DataFrame()
noErrCode = set()
log.info('... Create dummy columns')
for rowNr in range(len(error_col.index)):
    for error in error_col.iloc[rowNr].split('-'):
        if sum(error_col.str.contains(error)) < MIN_ERRORCOUNT:
            continue
        
        #def getValues_byErrNr(self,errNr,*columns): # errCode,args = 'BI-05',['severity','weight']
        errCode = try_GetVal(ListOSE, int(error), 'Error ID')
        
        if errCode:
            data_table.loc[error_col.index[rowNr], errCode] = 1
        else:
            noErrCode.add(error)
        
if noErrCode:
    log.error('No error Code for: {}'.format(noErrCode))
data_table = data_table.fillna(0)
# create correlations table 
log.info('... Create and Sort Correlation')
corr_table = data_table.corr()

# create a table with descending error correlations for each error
errorCorrelation
reindex = [] # to create a index with error-code and description
for err in corr_table.index:
    pass
    err_index = '{}:{}'.format(err,try_GetVal(ListOSE, err, 'Error Text Service ', column_index='Error ID'))
    colnr = 1 # keeps track of the current column
    
    reindex.append(err_index)
    
    for relatedErr in corr_table[err].sort_values(ascending=False).keys():
        relatedErr_desc = try_GetVal(ListOSE, relatedErr, 'Error Text Service ', column_index='Error ID')
        
        if err != relatedErr and corr_table[err][relatedErr] > MIN_CORR:
            errorCorrelation.loc[err_index,'{}-Fehler'.format(colnr)] = '{}:{}'.format(
                    relatedErr,relatedErr_desc)
            errorCorrelation.loc[err_index,'{}-korrelation(%)'.format(colnr)] = corr_table[err][relatedErr]
            colnr +=1 
            
errorCorrelation.name = 'Fehler Code'
errorCorrelation.sort_values(['1-korrelation(%)'],ascending=False,inplace=True)          
sheets_list['07a_Error-Correlation-List'] = errorCorrelation
corr_table.index = reindex
sheets_list['07b_Error-Correlation-Matrix'] = corr_table

#%% Generate excel file
log.info('Save ClinicStatistics to file')
mindate = pd.to_datetime(data_df.loc[:,'date'],format='%Y.%m.%d').min().strftime('%Y%m%d')
maxdate = pd.to_datetime(data_df.loc[:,'date'],format='%Y.%m.%d').max().strftime('%Y%m%d')

EXwriter = pd.ExcelWriter(join(*[
        target_dir_path,
        mindate + '-' + maxdate + '-' +clinicStatistics_file]))

for sheet in sheets_list:
    sheets_list[sheet].to_excel(EXwriter,sheet_name=sheet)
data_df.to_excel(EXwriter,'99_RohData')

EXwriter.save()

#%% Service Auswertung
# this extra file is formatted to be imported to ERP
log.info('Save ServiceTotals to file')
ServiceTotals = Clinictotals[['00_Clinic_Name', '01_Clinic_ID', '02_Monat', '03_Jahr',
        '10_Anzahl Behandlungen', '11_Anzahl kurze Behandlungen',
        '12_Anzahl lange Behandlungen', '13_Anzahl Behandlungsabbruche']].copy()

ServiceTotals = ServiceTotals[ServiceTotals['02_Monat'] != 'total']
ServiceTotals.rename(columns={"10_Anzahl Behandlungen": "04_Anzahl Behandlungen",
                              "11_Anzahl kurze Behandlungen": "05_Anzahl kurze Behandlungen",
                              "12_Anzahl lange Behandlungen":"06_Anzahl lange Behandlungen",
                              "13_Anzahl Behandlungsabbruche":"07_Anzahl Behandlungsabbruche"},
                inplace=True)

ServiceTotals.to_excel(join(target_dir_path, mindate + '-' + maxdate + '-Serviceauswertung.xlsx'))

#%% Error Prio
evalErrorPriority()

#%% update performance indexes
performances = pd.read_excel(join(res_dir_path,performances_file), index_col=0)
for month in Monatsbericht.columns[:-1]: # last column is "total" 
    date = pd.to_datetime(month,format='%Y_%m')
    performances.loc[date,'treatments'] = Monatsbericht[month]['10_Anzahl Behandlungen']
    performances.loc[date,'abortions'] = Monatsbericht[month]['13_Anzahl Behandlungsabbruche']
performances.to_excel(join(res_dir_path,performances_file))

#%%
log.info('Done')
