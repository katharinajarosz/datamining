# -*- coding: utf-8 -*-

"""
Created on Thu Apr 11 08:58:37 2019

@author: rahe

Creates a report to identify the most critical errors in the machine
comparing errors from the "Gesemtfehelerliste (GFL), the errorTable (Csv-files)
and service operations (ERP - System)

"""

import pandas as pd
import datetime

import sys, os
sys.path.append('..')

from FileHandling.DMLoggers import log

from ExternalConnections.FileDownload import download_file

from Constants import target_dir_path, res_dir_path, performances_file,\
    filepath_GFL, filepath_ListOfSystemErrors, filepath_ErpConnection
   
#%%
# - - - - - - - - - - -
#
#        Parameters
#
# - - - - - - - - - - -

#params
date_start = '01.01.2018'
date_end = '30.12.2022'
OVERWRITE = True

errorTable_path = os.path.join(target_dir_path,'errorTable.csv')

#files out
results_Excel_file =  os.path.join(target_dir_path,'ErrorPrio_results.xlsx')
fehlerauswertung_filename = 'Fehlerauswertung'

#%%
def download_sources(overwrite_=True):
    
    log.info('Download GFL...')
    path_gfl = download_file(filepath_GFL,overwrite=overwrite_)
    
    # ErrorTable
    log.info('Download errorTable...')
    path_errorTable = download_file(errorTable_path,overwrite=overwrite_)

    # List of system errors
    log.info('Download List of system errors...')
    path_ListOfSE = download_file(filepath_ListOfSystemErrors,overwrite=overwrite_)

    # ERP file
    log.info('Download ERP file...')
    path_ERPfile = download_file(filepath_ErpConnection,overwrite=overwrite_)
    
    return path_gfl, path_errorTable, path_ListOfSE, path_ERPfile

def try_findIndex(Table,indexVal,col):
    ## lookup for a index in a table and return a column value with try-catch
    if indexVal.isspace():
        return float('nan')
    
    try:
        return Table.loc[indexVal][col]
    except:
        return float('nan')

#%%
def evalErrorPriority():
    log.debug('Start evalErrorPriority')
    
    # Download and read source files
    Path_GFL, Path_ErrorTable, Path_ListOfSystemErrors, Path_ERPTable = download_sources(OVERWRITE)
    gfl_tb = pd.read_excel(Path_GFL,sheet_name='GFL',header=5,index_col='Pos.')
    errorTable = pd.read_csv(Path_ErrorTable,decimal=',',sep='\t')
    
    ListOfSystemErrors = pd.read_excel(Path_ListOfSystemErrors, header=1, index_col=0)
    
    ERP_locations = pd.read_excel(Path_ERPTable, sheet_name='locations',
            header=0,index_col=0)
    ERP_mReallocations = pd.read_excel(Path_ERPTable, sheet_name='mReallocations',
            header=0,index_col=0)
    ERP_svcRepais = pd.read_excel(Path_ERPTable, sheet_name='svc_repairs',
            header=0,index_col=0)    
    
    ## add severity and abortion to the errorTable
    severityTB = ListOfSystemErrors[['Error ID','Severity','Abort']]
    severityTB.set_index('Error ID',inplace=True)
    
    errorTable['Severity'] = errorTable['error_code'].apply(lambda x: try_findIndex(severityTB, x, 'Severity'))
    errorTable['Abortion'] = errorTable['error_code'].apply(lambda x: try_findIndex(severityTB, x, 'Abort'))
    
    # - - - - - - - - - - -
    #
    #        Create table with Params
    #
    # - - - - - - - - - - -
    params = pd.DataFrame.from_dict({
        'Date filter Start':date_start,
        'Date filter End':date_end,
        'Results file':results_Excel_file,
        'GFL_path':filepath_GFL,
        'ErrorTable_path':errorTable_path
        },orient='index',columns=['value'])
    
    # log parameters
    log.info('Start ErrorPriority')
    log.info(sys.executable)
    for k,v in params.iterrows():
        log.info('Parameter(%s : %s)'%(k,v['value']) )
    
    # - - - - - - - - - - -
    #
    #%%      ERP
    #
    # - - - - - - - - - - -
    log.debug('Start Erp evaluation')
    # get the table with the service repair operations
    svc = ERP_svcRepais
    # filter the dates and remove new lines and tabs (to save as csv or excel properly)
    svc = svc.fillna('').replace(r'\n',' - ', regex=True).replace(r'\t',' - ', regex=True)
    begin= pd.to_datetime(date_start, format="%d.%m.%Y")
    end = pd.to_datetime(date_end, format="%d.%m.%Y")
    svc = svc[(svc['Datum Auftrag'] > begin) & (svc['Datum Auftrag'] < end) &
              (svc['Einsatzart'] == 'Reparatur')]   
    
    # replaces empty strings ^$, space characters ^\s, and just ";" by NaN
    tmp = svc['schlagwoerter'].replace('^$|^\s|^;', float('nan'), regex=True).dropna()
    
    svc_totals = {}
    for keywordsStr in tmp.values:
        for keyword in keywordsStr.split(';'):
            if keyword:
                if keyword in svc_totals.keys():
                    svc_totals[keyword] += 1
                else:
                    svc_totals[keyword] = 1
    
    #svc_totals = svc.Schlagwort.value_counts()
    
    svc_result = pd.Series(svc_totals).sort_values(ascending=False)
    
    log.debug('Finished Erp evaluation. Evaluated %d'%(len(svc)))
    # - - - - - - - - - - -
    #
    #%%       GFL
    #
    # - - - - - - - - - - -
    log.debug('Start GFL evaluation')
    
    gfl_tb['Wann'] = pd.to_datetime(gfl_tb['Wann'], format="%Y-%m-%d")
    
    #filter date
    gfl_tb = gfl_tb[gfl_tb['Wann'] >= pd.to_datetime(date_start,format='%d.%m.%Y')]
    gfl_tb = gfl_tb[gfl_tb['Wann'] <= pd.to_datetime(date_end,format='%d.%m.%Y')]
    # filter only treatment abortions
    gfl_tb = gfl_tb[gfl_tb['Behandlungsabbruch']=='Ja']
    
    keyWBA2109 = gfl_tb['Kategorie-Schlagwort\nCatchword'].value_counts()
    gfl_result = (keyWBA2109).sort_values(ascending=False)
    log.debug('Finished GFL evaluation. Evaluated %d'%(len(gfl_tb) ) )
    # - - - - - - - - - - -
    #
    #%%        Error Table
    #
    # - - - - - - - - - - -
    log.debug('Start ErrorTable evaluation')
    list_OSE = ListOfSystemErrors
    
    errorTable.columns
    # filter dates
    errorTable = errorTable[pd.to_datetime(errorTable['date'],format='%Y_%m_%d',errors='coerce')
               >= pd.to_datetime(date_start,format='%d.%m.%Y')]
    errorTable = errorTable[pd.to_datetime(errorTable['date'],format='%Y_%m_%d',errors='coerce')
               <= pd.to_datetime(date_end,format='%d.%m.%Y')]
    errorTable = errorTable[errorTable['System_State'] != 'Service']
    errorTable = errorTable[errorTable['error_code'] != ' ']
    
    def lookup(table,lookup_col,lookup,ret_col):
        ## a function to apply to the 'error codes' in order to get the corresponding
        # values from the list of system errors
        try:
            return table[table[lookup_col] == lookup][ret_col].values[0]
        except Exception as e:
            return float('nan')
    
    errorTable_result = pd.DataFrame()    
    ##errorTable_result['error_sum'] = pd.Series(errorTable.groupby('error_code')['error_weight'].sum())
    for code in [x for x in errorTable['error_code'].unique() if x]: # code='IH-19'
        errorTable_result.loc[code,'keyword'] = lookup(list_OSE,'Error ID',code,'Catchphrase')
        errorTable_result.loc[code,'Severity'] = lookup(list_OSE,'Error ID',code,'Severity')
        errorTable_result.loc[code,'weight'] = lookup(list_OSE,'Error ID',code,'weight')
        errorTable_result.loc[code,'Error Text (Service)'] = lookup(list_OSE,'Error ID',code,'Error Text (Service)')
        errorTable_result.loc[code,'Solution for Service'] = lookup(list_OSE,'Error ID',code,'Solution for Service')
        errorTable_result.loc[code,'error_sum'] = errorTable_result.loc[code,'weight'] * sum(errorTable['error_code'] == code)
        #errorTable_result.loc[code,'Gewichtung'] = 1 # TODO add the weights into Database
    #getColumn(list_OSE,'IH-87','keyword')
    # Add gewichtung, keyword and severity to each error
    # filter Warnings, Infos and Service mode
    MD_tb = errorTable_result[
        #(errorTable_result['Severity'] != 'Warning') &
        (errorTable_result['Severity'] != 'Info') &
        (errorTable_result['Severity'] != '')]
    MD_tb = MD_tb.sort_values(by='error_sum',ascending=False)
    keyW_errT = MD_tb.groupby('keyword')['error_sum'].sum()
    MD_result = (keyW_errT).sort_values(ascending=False)
    
    log.debug('Finished ErrorTable evaluation. Evaluated %d'%(len(keyW_errT)))
    
    # - - - - - - - - - - -
    #
    #%%        Summary
    #
    # - - - - - - - - - - -
    keyword_list = list_OSE.groupby('Catchphrase')['Error ID'].apply(list)
    
    summary = pd.concat([
        keyword_list,
        pd.DataFrame(MD_result.rename('MD')),
        pd.DataFrame(gfl_result.rename('GFL')),
        pd.DataFrame(svc_result.rename('SVC'))
            ], axis=1, sort=False).fillna(0)
    
    summary['MD%'] = summary['MD']/summary['MD'].sum()
    summary['GFL%'] = summary['GFL']/summary['GFL'].sum()
    summary['SVC%'] = summary['SVC']/summary['SVC'].sum()
    
    summary['total'] = summary['MD']/summary['MD'].sum()+summary['GFL']/summary['GFL'].sum()+summary['SVC']/summary['SVC'].sum()
    summary = summary.sort_values('total',ascending=False)
    
    # - - - - - - - - - - -
    #
    #        Save to file
    #
    # - - - - - - - - - - -
    res_list = [('Summary',summary),
                ('Params',params),
                ('MD_data',MD_tb),
                ('GFL_data',gfl_tb),
                ('SVC_data',svc),
                ('List_of_System_errors',list_OSE)]
    
    log.debug('Load to excel writer'+results_Excel_file)
    writer = pd.ExcelWriter(results_Excel_file, engine='xlsxwriter')
    
    for name,tb in res_list:
        log.debug('load %s'%(name))
        tb.to_excel(writer, sheet_name=name)
    
    log.debug('Save...')
    writer.save()
    
    ## save errorTable to file
    # adjust date format
    dates = pd.to_datetime(errorTable['date'], yearfirst=True, format='%Y_%m_%d')
    mindate = dates.min().strftime('%Y%m%d')
    maxdate = dates.max().strftime('%Y%m%d')
    errorTable['date'] = dates
    
    #save
    EXwriter = pd.ExcelWriter(
        os.path.join(*[target_dir_path,
        mindate + '-' + maxdate + '-' +fehlerauswertung_filename+'.xlsx']))

    errorTable.to_excel(EXwriter,sheet_name='errorTable')
    ListOfSystemErrors[
        ListOfSystemErrors['Error ID'] != ''].to_excel(EXwriter,sheet_name='List_Of_System_Errors')

    EXwriter.save()
    
    #%% update performance values
    performances = pd.read_excel(os.path.join(res_dir_path,performances_file), index_col=0)
    
    errors = errorTable[(errorTable['Severity'] == 'Fatal') | (errorTable['Severity'] == 'Error')]
    errors = errors[ 
                    (errors['locationMD'] != "") &
                    (~(errors['locationMD'].str.contains('Hepa Wash',na=True, case=False)\
                       | errors['locationMD'].str.contains('Advitos',na=True, case=False)))]
    
    for yearmonth in errorTable['date'].dt.strftime('%Y_%m').unique():
        start_date = pd.to_datetime(yearmonth,format='%Y_%m')
        if start_date.month != 12:
            end_date = pd.Timestamp(year=start_date.year, month=start_date.month+1,day=1)
        else:
            end_date = pd.Timestamp(year=start_date.year+1, month=1,day=1)
        end_date -= pd.Timedelta(days=1)

        performances.loc[start_date,'errors'] = errors[(errors['date'] >= start_date) & (errors['date'] <= end_date)]['error_weight'].sum()
    performances.to_excel(os.path.join(res_dir_path,performances_file))
    
    log.debug('FINISH')
    
#%% __main__
if __name__ == '__main__':
    evalErrorPriority()