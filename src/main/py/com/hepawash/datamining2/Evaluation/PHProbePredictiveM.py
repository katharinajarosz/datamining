import pandas as pd
import numpy as np
import os

import sys
sys.path.append('..')
from FileHandling.DMLoggers import log
from Constants import target_dir_path

#%% funcions

def getTimepassed(row,merged_df,minError=0):
    ## return the time that has passes since:
    # - the first occurence of this SN of toError = False or
    # - the first time this SN has an error (negative if it is before the error)
    sn = row['03_probeSN']
    currentDate = pd.to_datetime(row['01_date'],format='%Y_%m_%d')
    
    firstDayAcidError = pd.to_datetime(\
        merged_df['01_date'][(merged_df['03_probeSN'] == sn) &
                 (merged_df['04_acid_errTime'] > minError)].min(),
        format='%Y_%m_%d')

    firstDayAlkaError = pd.to_datetime(\
        merged_df['01_date'][(merged_df['03_probeSN'] == sn) &
                 (merged_df['05_alka_errTime'] > minError)].min(),
        format='%Y_%m_%d')
        
    firstDay = pd.to_datetime(\
        merged_df['01_date'][merged_df['03_probeSN'] == sn].min(),
        format='%Y_%m_%d')
    
    disinfections = sum(pd.to_datetime(merged_df[(merged_df['03_probeSN'] == sn)]['01_date'],format='%Y_%m_%d') < currentDate)
    
    return (currentDate - firstDay).days, (currentDate - firstDayAcidError).days,(currentDate - firstDayAlkaError).days, disinfections

#%% __main__
if __name__ == '__main__':
    
    log.debug('Load mined data')
    file_data = pd.read_csv(os.path.join(target_dir_path,'phProbePredictive_DbscanScore 1.csv') ,decimal=',',sep='\t')
    
    # file_data = pd.read_csv(os.path.join(target_dir_path,'phProbePredictive_DbscanScore.csv') ,decimal=',',sep='\t')
    # data Evaluation
    log.debug('Start merging data')
    # merge errors of the different position
    merged_df = pd.DataFrame()
    merge_list = [('03_ACID_SN','07_citric_acid_err','14_alkal_acid_err'),
                  ('04_BASE_SN','08_citric_base_err','15_alkal_base_err'),
                  ('05_MON_SN','09_citric_mon_err','16_alkal_mon_err'),
                  ('06_RES_SN','10_citric_res_err','17_alkal_res_err')]
    
    for ix,row in file_data.iterrows():
        for sn,ac_err,alk_err in merge_list: 
            merged_df = merged_df.append(
                    {'03_probeSN':row[sn],
                     '03a_Position': sn.split('_')[1],
                     '04_acid_errTime':row[ac_err],
                     '05_alka_errTime':row[alk_err],
                     '02_machine':row['01_machine'],
                     '01_date':row['02_date']},
                     ignore_index=True)
            
    # calculates the time that has passed since first error, installed
    # and number of disinfections for each row
    log.debug('Start calculating time passed')
    for i,row in merged_df.iterrows():
        days, daysAcError, daysAlkError, disinfections = getTimepassed(row,merged_df,minError=0)    
    
        merged_df.loc[i,'05a_timeSince_AcidError'] = daysAcError
        merged_df.loc[i,'05b_timeSince_AlkaError'] = daysAlkError
        merged_df.loc[i,'06_timeSinceInstalled'] = days
        merged_df.loc[i,'07_disinfections'] = disinfections

    # calculate the monthly statistics
    log.debug('Start monthly statistics')
    minError = 0.1 # minimum time_error to consider as an error-probe
    
    merged_df['01a_datetime'] = pd.to_datetime(merged_df['01_date'],format='%Y_%m_%d')
    merged_df.sort_values('01a_datetime',inplace=True)
    
    monthlyStat = pd.DataFrame()
    # filters every probe
    for probeSN in merged_df['03_probeSN'].unique():
        probe_df = merged_df[merged_df['03_probeSN'] == probeSN]#probeSN=9027693
        
        for usedMachine in probe_df['02_machine'].unique():
            # divide for each machine
            probe_machine_df = probe_df[probe_df['02_machine'] == usedMachine]# usedMachine='LK2001-037'
            # create the x index for this machine-probe pair     
            idx_str = '{:.0f} {}'.format(probeSN, usedMachine)
            
            # add information of the last date of this probe-machine pair
            monthlyStat.loc[idx_str,'last_date'] = probe_machine_df['01a_datetime'].max().strftime('%Y.%m.%d')
            
            # group by month to calculate monthly stats
            grpd_probe_df = probe_machine_df.groupby(pd.Grouper(key='01a_datetime',freq='M'))
            
            for indx,cut_df in grpd_probe_df:
                # create y-index for this month
                month_str = indx.strftime(format='%Y-%m')
                
                # add the number of errors
                monthlyStat.loc[idx_str,month_str] =\
                                sum(cut_df['04_acid_errTime'] > minError) + \
                                sum(cut_df['05_alka_errTime'] > minError)
                
                # the total number of disinfections this month
                monthlyStat.loc[idx_str,month_str+'_total'] = \
                len(cut_df)
     
    #monthlyStat.to_excel('monthlyStat.xlsx')
                
    # for every machine, gets last known sensors and how long they are used
    log.debug('Mark time sensor is in use')
    for usedMachine in merged_df['02_machine'].unique():
        machine_df = merged_df[merged_df['02_machine'] == usedMachine]
        lastDate = machine_df['01a_datetime'].max()
        for probeInUse in machine_df[machine_df['01a_datetime'] == lastDate]['03_probeSN']:
            
            timeInUse = pd.datetime.today() \
            - machine_df[machine_df['03_probeSN'] == probeInUse]['01a_datetime'].min()
            
            monthlyStat.loc['{:.0f} {}'.format(probeInUse, usedMachine),'months_used'] = \
            timeInUse.days*12/365
   
    #%%
    # save to file
    outFile = os.path.join(target_dir_path,'phProbe_PreMaintenance.xlsx')
    log.debug('Save to file {}'.format(outFile))
    outWriter = pd.ExcelWriter(outFile, engine='xlsxwriter')
    
    # save monthly status with name and sorted
    monthlyStat.index.name = 'Evaluated: ' + pd.datetime.today().strftime('%Y.%m.%d')
    monthlyStat.sort_index(axis=1, inplace=True)
    monthlyStat.sort_values(by=['last_date'],ascending=False,inplace=True)
    monthlyStat.to_excel(outWriter,sheet_name='errors_monthly');
    
    
    merged_df.to_excel(outWriter,sheet_name='errors_merged');
    file_data.to_excel(outWriter,sheet_name='errors_disinfections');
    outWriter.save()
    log.debug('END')