# -*- coding: utf-8 -*-

#%% imports
import sys, os
import pandas as pd
import datetime

sys.path.append('..')
from Constants import target_dir_path
from FileHandling.DMLoggers import log

#%% constants
inDataFile_name = 'data.csv'

sizesReport_filename = 'sizesReport.xlsx'

#%% init

dataDF = pd.read_csv(os.path.join(target_dir_path, inDataFile_name),
                     sep='\t',decimal=',')

#%% state vars

#%% functions

def getLastDay(year,month):
    # returns the last day of the month (using just datetime module)
    nextmonth_date = datetime.datetime(year, month, 28) + datetime.timedelta(days=4)
    first_nextmonth = datetime.datetime(nextmonth_date.year, nextmonth_date.month, 1)
    return (first_nextmonth - datetime.timedelta(1)).day

def eval_month(dataDF, year=2019, month=6):
    ## Creates the statistics for every month
    #
    # @return Series with sum of:
    # - size_files, size_subfolders, size_rawData, machines
    # name of the series is YYYY_MM
    
    dates = pd.to_datetime(dataDF['date'],  format='%Y.%m.%d', yearfirst=True)
    
    start_date = datetime.datetime(year, month, 1)
    end_date = datetime.datetime(year, month, getLastDay(year, month))
    
    month_idx = (dates >= start_date)  & (dates < end_date)
    monthDF = dataDF[month_idx]
    
    item = pd.Series()
    item['size_files'] = (monthDF['size_files']/1024/1024).sum()
    item['size_subfolders'] = (monthDF['size_subfolders']/1024/1024).sum()
    item['size_rawData'] = (monthDF['size_rawData']/1024/1024).sum()
    item['machines'] = len(monthDF['machine'].unique())
    
    item.name='{}_{:02}'.format(year,month)
    
    # TODO add time analysis (total_time)
    return item
    
#%% main

if __name__ == '__main__':
    
    # to get every unique year-month combination  
    yearmonth = dataDF.apply(lambda x: (x['year'], x['month']),axis=1)
   
    final_report = pd.DataFrame()
    for ym in yearmonth.unique():
       final_report = final_report.append(eval_month(dataDF,ym[0],ym[1]))
    
    ExcWr = pd.ExcelWriter(os.path.join(target_dir_path,sizesReport_filename))
    
    final_report.to_excel(ExcWr,sheet_name='final_report')
    dataDF.to_excel(ExcWr,sheet_name='dataDF')
    
    ExcWr.save()
    
    for mach in dataDF['machine'].unique():
        files,subfolders,rawData = dataDF[['size_files','size_subfolders','size_rawData']][dataDF['machine'] == mach].sum()/1024/1024/1024
        
        log.info('{} file:{:.1f} subfd:{:.1f} raw:{:.1f}'.format(mach,files,subfolders,rawData))
        
        
