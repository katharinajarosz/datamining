#
# This scripts evaluates how much of the concentrates is thrown away by
# the clinics
#
# @author: RaHe
# @organization: ADVITOS GmbH

import os
import pandas as pd
import sys
import datetime

sys.path.append('..')
import MachineDay
from MachineDay import load_MD
from Datamining import is_valid_machine,is_valid_date
from FileHandling.DMLoggers import log
from Constants import source_path

print (sys.version,'\n',sys.executable)

from MainMining.HeaderMining import tryGetProperty
from Util import removeGermanUTF8Chars
import traceback

def get_concReplacements(data,conc):
    return data[(data[conc].diff() < 0) &
                (data['Treatment_State'] > 0)
                    ].index

def get_Treatment_start(data):
    return data[(data['Treatment_State'] >0) &
               (data['System_State'] == 4)
               ].index[0]

def get_intervall_stats(data,start,end,conc):
    filtered = data[start:end]
    #filtered = filtered[filtered['SystemState'] == 4]

    kf_median = (filtered[filtered['CL_SUPPLY_FLOW_E'] > 0])['CL_SUPPLY_FLOW_E'].median()
    kf_mean = (filtered[filtered['CL_SUPPLY_FLOW_E'] > 0])['CL_SUPPLY_FLOW_E'].mean()

    vol_conc = filtered[conc].max()
    rest = 5000-vol_conc
    
    return {'03_Conc':conc.split('_')[1],\
            '04_start':datetime.datetime.strftime(start,'%H:%M:%S'),\
            '05_end':datetime.datetime.strftime(end,'%H:%M:%S'),\
            '06_duration(hours)':'%.2f'%((end-start).total_seconds()/60/60),\
            '06a_patientTreatment(hours)': len(data[data['Treatment_State'] == 6])/60/60,\
            '07_kf_median':'%.1f'%kf_median,\
            '08_kf_mean':'%.1f'%kf_mean,\
            '09_vol_conc':'%.1f'%vol_conc,\
            '10_rest':'%.1f'%rest}

def concentrate_mining(machine_day):
    tdmsData = machine_day.TDMSdata
    if (len(tdmsData[tdmsData['Treatment_State'] == 4]) > 0):
        result = pd.DataFrame()
        
        for conc in ['C_ACID_VOLUME_CD','C_BASE_VOLUME_CD']:
            start = get_Treatment_start(machine_day.TDMSdata)
            end = get_Treatment_start(machine_day.TDMSdata[::-1])
            ac_replacements = get_concReplacements(machine_day.TDMSdata,conc)

            intervalls = []
            intervalls.append(start)
            for rep in ac_replacements:
                intervalls.append(rep)
            intervalls.append(end)

            for idx in range(1,len(intervalls)):
                result = result.append(           
                    get_intervall_stats(machine_day.TDMSdata,intervalls[idx-1],intervalls[idx],conc),ignore_index=True
                )
    else:
        return
    result['01_machine'] = machine_day.machine
    result['02_date'] = machine_day.date
    result['02a_location'] = removeGermanUTF8Chars(tryGetProperty(machine_day,'Machine location'))
    return result#.reindex(sorted(result.columns), axis=1)

def conc_makesummary(conc_data):
    print(conc_data)
    summary = pd.DataFrame()
    for m in (conc_data['01_machine']).unique():
        for d in conc_data[conc_data['01_machine'] == m]['02_date'].unique():
            entry = {}
            entry['00a_machine'] = conc_data['01_machine'].iloc[0]
            entry['00b_date'] = conc_data['02_date'].iloc[0]
            entry['00c_location'] =conc_data['02a_location'].iloc[0]
            
            treat_data = conc_data[(conc_data['01_machine'] == m) & (conc_data['02_date'] == d)]\
            .astype({'06_duration(hours)':'float',
                     '07_kf_median':'float',
                     '08_kf_mean':'float',
                     '09_vol_conc':'float',
                    '06a_patientTreatment(hours)':'float'})
            
            entry['01_duration'] = (treat_data['06_duration(hours)'].sum()/2)
            entry['01a_patientTreatment'] = (treat_data['06a_patientTreatment(hours)'].median())
            kf_values = treat_data['07_kf_median'].unique()
            
            entry['02_kf'] = kf_values[kf_values > 0]
            
            usedA = treat_data[treat_data['03_Conc'] == 'ACID']['09_vol_conc'].sum()
            entry['03_usedConcA'] = usedA
            
            usedB = treat_data[treat_data['03_Conc'] == 'BASE']['09_vol_conc'].sum()
            entry['04_usedConcB'] = usedB
            
            replacements = treat_data[treat_data['07_kf_median'].astype(float) > 0]
            
            entry['05_replacementsNr'] = len(replacements)
            
            entry['06_acid_replacements'] = treat_data[treat_data['03_Conc']=='ACID']['06_duration(hours)'].cumsum().astype(str).tolist()
            
            entry['07_base_replacements'] = treat_data[treat_data['03_Conc']=='BASE']['06_duration(hours)'].cumsum().astype(str).tolist()
            
            entry['08_restA'] = (len(replacements[replacements['03_Conc']=='ACID']))*5000-usedA
            print('Len replacementAC:%d'%(len(replacements[replacements['03_Conc']=='ACID'])))
            
            entry['08_restB'] = (len(replacements[replacements['03_Conc']=='BASE']))*5000-usedB
            
            summary = summary.append(entry,ignore_index=True)
            #print(summary)
    return summary
    
target_dir_path = '../../../../../../target/'

summary = pd.DataFrame()
details = pd.DataFrame()

machine_list = sorted([m for m in os.listdir(source_path) if is_valid_machine(source_path,m)])

#machine_list = ['LK2001-006']
for machine in machine_list:
    m_path = os.path.join(source_path,machine)

    date_list = sorted([d for d in os.listdir(m_path) if is_valid_date(source_path,machine,d)])
    for date in date_list:

        try:        
            machine_day = MachineDay.load_MD(source_path,machine, date)  

            conc_data = concentrate_mining(machine_day)
            if conc_data is not None:
                conc_summary = conc_makesummary(conc_data)
                
                summary = summary.append(conc_summary)
                details = details.append(conc_data)
            else:
                log.debug('No Treatment found')
        except Exception as e:
            traceback.print_exc()
        
summary.to_csv('H:/TEMP/summary.csv',sep='\t',decimal=',')
details.to_csv('H:/TEMP/details.csv',sep='\t',decimal=',')
print('FINISHED')