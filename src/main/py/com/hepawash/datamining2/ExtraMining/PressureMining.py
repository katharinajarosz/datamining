# -*- coding: utf-8 -*-

#
# Independend Analysis of the pressure in the machine
# The idea is identify when the machine has negative pressure that 
# could case air intake

import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.append('..')
import datetime
import os

import MachineDay

import Datamining

from FileHandling.DMLoggers import log

from Constants import SStates, source_path, dm_target

#%% constants

PMIN = -1000
PMAX = 1000
PBIN = 25

include_absMax = False
P_ABSMAX = 3500

foldername = 'pressureEval'

rangeformat = '{} {}'

#%% init

if include_absMax:
    p_intervalls = [(-P_ABSMAX,PMIN)] + [(p,p+25) for p in range(PMIN,PMAX,PBIN)] + [(PMAX,P_ABSMAX)]
else:
    p_intervalls = [(p,p+25) for p in range(PMIN,PMAX,PBIN)]

    
# definition of the channels to analyse
p_Channels = ['S_P_HW_ACID_CD','S_P_HW_BASE_CD','S_P_DIA_IN_CD','S_P_DIA_OUT_CD',
              'S_P_BLOOD_ART_CD', 'S_P_BLOOD_PRE_CD', 'S_P_BLOOD_VENOUS_CD']

    
histograms = pd.DataFrame()

idx = pd.MultiIndex.from_product(
        [[s for s in SStates.keys() if isinstance(s,int)],
          p_Channels]
        , names=['state', 'sensor'])

cols = [rangeformat.format(min_,max_) for min_,max_ in p_intervalls]

histograms = pd.DataFrame(0,index=idx, columns=cols )

#%%
       
def getNegPress_TimeInState(machine_day,chan,state):
    ## Returns the time in which the channel has a negative value
    # chan= 'S_P_HW_ACID_CD'; state=5
    return machine_day.TDMSdata[chan][
        (machine_day.TDMSdata[chan] < 0) & (machine_day.TDMSdata['System_State'] == state)
        ].count() / sum(machine_day.TDMSdata['System_State'] == state)

def getNegPress_valuesInState(machine_day,chan,state):
    ## Returns the pressure values for the state in the format:
    # (min, 25%perc, mean, 75%perc, max)
    desc = machine_day.TDMSdata[chan][
        (machine_day.TDMSdata[chan] < 0) & (machine_day.TDMSdata['System_State'] == state)
        ].describe()
    
    return desc['min'],desc['25%'], desc['mean'],desc['75%'], desc['max']

def getPress_valuesInState(machine_day,chan,state):
    ## Returns the pressure values for the state in the format:
    # (min, 25%perc, mean, 75%perc, max)
    desc = machine_day.TDMSdata[chan][(machine_day.TDMSdata['System_State'] == state)].describe()
    
    return desc['min'],desc['25%'], desc['mean'],desc['75%'], desc['max']

def minePressure(machine_day,target_dir_path=''):
    ## 
    # target_dir_path is for compatibility reasons
     
    negPress_times = pd.DataFrame()
    negPress_values = pd.DataFrame()
    
    # definition of the channels to analyse
    pChannels = ['S_P_HW_ACID_CD','S_P_HW_BASE_CD','S_P_DIA_IN_CD','S_P_DIA_OUT_CD']
    
    res_index = machine_day.machine + ' ' + machine_day.date

    # skip dict part where State->number, use only number->state
    for statenr,stateName in [(x,SStates[x]) for x in SStates if isinstance(x,int)]:
        for chan in pChannels:
            chanName = chan.split('_')[2] + chan.split('_')[3]
            
            # time evaluation
            timeColumnName = 'Tneg-' + stateName + '-' + chanName
            negPress_times.loc[res_index, timeColumnName] = getNegPress_TimeInState(machine_day,chan,statenr)
            
            # pressure evaluation
            pressureColPrefix = 'P-' + stateName + '-' + chanName
            pmin,p25,pmean,p75,pmax =  getPress_valuesInState(machine_day,chan,statenr)
            # (min, 25%perc, mean, 75%perc, max)
            
            negPress_values.loc[res_index, pressureColPrefix + '-min'] = pmin
            negPress_values.loc[res_index, pressureColPrefix + '-25%'] = p25
            negPress_values.loc[res_index, pressureColPrefix + '-mean'] = pmean
            negPress_values.loc[res_index, pressureColPrefix + '-75%'] = p75
            negPress_values.loc[res_index, pressureColPrefix + '-max'] = pmax
            
    return negPress_times, negPress_values
            
    #%%
def histPressure(machine_day, state_nr, chan):
    ## 
    # target_dir_path is for compatibility reasons
     
    # machine_day = md; state_nr = 4; chan='S_P_HW_BASE_CD'
    
    # filter the data in this state
    df_state = md.TDMSdata[md.TDMSdata['System_State'] == state_nr]

    
    vals = {}

    for p_min, p_max in p_intervalls:
        pass
        vals[rangeformat.format(p_min,p_max)] = sum ((df_state[chan] >= p_min) & (df_state[chan] < p_max))
    
    return vals
        
        
def mineHistPressure(machine_day): # machine_day = md
    log.debug(' '.join(['mineHistPressure',machine_day.machine, machine_day.date]))

    global histograms
    
    # for every state
    for stateNr in [s for s in SStates.keys() if isinstance(s,int)]:
        pass
    
        for channel in p_Channels:
            pass
            
            hist = histPressure(machine_day,stateNr,channel)
            
            for interv in hist:
                pass
                if hist[interv]:
                    histograms.loc[(stateNr,channel),interv] += hist[interv]
    
            
#%% __name__ == '__main__
if __name__ == '__main__':
    #%%

    log.info('Start Pressuremining')
    script_init_time = datetime.datetime.now()
    
    machine_list, _ = Datamining.findAll_machineDates(source_path)#sorted([m for m in os.listdir(source_path) if Datamining.is_valid_machine(source_path,m)])
    log.info('%d Valid machines found'%len(machine_list))
    
    print('Calculate to total number of folders...')
    analyzed_folder = 0
    total_folders = 0
    success_reads = 0
    
    # analyse the folders
    p_times = pd.DataFrame()
    p_values = pd.DataFrame()
    
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if Datamining.is_valid_date(source_path,machine,d)])
        for date in date_list:
            analyzed_folder +=1
            error_scripts = 0
            
            try: # machine = 'LK2001-016'; date = '2020_02_12'
                # If Load and Header have errors move to next machine-day
                md = MachineDay.load_MD(source_path,machine,date)
                if not md:
                    log.error('RUN FAIL: md-load %s %s'%(machine,date),exc_info=True)
                    continue
                
                mineHistPressure(md)
                
                #times,values = minePressure(md)
                #p_times  = p_times.append(times)
                #p_values = p_values.append(values)

            except Exception:
                log.error('Mine Error',exc_info=True)
                error_scripts += 1
            
            if not error_scripts:
                log.info('RUN SUCCESSFUL %s %s'%(machine, date))
                success_reads += 1
            else:
                log.info('RUN FAIL: mining %s %s : %s'%(machine, date, error_scripts))

       #histograms.loc[(2,'S_P_HW_ACID_CD')].plot.bar()
       
    log.info('Start plot')
    #%%
    os.makedirs(os.path.join(dm_target,foldername),exist_ok=True)
    
    histograms.columns
    for histIdx in histograms.index:
        pass # histIdx = (0,'S_P_HW_ACID_CD')
    
        name = '{}-{}.png'.format(SStates[histIdx[0]],histIdx[1])
        log.info('Plot: ' + name)
    
        fig, ax = plt.subplots()
        plt.rcParams.update({'font.size': 4})
        
        totalSecs = sum(histograms.loc[histIdx].values)

        ax.bar(histograms.loc[histIdx].index,histograms.loc[histIdx].values/totalSecs*100)
        plt.xticks(rotation=90)
        #for label in ax.get_xaxis().get_ticklabels()[::2]:
        #    label.set_visible(False)
        plt.title('{} {}'.format(SStates[histIdx[0]],histIdx[1]))
        ax.set_ylabel('Zeit (%)')
        ax.set_xlabel('Druck Bereiche')

        plt.savefig(os.path.join(dm_target,foldername,name),bbox_inches='tight',dpi=400)
        plt.close()
    log.info('Finnish')
#%%
#     raise StopIteration
    
# #%%
#     # create a overview table
#     # each line is a signal, columns are the percentiles [min,25%,avg,75%,max]
#     avgs_val = pd.DataFrame()
    
#     cols = set(['-'.join(x.split('-')[:-1]) for x in p_values.columns])
#     sufx = set([(x.split('-')[-1]) for x in p_values.columns])
#     for col in cols:
#         for suf in sufx:
#             colName = col+'-'+suf
#             avgs_val.loc[col,suf] = p_values[colName].mean()

#     avgs_time = pd.DataFrame()
#     for col in p_times.columns:
#         desc = p_times[col].describe()
#         avgs_time.loc[col,'min'] = desc['min']
#         avgs_time.loc[col,'25%'] = desc['25%']
#         avgs_time.loc[col,'mean'] = desc['mean']
#         avgs_time.loc[col,'75%'] = desc['75%']
#         avgs_time.loc[col,'max'] = desc['max']

# #%%

#     wr = pd.ExcelWriter(os.path.join(target_dir_path,'pressureEvaluation.xlsx'), engine='xlsxwriter')
#     p_times.to_excel(wr,sheet_name='time')
#     p_values.to_excel(wr,sheet_name='pressure')
#     avgs_val.to_excel(wr,sheet_name='avgs_val')
#     avgs_time.to_excel(wr,sheet_name='avgs_time')
#     wr.save()     
        
        
#%%
