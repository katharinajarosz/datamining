"""
@Author: Juan Fernandez

"""
import os
from datetime import datetime as dt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import multiprocessing
from scipy.stats import linregress

import MachineDay as MachineData
from FileHandling.DMLoggers import log

def add_prefix_to_columns(df,prefix):
    for column in df.columns:
        df[column+prefix] = df[column]
        df.drop(column,axis=1,inplace=True)
        
def slope_series(df):
    df_numerics = df.select_dtypes(include=['int16', 'int32', 'int64', 'float16', 'float32', 'float64'])
    return df_numerics.apply(lambda x: linregress(range(0,x.size),x)[0])

def mineCrossSwitch(machine_data):
    
    try:
        #Create a copy of the MachineData Dataframe
        df = machine_data.TDMSdata.copy()
        
        # Create the Cross-Switch State Column.
        # Only for PatientTreatment and Bypass
        df = df[((df['Treatment_State'] == 5)|(df['Treatment_State'] == 6))]
        
        #Continue Just if there is a treatment
        if not(df.empty):
            df['Cross_Switch_State'] = df['A_V3_CROSS_DEM'].apply(int).diff().apply(lambda x: 1 if x < 0 else x)
            df['Cross_Switch_State'] = df.apply(lambda row: row['Cross_Switch_State'] if ((row['Treatment_State'] == 5)|(row['Treatment_State'] == 6)) else 0,axis=1)
            df['Cross_Switch_State'] = df['Cross_Switch_State'].cumsum()
            
            # If the machine was in Treatment and there was more than one Cross-Switch
            # make the calculations and save them in the csv file
            if len(df['Cross_Switch_State'].unique())>1:
                
                # filter just the relevant channels
                df = df[(
                        [c for c in df.columns if ( c.startswith('CL_')
                        or c.startswith('C_') or c.startswith('S_'))
                        or c == 'Cross_Switch_State' ]
                        )]
                
                #Calculate the average,standard deviation and slope value of every cross-switch
                df_mean = df.groupby('Cross_Switch_State').apply(lambda x: np.mean(x)).iloc[1:-1]
                add_prefix_to_columns(df_mean,'_mean')
                df_std = df.groupby('Cross_Switch_State').apply(lambda x: np.std(x)).iloc[1:-1]
                add_prefix_to_columns(df_std,'_std')
                #df_slope = df.groupby('Cross_Switch_State').apply(lambda x: slope_series(x)).iloc[1:-1]
                #add_prefix_to_columns(df_slope,'_slope')
                
                df_cross_switch = pd.concat([df_mean,df_std],axis=1)
                #df_cross_switch = pd.concat([df_cross_switch,df_slope],axis=1)
                df_cross_switch['location'] = machine_data.locationERP
                df_cross_switch['date'] = dt.strptime(machine_data.date,'%Y_%m_%d')                
                df_cross_switch['machine'] = machine_data.machine
                df_cross_switch['software'] = machine_data.softwareVersion
                df_cross_switch = round(df_cross_switch,2)
                
                return df_cross_switch
                
    except Exception as e:
        log.error('mineCrossSwitch: %s'%e,exc_info=True)
        
if __name__ == '__main__':
    
    # DEBUG CODE
    machine = 'LK2001-039'
    date = '2018_06_05'
    source_path = 'X:/LogData_LK2001/'
    
    md = MachineData.load_MD(source_path,machine,date)
    
    mineCrossSwitch(md)
    # DEBUG CODE
    
    # filter just the relevant channels
    md.TDMSdata.columns.tolist()
    md.TDMSdata = md.TDMSdata[(
                        [c for c in md.TDMSdata if ( c.startswith('CL_')
                        or c.startswith('C_') or c.startswith('S_'))
                        or (c == 'Cross_Switch_State') ]
                        )]# md.TDMSdata['Cross_Switch_State']
    
    print("CrossSwitchMining.py Finished")
#%%    
    #csData = pd.read_csv('H:\\TEMP\\cs_data.csv',sep='\t',decimal=',')
    #print(csData.columns.tolist())
#%%
    #uniques = pd.DataFrame()
    #for col in csData.columns:
    #    uniques.loc[col,'uniques'] = len(csData[col].unique())
        
    #print(len(uniques)/3,len(uniques[uniques['uniques']!=1])/3)
    #len(csData.columns)
    #len([c for c in csData.columns if (
    #        c.startswith('CL_') or c.startswith('C_') or c.startswith('S_'))])
    
  