# -*- coding: utf-8 -*-

#
# Creates a History of which machine showing the states they were in and 
# the errors that occurred
#

#%% imports
import pandas as pd
import sys
import datetime

# import other Datamining Modules
sys.path.append('..')
from Constants import source_path, SStates
from MachineDay import load_MD
from Datamining import findAll_machineDates
from FileHandling.DMLoggers import log
from ExternalConnections.AdvosBI import advosBIinterface

from MainMining.HeaderMining import mineHeader

#%% Globals


#%% functions
def state_analysis(md): # machinde,date = 'LK2001-006','2019_01_02' ; md= load_MD(source_path,machine,date)
    
    states_df = pd.DataFrame()
   
    state_changes = md.TDMSdata[md.TDMSdata['System_State'].diff() != 0]
    
    for state_change_nr in range(len(state_changes.index)): # state_change_nr = 2
        
        new_data = pd.Series()
        
        ## - - - - - calculate the state, start and end
        
        #state_start = datetime.datetime.fromtimestamp(state_changes.index[state_change_nr].timestamp())
        state_start = state_changes.index[state_change_nr]
        
        new_data['10_State'] = SStates[md.TDMSdata.loc[state_start]['System_State']]
        
        if state_change_nr == len(state_changes.index)-1:
            # if its the last one, get the last timestamp
            state_end = md.TDMSdata.index.max()
        else:
            # otherwise get the timestamp *before* the start of next state
            # (this state may not end when the next begins, ex. machine turned off)
            next_start = state_changes.index[state_change_nr+1]
            int_loc = md.TDMSdata.index.get_loc(next_start)
            state_end = md.TDMSdata.iloc[int_loc-1].name

        new_data['20_state_start'] = datetime.datetime.fromtimestamp(state_start.timestamp())
        new_data['30_state_end'] = datetime.datetime.fromtimestamp(state_end.timestamp())
                        
        # total time
        new_data['40_total_time(h)'] = (state_end - state_start).total_seconds()/60/60
        
        ## - - - - - get the errors corresponding to the state
        error_times = pd.to_datetime(md.CSVdata['Time'],dayfirst=True)
        
        #datetime.datetime.fromtimestamp(treatment.index.min().timestamp())
        error_index_list = \
        (error_times >= datetime.datetime.fromtimestamp(state_start.timestamp())) & \
        (error_times <= datetime.datetime.fromtimestamp(state_end.timestamp()))

        
        # get all errors as a string, maintaining order and removing duplicates
        errors_str = ''
        for err in md.CSVdata[error_index_list]['Error ID'].values:
            if err not in errors_str:
                errors_str += err + ' '
                
        # column 5 is added by the main script
        new_data['60_errors(chronologic)'] = errors_str.strip()
        
        #new_data.name = datetime.datetime.fromtimestamp(state_start.timestamp()).strftime('%Y_%m_%d %H:%M:%S')
        
        states_df = states_df.append(new_data, ignore_index=True)

    states_df['20_state_start'] = pd.to_datetime(states_df['20_state_start'])
    states_df['30_state_end'] = pd.to_datetime(states_df['30_state_end'])

    return states_df


#%% init


#%% main
if __name__ == '__main__':
    log.info('Start MachineOverview.py')
    
    machineDates , nrOfMDs = findAll_machineDates(source_path)
    
    all_data = {}
    for machine in machineDates:
        
        final = pd.DataFrame()
        log.debug('Machine {}'.format(machine))
        
        for date in machineDates[machine]:

            try:        
                # machine, date = 'LK2001-006','2019_11_11'
                new_entry = pd.DataFrame()
                
                mday = load_MD(source_path,machine,date,load_tdms=True)
                
                if mday:
                    
                    header = mineHeader(mday)
                    states_data = state_analysis(mday) # has multiple rows
                    
                    states_data['01_locationMD'] = header['locationMD'].iloc[0]
                    states_data['02_locationERP'] = header['locationERP'].iloc[0]
                    states_data['03_filename_version'] = header['filename_version'].iloc[0]
                    states_data['04_parameter_file'] = header['parameter_file'].iloc[0]
                    
                    new_entry = new_entry.append(states_data)
                
                    final = final.append(new_entry)

            except:
                log.error('Machine-Day not evaluated:{} {}'.format(machine,date),exc_info=True)
        
        if not final.empty:
            # calulated the time before the state and converts to hours
            all_data[machine] = final.copy()
            all_data[machine].sort_index(axis=1,inplace=True) # sort columns
            all_data[machine]['50_time_off(h)'] = (all_data[machine]['20_state_start'] - all_data[machine]['30_state_end'].shift(1))
            all_data[machine]['50_time_off(h)'] = all_data[machine]['50_time_off(h)']\
                                                    .fillna(pd.Timedelta(seconds=0))\
                                                    .apply(datetime.timedelta.total_seconds)/3600
        
            all_data[machine]['20_state_start'].iloc[0]
    
    # add times without machine data # machine='LK2001-006'
    all_data[machine]['50_time_off'] = all_data[machine]['20_state_start'].shift(-1) - all_data[machine]['30_state_end']
    
    # Save to excel
    wr = pd.ExcelWriter('MachineOverview.xlsx',datetime_format='mmm d yyyy hh:mm:ss', options={'remove_timezone': True})
    for m in all_data:
        all_data[m].to_excel(wr,sheet_name=m)
    wr.save()
    