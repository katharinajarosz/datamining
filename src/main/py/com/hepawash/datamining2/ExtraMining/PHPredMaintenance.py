#%%
# This file calculates a predictive maintenance report to identify ph-sensors
# that are not measuring correctly in the field and are expected to stop working
# properly soon
#
#
# @author: RaHe
# @organization: ADVITOS GmbH

import pandas as pd
import numpy as np

from sklearn.cluster import DBSCAN

import sys, datetime, os

sys.path.append('..')
from FileHandling.DMLoggers import log
from MachineDay import load_MD
from Datamining import is_valid_date, is_valid_machine
from Constants import source_path, target_dir_path

#%% parameters

# DBSCAN
CLUSTER_DISTANCE = 0.2 # Distance of the ph-measurements DBSCAN-Cluster
MINSAMPLES = 1 # minimum samples inside the distance to consider a cluster

# minimum time_error to consider a disinfection state as an error
minError = 0.1

#%% functions


def get_ph_SN(md,pos):
    try:
        pos2name = {
            'C_PH_ACID_CD':'Sond_Acid',
            'C_PH_BASE_CD':'Sond_Base',
            'C_PH_MON_CD':'Sond_Monitor',
            'C_PH_RES_CD':'Sond_Reservoir'
                }
        
        return md.PHVdata['Serial Number'][md.PHVdata['Sond_Name'] == pos2name[pos]].values[-1]
    except:
        return float('nan')

def getBadSensors(values,eps_):
    ## Returns the number of clusters using DBSCAN algorthm
    
    db = DBSCAN(eps=eps_, min_samples=MINSAMPLES).fit(values.values.reshape(-1,1))
    counter = [0,0,0,0]
    undef = 0
    
    # Cluster labels for each point in the dataset given to fit().
    # Noisy samples are given the label -1.
    labels = db.labels_
    
#    for uniques in [i for i, occr in enumerate(labels) if occr == 1]:
#        counter[uniques] += 1
    
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    count_labels = pd.Series(labels).value_counts()
    
    for lab,cnt in count_labels.items():
        if cnt == 1:
            # if the sensor is alone in its cluster, mark it
            counter[labels.tolist().index(lab)] = 1
        if cnt == 2 and n_clusters_ == 2:
            # if there are two clusters with two elemts, mark as undefined
            undef = 1

    # if there is noise, mark too
    for i, val in enumerate(labels,0):
        if val == -1:
            counter[i] = 1

    return counter[0],counter[1],counter[2],counter[3],undef,n_clusters_

last_phv = pd.Series() # this stores the last known PHV values
def mine_DBSCAN_score(md):

    tdmsData = md.TDMSdata

    channels = ['C_PH_ACID_CD','C_PH_BASE_CD','C_PH_MON_CD','C_PH_RES_CD','Active Subsequence']
    # filter the right substates
    filtered = tdmsData[channels][
    (tdmsData['System_State'] == 6) &
    ((tdmsData['Disinfection_State'] == 1) | (tdmsData['Disinfection_State'] == 3) ) &
    ((tdmsData['Active Subsequence'] == 4) | (tdmsData['Active Subsequence'] == 38)) &
    ((tdmsData['CL_FLOW_PP_SU_BASE_DEM'] == 0) & (tdmsData['CL_FLOW_PP_SU_ACID_DEM'] == 0))]
    
    result = pd.DataFrame(index=[md.machine+' '+md.date] )
    result['01_machine'] = md.machine
    result['02_date'] = md.date
    
    # column 3 4 5 6
    for i,ch in enumerate(channels[:-1],3):
        columnName = '{:02}_{}_{}'.format(i,ch.split('_')[2],'SN')
        SNvalue = get_ph_SN(md,ch)
        
        if SNvalue == SNvalue: # if not Nan
            result[columnName] = SNvalue
            last_phv[columnName] = SNvalue
            last_phv.machine = md.machine
            last_phv.date = md.date
        else:
            try:
                if (md.machine == last_phv.machine) and \
                (pd.to_datetime(md.date,format='%Y_%m_%d') >  pd.to_datetime(last_phv.date,format='%Y_%m_%d')):
                    result[columnName] = last_phv[columnName]
            except:
                result[columnName] = float('nan')

    
    if filtered.empty:
        log.debug('No disinfection found')
        return pd.DataFrame()
    
    citric = filtered[['C_PH_ACID_CD','C_PH_BASE_CD','C_PH_MON_CD','C_PH_RES_CD']]\
    [filtered['Active Subsequence'] == 4]
    
    if not citric.empty:
        tmp = citric.apply(lambda row: getBadSensors(row,eps_=CLUSTER_DISTANCE),axis=1)
        unpack = tmp.apply(pd.Series)
        
        result['07_citric_acid_err'] = sum(unpack[0])/len(unpack)
        result['08_citric_base_err'] = sum(unpack[1])/len(unpack)
        result['09_citric_mon_err'] = sum(unpack[2])/len(unpack)
        result['10_citric_res_err'] = sum(unpack[3])/len(unpack)
        result['11_citric_undefined'] = sum(unpack[4])/len(unpack)
        result['12_citric_clusters_min'] = min(unpack[5])
        result['13_citric_clusters_max'] = max(unpack[5])
    else:
        result['07_citric_acid_err'] = float('Nan')
        result['08_citric_base_err'] = float('Nan')
        result['09_citric_mon_err'] = float('Nan')
        result['10_citric_res_err'] = float('Nan')
        result['11_citric_undefined'] = float('Nan')
        result['12_citric_clusters_min'] = float('Nan')
        result['13_citric_clusters_max'] = float('Nan')
    
    alkal = filtered[['C_PH_ACID_CD','C_PH_BASE_CD','C_PH_MON_CD','C_PH_RES_CD']]\
    [filtered['Active Subsequence'] == 38]
    
    if not alkal.empty:
        tmp = alkal.apply(lambda row: getBadSensors(row,eps_=CLUSTER_DISTANCE),axis=1)
        unpack = tmp.apply(pd.Series)
        result['14_alkal_acid_err'] = sum(unpack[0])/len(unpack)
        result['15_alkal_base_err'] = sum(unpack[1])/len(unpack)
        result['16_alkal_mon_err'] = sum(unpack[2])/len(unpack)
        result['17_alkal_res_err'] = sum(unpack[3])/len(unpack)
        result['18_alkal_undefined'] = sum(unpack[4])/len(unpack)
        result['19_alkal_clusters_min'] = min(unpack[5])
        result['20_alkal_clusters_max'] = max(unpack[5])
    else:
        result['14_alkal_acid_err'] = float('Nan')
        result['15_alkal_base_err'] = float('Nan')
        result['16_alkal_mon_err'] = float('Nan')
        result['17_alkal_res_err'] = float('Nan')
        result['18_alkal_undefined'] = float('Nan')
        result['19_alkal_clusters_min'] = float('Nan')
        result['20_alkal_clusters_max'] = float('Nan')
    
    return result

# merged_df_ = merged_df
def getTimepassed(row,merged_df_,minError=0):
    ## return the time that has passes since:
    # - the first occurence of this SN of toError = False or
    # - the first time this SN has an error (negative if it is before the error)
    sn = row['03_probeSN']
    currentDate = pd.to_datetime(row['01_date'],format='%Y_%m_%d')
    
    mergedDF_na = merged_df_.fillna(0)
    
    firstDayAcidError = pd.to_datetime(\
        mergedDF_na['01_date'][(mergedDF_na['03_probeSN'] == sn) &
                 (mergedDF_na['04_acid_errTime'] > minError)].min(),
        format='%Y_%m_%d')

    firstDayAlkaError = pd.to_datetime(\
        mergedDF_na['01_date'][(mergedDF_na['03_probeSN'] == sn) &
                 (mergedDF_na['05_alka_errTime'] > minError)].min(),
        format='%Y_%m_%d')
        
    firstDay = pd.to_datetime(\
        mergedDF_na['01_date'][mergedDF_na['03_probeSN'] == sn].min(),
        format='%Y_%m_%d')
    
    disinfections = sum(pd.to_datetime(merged_df[(merged_df['03_probeSN'] == sn)]['01_date'],format='%Y_%m_%d') <= currentDate)
    
    return (currentDate - firstDay).days, (currentDate - firstDayAcidError).days,(currentDate - firstDayAlkaError).days, disinfections

#%% MAIN 
if __name__ == '__main__':
    ## @cond INTERNAL

    log.info('Start pH Drift mine_all. Src:%s Target:%s'%(source_path,target_dir_path) )
    
    # for refference
    script_init_time = datetime.datetime.now()
    
    # get the machine list
    machine_list = sorted([m for m in os.listdir(source_path) if is_valid_machine(source_path,m)])

    log.info('%d Valid machines found'%len(machine_list))
    
    print('Calculate to total number of folders...')
    analyzed_folder = 0 # to not divide by zero
    total_folders = 0

    # calculate total number of folders
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if is_valid_date(source_path,machine,d)])
        total_folders += len(date_list)

    file_data = pd.DataFrame()
    # analyse the folders
    for machine in machine_list:
        m_path = os.path.join(source_path,machine)
        
        date_list = sorted([d for d in os.listdir(m_path) if is_valid_date(source_path,machine,d)])
        for date in date_list:
            analyzed_folder +=1

            log.info('Analyse %s %s. %d of %d (%d%%)'%(machine,date,analyzed_folder,total_folders,100*analyzed_folder/total_folders))
            
            # prevision of the end of the run
            log.info("ETA: %s"%datetime.datetime.strftime(
                (script_init_time + (total_folders/analyzed_folder)*(datetime.datetime.now()-script_init_time) )
                ,format='%Y-%m-%d %H:%M:%S'))
            
            # - - - - - - - - - - - - - - - - -
            #        MINING SCRIPTS
            # - - - - - - - - - - - - - - - - -
            try:
                md = load_MD(source_path,machine,date,load_tdms=True)
                if not md:
                    log.error('FALSE MD, NOT LOADED %s %s'%(machine,date))
                    continue
    
                # Handle errorTable.csv (report)
                for mine in [mine_DBSCAN_score]:
                    log.debug('Mine: '+mine.__name__)
                    new_Data = mine(md)
                    file_data = file_data.append(new_Data,sort=False,ignore_index=True)

            except Exception as e:
                log.error('MD NOT LOADED %s %s %s'%(machine,date,e),exc_info=True)
        
    file_data.to_csv(os.path.join(target_dir_path,'phProbePredictive_DbscanScore.csv') ,decimal=',',sep='\t')    
    
    
    #%% file_data = pd.read_csv(os.path.join(target_dir_path,'phProbePredictive_DbscanScore.csv') ,decimal=',',sep='\t')
    # data Evaluation
    log.debug('Start merging data')
    # merge errors of the different position
    merged_df = pd.DataFrame()
    merge_list = [('03_ACID_SN','07_citric_acid_err','14_alkal_acid_err'),
                  ('04_BASE_SN','08_citric_base_err','15_alkal_base_err'),
                  ('05_MON_SN','09_citric_mon_err','16_alkal_mon_err'),
                  ('06_RES_SN','10_citric_res_err','17_alkal_res_err')]
    
    for ix,row in file_data.iterrows():
        for sn,ac_err,alk_err in merge_list: 
            merged_df = merged_df.append(
                    {'03_probeSN':row[sn],
                     '03a_Position': sn.split('_')[1],
                     '04_acid_errTime':row[ac_err],
                     '05_alka_errTime':row[alk_err],
                     '02_machine':row['01_machine'],
                     '01_date':row['02_date']},
                     ignore_index=True)
            
    # calculates the time that has passed since first error, installed
    # and number of disinfections for each row
    log.debug('Start calculating time passed')
    for i,row in merged_df.iterrows():
        days, daysAcError, daysAlkError, disinfections = getTimepassed(row,merged_df,minError=minError)    
    
        merged_df.loc[i,'05a_timeSince_AcidError'] = daysAcError
        merged_df.loc[i,'05b_timeSince_AlkaError'] = daysAlkError
        merged_df.loc[i,'06_timeSinceInstalled'] = days
        merged_df.loc[i,'07_disinfections'] = disinfections

    # calculate the monthly statistics
    log.debug('Start monthly statistics')
    
    merged_df['01a_datetime'] = pd.to_datetime(merged_df['01_date'],format='%Y_%m_%d')
    merged_df.sort_values('01a_datetime',inplace=True)
    
    monthlyStat = pd.DataFrame()
    # filters every probe
    for probeSN in merged_df['03_probeSN'].unique():
        probe_df = merged_df[merged_df['03_probeSN'] == probeSN]#probeSN=9133914
        
        for usedMachine in probe_df['02_machine'].unique():
            # usedMachine = 'LK2001-021'
            
            # divide for each machine
            probe_machine_df = probe_df[probe_df['02_machine'] == usedMachine]# usedMachine='LK2001-043'
            # create the x index for this machine-probe pair     
            idx_str = '{:.0f} {} {}'.format(probeSN, usedMachine, probe_machine_df['03a_Position'].iloc[-1])
            
            # add information of the last date of this probe-machine pair
            monthlyStat.loc[idx_str,'last_date'] = probe_machine_df['01a_datetime'].max().strftime('%Y.%m.%d')
            
            # add information of the number of disinfetions of this probe
            monthlyStat.loc[idx_str,'disinfections'] = probe_machine_df['07_disinfections'].max()
            
            # group by month to calculate monthly stats
            grpd_probe_df = probe_machine_df.groupby(pd.Grouper(key='01a_datetime',freq='M'))
            
            for indx,cut_df in grpd_probe_df:
                # create y-index for this month
                month_str = indx.strftime(format='%Y-%m')
                
                # add the number of errors
                monthlyStat.loc[idx_str,month_str] =\
                                sum(cut_df['04_acid_errTime'] > minError) + \
                                sum(cut_df['05_alka_errTime'] > minError)
                
                # the total number of disinfections this month
                monthlyStat.loc[idx_str,month_str+'_total'] = \
                len(cut_df)
     
    #monthlyStat.to_excel('monthlyStat.xlsx')
                
    # for every machine, gets last known sensors and how long they are used
    log.debug('Mark time sensor is in use')
    for usedMachine in merged_df['02_machine'].unique():
        machine_df = merged_df[merged_df['02_machine'] == usedMachine]
        lastDate = machine_df['01a_datetime'].max()
        for probeInUse in machine_df[machine_df['01a_datetime'] == lastDate]['03_probeSN']:
            
            timeInUse = pd.datetime.today() \
            - machine_df[machine_df['03_probeSN'] == probeInUse]['01a_datetime'].min()
            
            monthlyStat.loc['{:.0f} {} {}'.format(probeInUse, usedMachine, machine_df[machine_df['03_probeSN'] == probeInUse]['03a_Position'].iloc[-1]),'months_used'] = \
            timeInUse.days*12/365
   
    #%
    # save to file
    outFile = os.path.join(target_dir_path,'phProbe_PreMaintenance.xlsx')
    log.debug('Save to file {}'.format(outFile))
    outWriter = pd.ExcelWriter(outFile, engine='xlsxwriter')
    
    # save monthly status with name and sorted
    monthlyStat.index.name = 'Evaluated: ' + pd.datetime.today().strftime('%Y.%m.%d')
    monthlyStat.sort_index(axis=1, inplace=True)
    monthlyStat.sort_values(by=['last_date'],ascending=False,inplace=True)
    monthlyStat.to_excel(outWriter,sheet_name='errors_monthly');
    
    
    merged_df.to_excel(outWriter,sheet_name='errors_merged');
    file_data.to_excel(outWriter,sheet_name='errors_disinfections');
    outWriter.save()
    log.debug('END')