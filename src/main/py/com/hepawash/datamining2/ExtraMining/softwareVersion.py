# -*- coding: utf-8 -*-
#
# Creates a history of the machines and which software version they used over
# time. Used to track releases/updates
#
# @author Rafael Hellmann

import os
import pandas as pd
import datetime

import sys
sys.path.append('..')
from FileHandling.DMLoggers import log
from Constants import source_path

#Src = 'X:\\LogData_LK2001'; Machine = 'LK2001-010'; date = '2019_12_31'
def eval_SWVersion(folder):
    #folder = os.path.join( *[Src, Machine, Date] )
    
    # 2019_01_16_08_04_28_LK2001-010_LK2001V3.9.0_r
    for tdmsFile in [f for f in os.listdir(folder) if '.tdms' == f[-5:]]:
        try:
            vString = tdmsFile.split('LK2001V')[1][:-5]
            return vString if '_r' in vString else '*'+vString
        except:
            pass
    return '*error'
#%%
def getKW(folder):
    # returs the 'KalenderWoche' for the specific folder
    try:
        date_dt = datetime.datetime.strptime(os.path.basename(folder),format('%Y_%m_%d'))
        
        # special treatment for KW1
        if date_dt.strftime('%V') == '01' and date_dt.strftime('%m') == '12':
            return date_dt.strftime('%y-52(Dec)')
       
        year = date_dt.strftime('%y')

        # adds the month from the first day of this KW        
        ISO_YKW = date_dt.strftime('%G-%V')
        strKW = (datetime.datetime.strptime(ISO_YKW+'-1','%G-%V-%u')).strftime(year+'-%V(%b)')
        
        return strKW
    except Exception as e:
        print(e)
        pass
    return 'err'
    
#
#folder = 'X:/2019_04_30'
#print (folder, getKW(folder))
#folder = 'X:/2019_05_01'
#print (folder, getKW(folder))
#folder = 'X:\\LogData_LK2001\\LK2001-006\\2019_01_02'
#print (folder, getKW(folder))
#folder = 'X:\\LogData_LK2001\\LK2001-036\\2019_12_31'
#print (folder, getKW(folder))

#%%
def createSummary(sw_Table):
    # creates a overview of how many machines are on each week
    # and which software version they use
    # @detail: ignores versions which '*'
    
    summary = pd.DataFrame()
    
    for kwCol in sw_Table.columns:
        summary.loc['total',kwCol] = sw_Table.loc[:,kwCol].count()
        
        # finding unique sw versions
        uniqueStr = sw_Table.loc[:,kwCol].dropna().unique()
        
        majorVersions = set()
        for version in uniqueStr:
            majorVersions.add('.'.join(version.split('.')[0:2]))
            
    
        for SWVers in majorVersions:
            if '*' in SWVers:
                continue
            summary.loc[SWVers,kwCol] = sw_Table.loc[:,kwCol].dropna().str.contains(SWVers).sum()
            
    return summary.sort_index()

#%%
if __name__ == '__main__':
    log.info('Start softwareVersion')
    #source_path = 'X:\\LogData_LK2001'
    
    swVersion_Table = pd.DataFrame()
    for machine in sorted([os.path.join(source_path,m) for m in os.listdir(source_path) if 'LK2001' in m]):
        log.debug('Machine:{}'.format(machine))
        for date in sorted([os.path.join(machine,d) for d in os.listdir(machine) if os.path.isdir(os.path.join(machine,d))]):
            version = eval_SWVersion(date)
            kw = getKW(date)
           
            swVersion_Table.loc[os.path.basename(machine),kw] = version
    
    log.info('Save to excel')        
    swVersion_Table = swVersion_Table.reindex(sorted(swVersion_Table.columns), axis=1)
    #swVersion_Table.to_excel('H:/H-tmp/SW-version.xlsx')
    
    summary = createSummary(swVersion_Table)
    
    excW = pd.ExcelWriter('H:/H-tmp/Eval-SWVersions.xlsx')
    swVersion_Table.to_excel(excW,sheet_name='machines')
    summary.to_excel(excW,sheet_name='summary')
    excW.save()    
            