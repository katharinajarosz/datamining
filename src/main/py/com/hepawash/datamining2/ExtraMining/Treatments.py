# -*- coding: utf-8 -*-
## This mining script creates a dataframe in which every row represents
# a treatment and every column is a property of this treatment
#
# The objective is to create a overview regarding how the machine is used in
# treatment (configurations) and errors
#
# author:rahe
#

#%% imports
import pandas as pd
import datetime
import sys
sys.path.append('..')

from Constants import source_path, dm_target

from os.path import join
from Datamining import findAll_machineDates
from MachineDay import load_MD

from FileHandling.DMLoggers import log
from ExternalConnections.AdvosBI import advosBIinterface

#%% Constants

# Defines which error-codes represent air-related errors
# TODO this should be replaced by the keyword 'luft im system'
air_error_list = ['IH-47','IH-49','IH-110','IH-108','IH-107','IH-50']

#%% inits

#%% functions


def treat_infos(md):
    ## These are generic informations about the treatment:
    # machine, date, length, etc
    
    mdData = md.TDMSdata[md.TDMSdata['System_State']==4]
    if mdData.empty:
        return
    
    else:
        log.debug('Eval treatment {} {}'.format(md.machine, md.date))
        
        result = pd.Series()
        
        #header
        result['1A_machine'] = md.machine
        result['1B_date'] = pd.datetime.strptime(md.date,'%Y_%m_%d').strftime('%Y.%m.%d')
        try:
            result['1C_locationMD'] = md.TDMSproperties['Machine location']
        except:
            result['1C_locationMD'] = ''
        result['1D_locationERP'] = md.locationERP
        
        # treatment evaluation
        result['1E_treatment_time(h)'] = (mdData.index.max()\
              - mdData.index.min()).total_seconds()/60/60
        result['1F_patienttreatment_time(h)'] = sum(mdData['Treatment_State'] == 6)/60/60
        result['1F_bypass_time(h)'] = sum(mdData['Treatment_State'] == 5)/60/60
        result['1F_disconect_time(h)'] = sum(mdData['Treatment_State'] == 11)/60/60
               
        result['1G_kf0_time(h)'] = sum(mdData['CL_SUPPLY_FLOW_DEM'] == 0)/60/60
        result['1H_kf160_time(h)'] = sum(mdData['CL_SUPPLY_FLOW_DEM'] == 160)/60/60
        result['1I_kf320_time(h)'] = sum(mdData['CL_SUPPLY_FLOW_DEM'] == 320)/60/60
        result['1J_kf(other)_time(h)'] = \
        len(mdData['CL_SUPPLY_FLOW_DEM'])/60/60 - result['1G_kf0_time(h)'] - result['1H_kf160_time(h)'] - result['1I_kf320_time(h)']
        
        # [x for x in mdData.columns if 'PH' in x]
        result['1K_PH_mean'] = mdData['CL_PH_RES_DEM'][mdData['Treatment_State'] >= 5].mean()
        
        return result


#%%

def getTimetoHWB150(machine_data):
    try:
        ## calculates the time until the HWB-flow reaches 150ml/min in treatment
        # this is a hint theat the ph-regulation is not working properly
        entry = pd.Series()
        
        # filter just treatment
        df = machine_data.TDMSdata[(machine_data.TDMSdata['System_State'] == 4) &
                                   (machine_data.TDMSdata['Treatment_State'] == 6)]
        
        # get the position of the first 150ml/min
        first150 = df['S_F_HW_BASE_CD'][(df['S_F_HW_BASE_CD'] < 151) & (df['S_F_HW_BASE_CD'] > 149)].index.min()
        
        # slice the machine data from beginning to first 150ml/min
        treatSlice = df.loc[:first150]
        treatSlice.index.min()
        # time until first 150ml/min
        
        treatSlice.columns
        entry['5A_time_toHWB150(min)'] = len(treatSlice)/60
        entry['5B_time_toHWB150_kf300(min)'] = sum(treatSlice['CL_SUPPLY_FLOW_DEM'] > 300)/60
        entry['5C_time_toHWB150_kf200(min)'] = sum((treatSlice['CL_SUPPLY_FLOW_DEM'] < 200) & (treatSlice['CL_SUPPLY_FLOW_DEM'] > 100))/60
        
        return entry
    except:
        entry['5A_time_toHWB150(min)'] = 0
        entry['5B_time_toHWB150_kf300(min)'] = 0
        entry['5C_time_toHWB150_kf200(min)'] = 0
        return entry
    
def get_concReplacements(data,conc):
    return data[(data[conc].diff() < 0) &
                (data['Treatment_State'] > 0) &
               (data['System_State'] == 4)
                    ].index

def get_Treatment_start(data):
    return data[(data['Treatment_State'] >0) &
               (data['System_State'] == 4)
               ].index[0]

def get_intervall_stats(data,start,end,conc):
    filtered = data[start:end]
    #filtered = filtered[filtered['SystemState'] == 4]
    vol_conc = filtered[conc].max()
    rest = 5000-vol_conc
    
    return {'03_Conc':conc.split('_')[1],
            '04_start':datetime.datetime.strftime(start,'%H:%M:%S'),
            '05_end':datetime.datetime.strftime(end,'%H:%M:%S'),
            '06_duration(hours)':(end - start).total_seconds()/60/60,
            '09_vol_conc':vol_conc,
            '10_rest':rest}


def concentrate_mining(machine_day):
    tdmsData = machine_day.TDMSdata[machine_day.TDMSdata['System_State'] == 4].copy()
    if (len(tdmsData[tdmsData['Treatment_State'] == 4]) > 0):
        conc_data = pd.DataFrame()
        
        tdmsData.columns
        
        ## find every concentrate replacement
        for conc in ['C_ACID_VOLUME_CD','C_BASE_VOLUME_CD']:
            pass
            start = get_Treatment_start(machine_day.TDMSdata)
            end = get_Treatment_start(machine_day.TDMSdata[::-1])
            ac_replacements = get_concReplacements(machine_day.TDMSdata,conc)

            intervalls = []
            intervalls.append(start)
            for rep in ac_replacements:
                intervalls.append(rep)
            intervalls.append(end)

            for idx in range(1,len(intervalls)):
                conc_data = conc_data.append(           
                    get_intervall_stats(machine_day.TDMSdata,intervalls[idx-1],intervalls[idx],conc),ignore_index=True
                )
       
        # summarize the replacements for this treatment
        entry = pd.Series()
        
        usedA = conc_data[conc_data['03_Conc'] == 'ACID']['09_vol_conc'].sum()
        entry['3A_usedConcA'] = usedA
        
        usedB = conc_data[conc_data['03_Conc'] == 'BASE']['09_vol_conc'].sum()
        entry['3B_usedConcB'] = usedB
       
        entry['3C_usedConcentrates'] = len(conc_data)
        
        acid_replacements = conc_data[conc_data['03_Conc']=='ACID']['06_duration(hours)'].cumsum()
        entry['3D_acid_replacements(hours)'] = ' '.join(['{:.1f}'.format(txt) for txt in acid_replacements])
        
        base_replacements = conc_data[conc_data['03_Conc']=='BASE']['06_duration(hours)'].cumsum()
        entry['3E_base_replacements(hours)'] = ' '.join(['{:.1f}'.format(txt) for txt in base_replacements])
        
        entry['3F_restA'] = (len(conc_data[conc_data['03_Conc']=='ACID']))*5000-usedA
        
        entry['3G_restB'] = (len(conc_data[conc_data['03_Conc']=='BASE']))*5000-usedB
        
        return entry
    else:
        return

def try_getmode(values,pos):
    
    try:
        return values.value_counts().index[pos]
    except:
        return ''
    

def errorsNr(md):
    ## Evaluates the errors in the treatment
    #
    try:
        new_data = pd.Series()
        
        # number of times 'error Quit' was clicked
        start = get_Treatment_start(md.TDMSdata)
        end = get_Treatment_start(md.TDMSdata[::-1])
        
        treat_uelog = md.UELOGdata
        treat_uelog['timestamp'] = pd.to_datetime(treat_uelog['date']+ ' ' + treat_uelog['time'],
                   format='%d.%m.%Y %H:%M:%S,%f', utc=True)
        treat_uelog = treat_uelog[(treat_uelog['timestamp'] > start)&(treat_uelog['timestamp'] < end)]
        new_data['4A_errorQuit_times'] = sum(treat_uelog['text'].str.contains('Error Quit was clicked'))
        
        treat_csv = md.CSVdata[md.CSVdata['Source Module'] == 'Treatment']
        
        new_data['4B_errors_top10'] = ''
        for i in range(10):
            new_data['4B_errors_top10'] = new_data['4B_errors_top10'] + '{} '.format(try_getmode(treat_csv['Error ID'],i))
        
        #new_data['4B_errors_top1'] = try_getmode(treat_csv['Error ID'],0)
        #new_data['4B_errors_top2'] = try_getmode(treat_csv['Error ID'],1)
        #new_data['4B_errors_top3'] = try_getmode(treat_csv['Error ID'],2)
        #new_data['4B_errors_top4'] = try_getmode(treat_csv['Error ID'],3)
        
        new_data['4C_errors_count'] = len(treat_csv['Error ID'])
        
        return new_data
    except:
        return pd.Series()

#%%

md = load_MD(source_path,'LK2001-017','2019_02_03') # neg
md = load_MD(source_path,'LK2001-016','2019_10_09') # pos


def timeAfter_airError(md):
    
    entry = pd.Series()
    
    try:
        tdmsData = md.TDMSdata
        csv_data = md.CSVdata
        
        airErrors = csv_data[csv_data['Error ID'].isin(air_error_list)]
        airErrors['Time'] = pd.to_datetime(airErrors['Time'],dayfirst=True)
        
        treatment = tdmsData[tdmsData['System_State'] == 4]
        
        if not (airErrors.empty or treatment.empty):
            # convert to timestamp to avaid comparing different timezones
            treat_start = datetime.datetime.fromtimestamp(treatment.index.min().timestamp())
            treat_end = datetime.datetime.fromtimestamp(treatment.index.max().timestamp())
            
            treat_airErrors = airErrors[(airErrors['Time'] > treat_start) & (airErrors['Time'] < treat_end)]
            
            last_airErr = treat_airErrors['Time'].max()
            
            timeDiff = treat_end - last_airErr
            
            entry['6A_timeAfter_airError(min)'] =  timeDiff.total_seconds()/60
        else:
            entry['6A_timeAfter_airError(min)'] =  float('Nan')
    except:
        entry['6A_timeAfter_airError(min)'] =  float('Nan')
        
    return entry

#%% main
if __name__ == '__main__':
    log.info('Start Treatments.py')
    
    machineDates , nrOfMDs = findAll_machineDates(source_path)
    
    final = pd.DataFrame()
    for machine in machineDates:
        log.debug('Machine {}'.format(machine))
        for date in machineDates[machine]:

            try:        
                # machine='LK2001-012'; date='2019_08_07'
                # machine='LK2001-026'; date='2019_07_12'
                md = load_MD(source_path,machine,date,load_tdms=False)
                
                if md and (md.TDMSmetadata['time_state_4'] > 0):
                    new_entry = pd.Series()
                    
                    log.debug('Treatment found:{}'.format(md.TDMSmetadata['time_state_4'] ))
                    md = load_MD(source_path,machine,date,load_tdms=True)
                    
                    new_entry = new_entry.append(treat_infos(md))
                    new_entry = new_entry.append(concentrate_mining(md))
                    new_entry = new_entry.append(errorsNr(md))
                    new_entry = new_entry.append(getTimetoHWB150(md)) # evaluation of time to HW-B to reach 150ml/min
                    new_entry = new_entry.append(timeAfter_airError(md))
                    
                    new_entry.name = '{} {}'.format(machine,date)
                    final = final.append(new_entry)

            except:
                log.error('Machine-Day not evaluated:{} {}'.format(machine,date),exc_info=True)


    
    exc_writer = pd.ExcelWriter(join(dm_target,'treatments-all.xlsx'))
    final.to_excel(exc_writer)
    exc_writer.save()
    
    # save in an excel by month
    final = pd.read_excel(join(dm_target,'treatments-all.xlsx'),index_col=0)
    
    #pd.to_datetime(final['1B_date'])
    ph_zero = final[final['1K_PH_mean'] == 0].index.tolist()

#%%

if False:    
    import pandas as pd
    from sklearn import tree, metrics
    
    allTreatments = pd.read_excel('H:\\_Technical\\Datamining\\AbortionList\\treatments-all.xlsx',index_col=0)
    allTreatments['Abortion'].fillna(0,inplace=True)
    
    clf = tree.DecisionTreeClassifier()
    
    allData = allTreatments.drop('Abortion',axis=1)
    ## filter non-numeric
    allData = allData[['1E_treatment_time(h)', '1F_patienttreatment_time(h)',
    '3A_usedConcA', '3B_usedConcB', '3C_usedConcentrates', '3F_restA', '3G_restB',
       '4A_errorQuit_times', '4C_errors_count']]
    
    # create a dummy for '4B_errors_1st', '4B_errors_2st', '4B_errors_3st', '4B_errors_4st'
    for idx_row in allData.index:
        for error_col in ['4B_errors_1st', '4B_errors_2st', '4B_errors_3st', '4B_errors_4st']:
            errCode = allTreatments.loc[idx_row,error_col]
            
            if errCode and errCode == errCode: # remove empty and nan
                allData.loc[idx_row, 'is_{}'.format(errCode)] = 1
    
    allData.fillna(0,inplace=True)
    
    allTarget = allTreatments['Abortion']
    len(allTreatments['Abortion'])
    
    maxIndex = round(len(allTreatments['Abortion'])*0.6)
    
    trainData = allData[:maxIndex]
    trainTarget = allTarget[:maxIndex]
    
    testData = allData[maxIndex:]
    testTarget = allTarget[maxIndex:]
    
    clf_fitted = clf.fit(trainData, trainTarget)
    
    prediction = pd.Series(clf.predict(testData),index=testData.index)
    
    print('Abortions:',sum(testTarget))
    print('predicted Abortions:',sum(prediction))
    print('Confusion Matrix:\n',metrics.confusion_matrix(testTarget,prediction))

#%%
    
if False:
    import pandas as pd
    from Constants import dm_target
    from os.path import join
    
    treat_all = pd.read_excel(join(dm_target,'treatments-all.xlsx'),index_col=0)
    
    notNA = treat_all.copy()
    notNA.loc[notNA['1K_PH_mean']<6] = float('NaN')
    notNA = notNA.dropna(subset=['1K_PH_mean'])
    
    bplot = notNA.boxplot(column='1K_PH_mean', by='1C_locationMD',vert=False,showfliers=False)
    
    clinics = treat_all['1C_locationMD'].unique()
    
    plottable = pd.DataFrame(columns=clinics)
    for clinic in clinics:
        clinicTreats = treat_all[treat_all['1C_locationMD'] == clinic]
        
        rownr=0
        for i, row in clinicTreats.iterrows():
            pass
            plottable.loc[rownr,clinic] = row['1K_PH_mean']
            rownr+=1
       
    plottable.boxplot(clinics[0])
    plottable.boxplot(column=clinics[0])
    
    #for loc in treat_all['1C_locationMD'].unique():
        #pd.DataFrame(treat_all['1F_patienttreatment_time(h)']\
        #[treat_all['1C_locationMD'] == loc]).boxplot()
    
    
    
