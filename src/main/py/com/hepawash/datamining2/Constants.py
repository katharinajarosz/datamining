##
# This files contain project-wide constants that can be imported everywhere
# avoiding circular imports and creating a central configuration
#
# @author: RaHe
# @organization: ADVITOS GmbH

import os

## Definition of the minimum size of a TDMS 'big file'
# files bigger than this are not directly read but need to be splitted first
bigfile_treshold = 600 # for TDMS files (MB)

# UELOG files bigger than this are ignored because they take too long to read
UELOG_bigfileLimit = 0.5 # (MB)

# root-folder  where all the converted machine-data is stored
source_path = 'X:\\LogData_LK2001'

# root-folder with the raw-machine data copied from the flash-drives
rawData_path = 'X:\\RohData_LK2001'

# root-folder where the Datamining-reports are saved 
target_dir_path = 'X:\\EvaluationData_LK2001\\run'

# resources folder
res_dir_path = 'X:\\EvaluationData_LK2001\\resources'

# file with KPI in resources folder
performances_file = 'PerformanceIndexes.xlsx'

# files with these extensions are considered machine-data files
MD_extensions = ['.csv', '.hwbin', '.phv', '.uelog']

## number of days before the 1st of the current month that the script will analyse
DAYSRANGE = 365-1

# evualuation will go until the last complete month
# if empty uses current date and until end of last month
LASTDATE = '' # YYYY_MM_DD


## Excluded machines. Machines must be a folder insource_path
# and have a number after 'LK2001-'
# @see Datamining.is_valid_machine
excluded_machines = [
    '00x',
    '000','001','002','003','004','005','007',
    '099',
    'MP1','MP2','MP3','MP4','MP5','MP6'
    ]

# Finds the absolute path of the roof folder where the code is
DMroot_fd = os.path.abspath(os.path.dirname(__file__))
while 'src' in DMroot_fd:
    DMroot_fd = os.path.dirname(DMroot_fd)

# create target folder
dm_target = os.path.join(DMroot_fd,'target')
os.makedirs(dm_target,exist_ok=True)

# create logs folder
log_fd = os.path.join(DMroot_fd,'logs')
os.makedirs(log_fd,exist_ok=True)

# save datamining root folder
dm_rootfolder = DMroot_fd

# definition of the system states
SStates = {
        0: 'STARTUP',
        1: 'IDLE',
        2: 'SERVICE',
        3: 'PREPARATION',
        4: 'TREATMENT',
        5: 'POSTPROCESS',
        6: 'DISINFECTION',
        }
SStates.update({v:k for k,v in SStates.items()}) # to work both ways

# File paths
filepath_ErpConnection = "https://sp.hepawash.com/sites/projects/420/Error%20List%20Library/ERP_Connection.xlsx"
filepath_AdvosBIConnection = "https://sp.hepawash.com/sites/projects/420/Error%20List%20Library/AdvosBI_Connection.xlsx"
filepath_listOA = 'https://sp.hepawash.com/sites/projects/403/Shared%20Documents/Action%20Effectiveness/List_of_actions.xlsx'
filepath_GFL = 'https://sp.hepawash.com/sites/projects/312/Shared%20Documents/GFL-Gesamtfehlerliste/Gesamtfehlerliste_Maschinen.xlsx'
filepath_ListOfSystemErrors = 'https://sp.hepawash.com/sites/projects/420/Error%20List%20Library/Error%20List%20Complete%202021_11_29_060008.xlsx'