'''
This file takes care of searching for bit TDMS files in the Server
and splits them using the splitting Tool

split_files() is the main funcion

@author RaHe

'''
#%% imports
import os
import sys
import subprocess
import errno
import pandas as pd
from nptdms import TdmsFile

# imports the function in preprocessing that gathers all files and sizes
sys.path.append('..') # in order to import superior package
from Datamining import is_valid_machine, is_valid_date
import DataPreparation.Preprocessing as Preprocessing
from FileHandling.DMLoggers import log
from Constants import bigfile_treshold, dm_rootfolder, source_path as source

#%% constants
## Number of repetitions for the splitting procedure,
# when it does not work in the first try 
max_split_tries = 10

## Commands and parameters to split the big files
# that will be passed to the tdms-splitter
# @detail a '/' is added before each command 
params = [
    ('f', bigfile_treshold),
    ('n', 10000),
    ('d', ''),
    ('s', '')    
]

# True = only show files to be splitted but do not split them
JUSTEVALUATE = True

#%% init
splitter_folder = os.path.join(*[dm_rootfolder,'src','main','resources','CLI-TDMS-Splitter-3v1'])

#%%
def _verify_splitted(folder):
    ## Opens all splitted files one time and rises an error if not successful
    # @detail: looks for all folders with 'LK2001' and all 'tdms' files inside
    log.debug('Verify %s'%folder)
    
    # go through all subdirs that have 'LK2001' in the name
    LK_subfolders = [s for s in os.listdir(folder) if (
        ('LK2001' in s) & os.path.isdir(os.path.join(folder,s))) ]
    
    if not LK_subfolders:
        log.error('ERROR! No splitted folder found!')
        return False
    
    for subdir in LK_subfolders:
        splitted_dir = os.path.join(folder,subdir)
        # find all tdms files in there
        for file in [f for f in os.listdir(splitted_dir) if f.endswith('.tdms')]:
            file_path = os.path.join(splitted_dir,file)
            
            try:
                TdmsFile(file_path)
            except Exception as e:
                log.error('ERROR opening splitted file:%s %s'%(file_path,e))
                return False
    return True

def _split_big_file(splitter, folder):
    ## splits a big file using the splitter
    # @detail: uses the global defined parameters
    
    global params
    
    # created the command    
    command = splitter
    command += ' '
    command += folder
    
    # add the parameters with slash
    for c,p in params:
        command += ' -%s %s'%(c,p)
        
    log.info(command)
    # takes from 10 to 30min for 600MB to 1.2GB
    
    a = subprocess.call(command)
    #a = ''
    
    #_verify_splitted(folder)
    # *if splitting tool not found exit with error
    log.debug('Finnish splitting %d'%a)
    return a

def is_valid_file(source,machine,filepath):
    ## Decides if the file is to be splitted based on the Datamining-Intervall
    
    date = os.path.split(filepath)[1][:10]
    return is_valid_date(source,machine,date)

def drop_splitted(f_list):
    for big_f,columns in f_list.iterrows():
    # Verify if file path without extension is a folder
        split_folder = os.path.splitext(big_f)[0]
        if os.path.isdir(split_folder):
            f_list = f_list.drop(big_f)
            log.debug("already split %s"%big_f)
        else:
            log.debug('not split %s'%big_f)
    return f_list

def split_files(splitter,source,machine,just_last_year=True,just_evaluate=False):
    ## searches all big files in a machine and split the ones not splitted
    # @param splitter: folder with the TDMS-Splitter.exe that accepts command-line input
    # @param source: folder of the machine data
    # @param machine: the machine from which to split the files
    # @param just_last_year: if only the dates for the next Datamining-run are to be evaluated
    # @param just_evaluate: Do not split files, just evaluate number of files to split
    
    # create the path to the .exe
    sp_exe = os.path.join(splitter,'TDMS-Splitterv3.1.exe')
    
    # verify it it exists
    if not os.path.isfile(sp_exe):
        raise FileNotFoundError(
            errno.ENOENT, os.strerror(errno.ENOENT), sp_exe)
    
    # gets all files for the machine
    log.debug('Get all files for %s'%machine)
    files_df = Preprocessing.get_list_of_all_files(source,machine)
    
    # filter just the big files
    if just_last_year:
        big_files_df = files_df[ [is_valid_file(source,machine,fi) for fi in files_df.index] ]
    else:
        big_files_df = files_df
    big_files_df = big_files_df[big_files_df['file_size'] >= (bigfile_treshold*1024*1024) ]
    big_files_df = big_files_df[big_files_df['extension'] == '.tdms']

    # filter the files that have already been splitted    
    big_files_df = drop_splitted(big_files_df)
    
    # get a list of the folders to split
    folders_to_split = Preprocessing.get_unique_folders_list(machine,big_files_df)
    
    # if this flag is set, return without splitting
    if just_evaluate:
        return len(folders_to_split)
    
    elif len(folders_to_split)>0:
        done = 0
        total_folders = len(folders_to_split)
        
        log.info('Starting to split %s, %d folders'%(machine,total_folders))
        for big_folder in folders_to_split:
            log.debug('Split %s %s'%big_folder)
            fd = os.path.join(*[source,big_folder[0],big_folder[1]])
            
            tries = max_split_tries
            while tries > 0:
                _split_big_file(sp_exe,fd)
            
                if _verify_splitted(fd):
                    log.debug('Split OK: %s'%fd)
                    break
                else:
                    log.error('Split Failed, repeat (%d): %s'%(tries,fd))
                
                tries -= 1
            
            done += 1
            log.debug('%d of %d, %.2f done'%(done,total_folders,done/total_folders))
    
        return done
    return 0

if __name__ == "__main__":
    log.debug('SplitFiles Started')
    
    #source = 'X:\LogData_LK2001'
    
    #sys.path.insert(0, '..')
    #import Datamining
    
    machine_list = sorted([m for m in os.listdir(source) if is_valid_machine(source,m)])
    
    report = {}
    for machine in machine_list:
        nr = split_files(splitter_folder,source,machine,just_last_year=True,just_evaluate=JUSTEVALUATE)
        log.info('Finnish machine %s, %d files'%(machine,nr))
        report[machine] = nr
    
    for m,n in report.items():
        log.debug('{}: {} files'.format(m,n))

    log.debug('SplitFiles Finished, total {}'.format(sum(report.values())))
    