'''
This file is run at the beginning of the mining script

it manages file integrity and creation of 'MachineDay' objects in the server

preprocess_machine() is the interface-function and analyzes one machine each time 
author: RaHe
organization: ADVITOS GmbH
'''
import numpy as np
import pandas as pd
import os
import time
import sys

sys.path.append('..')
import MachineDay
import Datamining
from FileHandling.DMLoggers import log
from Constants import source_path as source

#%% Switches

reanalyze_all = False

#%%
def get_list_of_all_files(source_path,machine):
    ## Goes through all machine-days and returns a DataFrame
    # with 'file_path', 'file_size', 'extension' for all files
    # that match ['.csv', '.tdms', '.uelog', '.phv']
    # OBS. Search for splitted files and ignores the big file
    
    # the list to return
    files_df = pd.DataFrame()
    
    # the machine-data extensions
    extension_list = ['.csv', '.tdms', '.uelog', '.phv']

    # goes through all dates
    folder_machine = os.path.join(source_path,machine)
    for date in [date for date in os.listdir(folder_machine) if os.path.isdir(os.path.join(folder_machine,date))]:
        folder_date = os.path.join(folder_machine,date)
        
        # if there is no obj file it is also added to the list
        obj_file = os.path.join(folder_date,machine+'_'+date+'.obj')
        if not os.path.isfile(obj_file):
            new_row = pd.DataFrame(index=[obj_file])
            new_row['file_size'] = 0
            new_row['extension'] = '.obj'

            files_df = files_df.append(new_row)
            
        
        # goes through all ffiles
        for file in [f for f in os.listdir(folder_date) if os.path.isfile(os.path.join(folder_date,f))]:
            file_path = os.path.join(folder_date,file)
            file_path = os.path.normpath(file_path)
            filename, extension = os.path.splitext(file_path)
            
            # if it is a machine-data file
            if extension in extension_list:
                
                # if its a tdms and there is a folder with same name, use the splitted files
                if extension == '.tdms' and os.path.isdir(filename):
                    for subfile in [f for f in os.listdir(filename) if os.path.isfile(os.path.join(filename,f))]:
                        subfile_path = os.path.join(filename,subfile)
                        subfile_path = os.path.normpath(subfile_path)
                        subextension = os.path.splitext(subfile_path)[1]
                        if subextension in extension_list:
                            new_row = pd.DataFrame(index=[subfile_path])
                            new_row['file_size'] = os.path.getsize(subfile_path)
                            new_row['extension'] = subextension
                            files_df = files_df.append(new_row)       
                # if not splitted, add the file to list
                else:
                    new_row = pd.DataFrame(index=[file_path])
                    new_row['file_size'] = os.path.getsize(file_path)
                    new_row['extension'] = extension

                    files_df = files_df.append(new_row)
            
    return files_df[~files_df.index.duplicated(keep='first')]

def get_analyzed_files_list(source_path,machine):
    ## Gets the list of analyzed files for a machine from the csv file in
    # the machine folder
    table_path = os.path.join(*[source_path,machine,'analyzed_files.csv'])
    
    # if this flag is set, the file with the analyzed files will be deleted
    if reanalyze_all:
        try:
            os.remove(table_path)
        except:
            pass
        
        return pd.DataFrame()

    try:
        analyzed_df = pd.read_csv(table_path,decimal=',',sep='\t',index_col=0) # machine path
    except Exception as e:
        log.error('get_analyzed_files_list: %s'%e)
        analyzed_df = pd.DataFrame()
    
    return analyzed_df[~analyzed_df.index.duplicated(keep='first')]

def get_changed_files_list(source_path,machine):
    ## Return a DataFrame with all the files that are not analysed (new or have changed)
    # @param files_df: List of all machine-data related files (tdms, phv...) of the machine
    # the index is the full path of the file and it has a 'file_size' column with its size
    # @param analyzed_df: list of all already analysed files of the machine
    # the index is the full path of the file and it has a 'file_size' column with its size
    log.info('get_changed_files_list %s'%(machine))
    
    # TODO create Backup of analysed files

    analyzed_df = get_analyzed_files_list(source_path,machine)
    
    # get all files currently on the machine sub-folders
    files_df = get_list_of_all_files(source_path,machine)
    
    filtered = files_df.copy()
    # go through analyzed files
    for analyzed_file,df_row in analyzed_df.iterrows():

        # if the file is in the list of all files    
        if analyzed_file in files_df.index:
            #get their sizes
            analyzed_size = int(df_row['file_size'])
            listed_size = files_df.loc[analyzed_file]['file_size']
            
            # if the size changed
            if analyzed_size != listed_size:
                log.warning('File changed: %s'%analyzed_file)
                # do not remove from list, folder will be re-analyzed
            
            # if did not change, remove from to-analyse DataFrame
            else:
                filtered = filtered.drop(analyzed_file)

        else:
            log.warning('File missing: %s'%analyzed_file)
            
    # TODO if no errors, delete backup
    return filtered

def get_unique_folders_list(machine, to_analyze):
    ## returns a list with all the folders to analyse
    # @param: a Dataframe with files to analyse as index
    # @param machine: machine name to search for in the paths and identify date-folders
    # @return: array with touples (machine, day)
    log.info('get_unique_folders_list %s'%(machine))
    
    machine_day_list = []
    for path in to_analyze.index:
        # splits the file of the file and gets the third and second last
        splited_path = path.split(os.sep)
        
        try:
            # locate the name of the machine in the path
            machine_pos = splited_path.index(machine)
            
            machine_day_list.append(
                (machine,splited_path[machine_pos+1]) )
        except:
            log.error('No %s found in %s'%(machine,path))
        
    # remove duplicates and return
    return pd.Series(machine_day_list).unique()

def save_analyzed(md):
        # add to files list

        list_columns = ['machine_day','file_size']
        list_file = os.path.join(*[
            md.source_path,
            md.machine,
            'analyzed_files.csv'] )
        
        md_file = os.path.normpath(os.path.join(md.folder,
                     md.machine+'_'+md.date+'.obj'))

        try:
            analyzed_table = pd.read_csv(list_file,decimal=',',sep='\t',index_col=0)
        except Exception as e:
            log.warning('analyzed_table: create new table. %s'%e)
            analyzed_table = pd.DataFrame(columns=list_columns)

        for analyzed in md.analyzed:
            new_row = {'machine_day': md_file,
                       #'analyzed_file': analyzed,
                       'file_size' : os.path.getsize(analyzed)
                      }
            df = pd.DataFrame(new_row,index=[os.path.normpath(analyzed)])
            analyzed_table = analyzed_table.append(df)

        analyzed_table.to_csv(list_file,decimal=',',sep='\t')

def analyze_folders(source,md_list):
    ## analyzes all folders in the list
    # @param md_list: list of touples (machine, day)
    # @return number of folders analysed without error
    
    valid_md = [(m,d) for (m,d) in md_list if (
            Datamining.is_valid_machine(source,m) & Datamining.is_valid_date(source,m,d,include_thismonth=True))]
    
    log.debug('analyze_folders - Analyzing %d machine-day folders'%(len(valid_md)))
    analyzed_folders = 0
    progress = 0
    # for all machine-day pairs
    for m,d in valid_md:
        try:
            # Create the machine day
            error_step = 'MD Init'
            machine_day = MachineDay.machineDay(source, m, d)

            # write to file            
            error_step = 'MD write'
            md_file = machine_day.write_machine_day()
            
            # save the analyzed files in the list
            error_step = 'Add to list'
            save_analyzed(machine_day)
            
            analyzed_folders += 1
            
        except Exception as e:
            log.error('Error analyzing %s - %s - %s - %s'%(m,d,error_step,e))
            
        progress += 1
        log.debug('Finish %s %s - %d of %d (%.2f done)'%(m,d,progress,len(valid_md),(progress/len(valid_md))*100))
        
    return analyzed_folders

def preprocess_machine(source_path, machine):
    ## Analyzes the machine-day folders that are not analysed yet
    # 1 - read csv in folder with 'analyzed files' list
    # 2 - get all machine-data related files in the machine sub-folders
    # 3 - compare the two lists for changed or new files
    # 4 - analyze changed or missing
    log.info('Preprocess %s %s'%(source_path,machine)) 
    
    # compare the two lists, find changes
    to_analyze_files = get_changed_files_list(source_path,machine)
    
    # find folders (machine-days) that need to be analysed
    to_analyse_folders = get_unique_folders_list(machine,to_analyze_files)
    
    # analyse all files
    new_folders = analyze_folders(source_path,to_analyse_folders)
    
    log.info('Machine %s finished. %d new folders'%(machine,new_folders))

if __name__ == '__main__':
    log.info("Preprocessing Started")
         
    #source = 'X:\\LogData_LK2001'
    #preprocess_machine(source, 'LK2001-033')
    machine_list = sorted([m for m in os.listdir(source) if Datamining.is_valid_machine(source,m)])
    
    for m in machine_list:
        preprocess_machine(source, m)
   
    log.info("Preprocessing Finished")