#
# Automatic reading and converting of the USB-Sticks that come from service
#
# @author: RaHe
# @organization: ADVITOS GmbH#
#
#

import string, sys, datetime, subprocess
import pandas as pd
from win32api import GetVolumeInformation as volumeInfo

from os import listdir, remove, makedirs
from os.path import join, exists, getsize, isfile, splitext, dirname

from shutil import copyfile

sys.path.append('..')
from Constants import MD_extensions, rawData_path, source_path, filepath_ErpConnection

from FileHandling.DMLoggers import log

from DataPreparation.SplitFiles import splitter_folder, split_files

from ExternalConnections.FileDownload import download_file

#%% Define Folder paths

# drives with this name are scanned for machine-data files
drive_name = 'LK2001'

# the original files will be copied to this location, inside machine-named folders
rawData_path # imported from constants.py

# the converted data will be copied here inside machine/date named folders
source_path # imported from constants.py

# this file in source_path folder is a log of all read files
convertedList_fileName = 'listOfReadFiles.csv'

# List of the HWBIN converters that will be called
converter_s102 =  'C:\\HWBINConverter\\Build\\HWBINConverter_3.9.0_s102.exe'
converter_r = 'C:\\Data\\Convert_hwbin\\HWBINConverter_3.10.0_r.exe'
converter_3_11_TDV = 'C:\\Data\\Convert_hwbin\\HWBINConverter_TDV_3.11.0_r111.exe'
converter_3_11_TDU = 'C:\\Data\\Convert_hwbin\\HWBINConverter_TDU_3.11.0_r000.exe'

converters_list = [converter_s102,converter_r,converter_3_11_TDV,converter_3_11_TDU]

# The parameters used when calling the converter
conv_params = ' -s -t ' + source_path

# selects if the original file should be removed after the conversion
delete_orignal = True

# selects if the tdms files
split_bigfiles = True

#%%

def getPathINfo(filePath):
    # Returns the machine-timestamp-version information from a standard
    # machine-data file
    # @param Absolute path or basename of the file
    # @return: pandas Series:
    # pathInfo['timeStmp'] = timeStmp
    # pathInfo['machine'] = machineSN
    # pathInfo['version'] = version
    # pathInfo['identifier'] = identifier
    # pathInfo['logPath'] = logPath
    # pathInfo['rawPath'] = rawPath
    
    #filePath='X:\\LogData_LK2001\\LK2001-043\2019_09_25\\2019_09_25_16_40_09_LK2001-043_LK2001V3.9.1_r.hwbin'
    base_name = basename(filePath)
    pathInfo = pd.Series()

    try:
        timeStmp, machineStr, version_ident = base_name.split('LK2001')
        timeStmp = datetime.strptime(timeStmp,'%Y_%m_%d_%H_%M_%S_')
        dateStr = timeStmp.strftime(format='%Y_%m_%d')
        machineNr = ''.join([m for m in machineStr if ( m.isdigit() or m in 'MP')])
        version = version_ident[1:version_ident.find('_')]
        identifier = splitext(version_ident[version_ident.find('_'):])[0]
        
        machineSN = 'LK2001-'+machineNr
        logPath = join(*[source_path,machineSN,dateStr,base_name])
        logPath_file, logPath_extension = splitext(logPath)
        if logPath_extension.strip() == '.hwbin':
            logPath = logPath_file+'.tdms'
        
        rawPath = join(*[rawData_path,machineNr,base_name])
        
        pathInfo['timeStmp'] = timeStmp
        pathInfo['date'] = timeStmp.strftime(format='%Y.%m.%d')
        pathInfo['machine'] = machineSN
        pathInfo['version'] = version
        pathInfo['identifier'] = identifier
        pathInfo['logPath'] = logPath
        pathInfo['rawPath'] = rawPath
    except:
        log.warning('Invalid filepath {}'.format(filePath))
    return pathInfo

#%%

def copy_makedir(orig,dest):
    # copies a file to a destination, creating the folder if is not existing
    log.debug('Copy file {} -to-> {} ({} bytes)'.format(orig,dest,getsize(orig)))
    # create folder for the destination
    makedirs(dirname(dest),exist_ok=True)
    
    copyfile(orig,dest)

#%%
def tryGetsize(path):
    # gets the size of a path but returns 0 if it does not exist
    try:
        return getsize(path)
    except OSError:
        return 0
    
#%%
def safe_remove(path_rawOrig, path_rawCopy, path_conv ):
    ## Deletes the original file if there is a copy in the raw data
    # and a corresponding converted file.
    # @detail: also verifies if the converted file size is >= original
    # @return: the deleted file path or 'False' if not deleted
    
    log.debug('Verify if orignal file can be deleted...({})'.format(path_rawOrig))
    if not isfile(path_rawOrig):
        log.warning('Orignal Machine-Data file not found')
        return False
    
    if not isfile(path_rawCopy):
        log.warning('No raw copy. File will not be deleted: {}'.format(
                path_rawCopy))
        return False
    
    if not isfile(path_conv):
        log.warning('No conveterd file. File will not be deleted: {}'.format(
                path_conv))
        return False
    
    #verify if the copy is the same size and the converted is >= size
    orig_filesize = getsize(path_rawOrig)
    copy_filesize = getsize(path_rawCopy)
    conv_filesize = getsize(path_conv)
    log.debug('File sizes: {}={}B, {}={}B, {}={}B'.format(
                path_rawOrig,orig_filesize,
                path_rawCopy,copy_filesize,
                path_conv,conv_filesize))
    
    if (copy_filesize == orig_filesize) and (conv_filesize >= orig_filesize):
        log.debug('Delete file:{}'.format(path_rawOrig))
        remove(path_rawOrig)
        return path_rawOrig
    else:
        log.warning('File will not be deleted, sizes do not match for:{}'.format(path_rawOrig))
        return False


#%%  Get list of all drives with machine data  
if __name__ == '__main__':
    log.info('Running readUSB as MAIN')
    # gets all drives that have the specified name 
    usbDrive_list = ['%s:\\' % d
                        for d in string.ascii_uppercase
                        if (exists('%s:' % d)
                        and volumeInfo('%s:\\' % d)[0] == drive_name)]
    
    log.info('Drives found ({})'.format(usbDrive_list))
    
    #%% Make list of files in the usb-sticks in a pandas DataFrame
    # index: the list of file-paths
    # columns: values from 'getpathinfo' plus the current date (for the log)
    
    files_list = pd.DataFrame()
    for usbDrive in usbDrive_list:
        for file in listdir(usbDrive):
            # for every drive and every file
            f_basename, extension = splitext(file) #file='2019_09_26_19_02_50_LK2001-030_LK2001V3.9.1_r.hwbin'
            
            if extension in MD_extensions:
                # for the valid machine-data files

                filepath_info = getPathINfo(file)
                for info in filepath_info.index:
                    files_list.loc[join(usbDrive,file),info] = filepath_info[info]
                
                files_list['date_converted'] = pd.datetime.now()
                
            elif file not in ['System Volume Information','NewPreTimeStamp.bin']:
                log.warning('found invalid file {} {}'.format(usbDrive,file))
                
    #%% Copy the raw files to the raw folder and to the converted if not hwbin
    log.info('Start copying raw files. Total: {}'.format(len(files_list.index)))
    for rawFile in files_list.index:
        # copy every file to raw_files
        copy_makedir(rawFile,files_list.loc[rawFile,'rawPath'])
        
        # if not a hwbin file copy to converted files too
        if splitext(rawFile)[1].strip() != '.hwbin':
            copy_makedir(rawFile,files_list.loc[rawFile,'logPath'])
    
    #%% Convert HWBIN to TDMS
    log.info('Start Converting HWBIN files')
    
    for conv in converters_list:
        log.debug('Command:{}'.format(conv + conv_params))
        subprocess.call(conv + conv_params)
    log.info('Finish Converting HWBIN files')
    
    #%% Clean USB Sticks
    # deletes the files from the USB sticks IF the raw files were copied 
    # and the converted data is there
    if delete_orignal:
        log.info('Start cleaning USB-Sticks')
        for origFile in files_list.index:
            safe_remove(origFile,
                      files_list.loc[origFile,'rawPath'],
                      files_list.loc[origFile,'logPath'])
    else:
        log.info('Switch delete_orignal==False, do not delete files')
    #%% Appends the list of converted files with date and size in X:/
    log.info('Save list of converted files to {}'.format(join(source_path,convertedList_fileName)))
    files_list['logPath_isfile'] = files_list['logPath'].apply(isfile)
    files_list['rawPath_size'] = files_list['logPath'].apply(tryGetsize)
    files_list.to_csv(join(source_path,convertedList_fileName),
                      sep='\t',decimal=',', mode='a', date_format ='%Y.%m.%d')

    #%% Split Big TDMS files
    if split_bigfiles:
        log.info('Start Splitting big files')
        for split_machine in files_list['machine'].unique():
            split_files(splitter_folder,
                        source_path,
                        split_machine,
                        just_last_year=True,
                        just_evaluate=False
                        )
    #split_files(splitter,source,machine,just_last_year=True,just_evaluate=False)
    else:
        log.info('Switch split_files==False, do not split big files')
    
