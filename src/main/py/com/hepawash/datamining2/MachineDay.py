'''
MachineDay Class
@author: RaHe
@organization: ADVITOS GmbH

Class that gathers all the information from a Machine-Day folder

'''

import pandas as pd

import os
import datetime
import pickle
import multiprocessing
NUMBER_OF_CORES = 4

from Constants import bigfile_treshold, source_path
from Util import removeGermanUTF8Chars
from FileHandling.DMLoggers import log
from FileHandling.TdmsHandler import tdms_channels, read_TDMS

from FileHandling.UelogHandler import read_uelog


def get_file_list(folder,ftype):
    ## Helper function
    # gets all files in folder with externsion
    # @return: list of absoluts path files
    list = []
    for file in os.listdir(folder):
        if file.endswith(ftype):
            list.append(os.path.join(folder,file))
    return list

def getFolderSize(absolute_path):
    ## Adds up the size of all files in a folder
    # that match the defined extensions 
    ext_list = ['.tdms','.phv','.csv','.uelog']
    absolute_files_size = 0
    for file in sorted(os.listdir(absolute_path)):
        extension = os.path.splitext(file)[1]
        if extension in ext_list:
            absolute_files_size = absolute_files_size + os.path.getsize(absolute_path+'/'+file)
    return absolute_files_size/1024/1024

#%%
def get_file_SW_version(folder):
        '''
           Get Software Version from the TDMS File String (The Host Software Version)
        '''
        try:
            for file in sorted(os.listdir(folder)):
                # file = '2018_01_24_15_21_01_LK2001-015_LK2001V3.7.2_r.tdms'
                f_basename, f_ext = os.path.splitext(file)
                if f_ext == '.tdms':
                    return f_basename.split('LK2001V')[1]
        except:
            return False

#%%      
class machineDay:
    ## Class that stores the data from a Machine-Day folder
    
    global tdms_channels
    # class variable defining the TDMS channels to read
    channels = tdms_channels
    
    def __init__(self, source_path,machine, date):
        log.debug('Init %s %s %s'%(source_path,machine,date))
        
        # save the passed parameters
        self.source_path = source_path # path info
        self.machine= machine # machine code-name
        self.date = date # date as a string
        
        # date as columns for filtering
        self.day = self.date[8:10]
        self.month = self.date[5:7]
        self.year = self.date[:4]
        
        # file variables
        self.folder = '/'.join([self.source_path,self.machine,self.date])
        self.files_size = getFolderSize(self.folder) # the total size of the files in the folder to detect changes
        self.softwareVersion = get_file_SW_version(self.folder) #Software Version of the TDMS File Name
        
        self.analyzed = [] # list with analysed files
        
        # Generate DMTDMS and add to list, load header of TDMS 
        self.TDMSdata = pd.DataFrame() #Data of the TDMS loaded as a DataFrame
        self.DMTDMS_list = [] # list of DMTDMS files
        self.TDMSproperties = {} # dictionary with the TDMS header
        self.read_tdms()
        
        # load UELOG data
        self.UELOGdata = pd.DataFrame() # data frame with the uelog information
        self.read_uelog()
        
        # load CSV data
        self.CSVdata = pd.DataFrame() # Dataframe with the csv information
        self.read_csv()
        
        # load PHV data
        self.PHVdata = pd.DataFrame() # Dataframe with the csv information
        self.read_phv()
                
    def read_tdms(self):
        ## Reads the header of the TDMS file and generates the DMTDMS files
        # Also adds files to analysed list and DMTDMS list 
        
        # generate the dmtdms
        log.debug('Generate DMTDMS')

        # creates smaller DMTDMS from the TDMS files
        isDM_TDMS = self.filter_TDMS_to_DMTDMS()
        
        # gather all dmtdms
        if isDM_TDMS:
            log.debug('Load DMTDMS')
            self.TDMSdata = pd.DataFrame()
            dm_folder = os.path.join(self.folder,'DM')
            dmtdms_list = get_file_list(dm_folder, 'dmtdms')
            if dmtdms_list:
                df_allTDMSdata = pd.DataFrame()
                for file in dmtdms_list:
                    with open(os.path.join(dm_folder,file),'rb') as f:
                        # load the dm_tdms object from the server
                        dmtdms = pickle.load(f)
                        # save the properties
                        self.TDMSproperties = dmtdms.properties
                        
                        # save which signals are stored in the MD file
                        self.TDMScolumns = dmtdms.dataframe.columns
                        
                        # the data is merged but not saved in the md because
                        # is too big to be stored
                        df_allTDMSdata = df_allTDMSdata.append(dmtdms.dataframe)
                        
                        # append to list of analysed files and path to the dmtdms
                        self.analyzed.append(dmtdms.file)
                        self.DMTDMS_list.append(file)
                
                self.TDMSmetadata = self.getTDMSmetadata(df_allTDMSdata)
        else:
            log.error('No Tdms found: %s %s'%(self.machine,self.date))
          
        
        return

    def getTDMSmetadata(self,df_tdms):
        ## gets information about the tdmsdata from this day so
        # other mining scripts can decide if the data needs to be loaded
        
        TDMSmetadata = {}
        for stateNr in range(7):
            TDMSmetadata['time_state_{}'.format(stateNr)] = \
            (df_tdms['System_State'] == stateNr).sum()
            
        TDMSmetadata['subsequences'] = df_tdms['Active Subsequence'].unique()
        
        return TDMSmetadata

    def read_uelog(self):
        ## reads uelog files and append to analysed list
        
        log.debug('Load Uelog')
        self.UELOGdata = pd.DataFrame()

        uelog_list = get_file_list(self.folder, 'uelog')
        if uelog_list:
            for file in uelog_list:
                df_uelog = read_uelog(file) 
                self.UELOGdata = self.UELOGdata.append(df_uelog)
                
                self.analyzed.append(file)
        return    
    
    def read_csv(self):
        # gather all csv
        log.debug('Load CSV')
        self.CSVdata = pd.DataFrame()
        csv_list = get_file_list(self.folder, 'csv')
        if csv_list:
            for file in csv_list:
                df_csv = pd.read_csv(file,sep='\t',encoding="iso-8859-1")
                
                if 'Time' not in df_csv.columns:
                    # if this column is missing, the header is wrong
                    try:
                        df_csv = pd.read_csv(file,sep='\t',encoding="iso-8859-1", header=None)
                        df_csv.columns =  self.CSVdata.columns
                        self.CSVdata = self.CSVdata.append(df_csv)
                    except:
                        log.error('CSV file has wrong header:{}'.format(file))
                
                else:
                    self.CSVdata = self.CSVdata.append(df_csv)
                
                self.analyzed.append(file)
        
        return
    
    def read_phv(self):
        # gather all phv
        log.debug('Load PHV')
        phv_list = get_file_list(self.folder, 'phv')
        if phv_list:
            for phv_file in phv_list:
                df = pd.read_csv(phv_file,sep='\t')
                filename = os.path.split(phv_file)[1]
                dtime = datetime.datetime.strptime(filename[:19],'%Y_%m_%d_%H_%M_%S')
                df['Time'] = dtime
        
                self.PHVdata = self.PHVdata.append(df)
            
                # add all phv files as analyzed
                self.analyzed.append(phv_file)
            
        return
    
    def find_tdms(self):
        ## Finds all tdms for a machine-day
        # if a file is splitted, the splitted files are added insted of the big file
        log.debug('%s %s Find-TDMS...'%(self.machine,self.date))
        tdms_list = pd.DataFrame()
        
        # goes through files in the Machine-Day folder
        
        for file in os.listdir(self.folder):
            if file[-5:] == '.tdms':
                # for every tdms create a entry with file as index,
                # file_size and split_folder (empty if not split)
                file_path = os.path.join(self.folder,file)
                split_folder = os.path.splitext(file_path)[0] # path without extension
                
                new_row = pd.DataFrame(index=[file_path])
                new_row['file_size'] = os.path.getsize(file_path)
                new_row['split_folder'] = '' 
                if os.path.isdir(split_folder):
                    new_row['split_folder'] = split_folder 
                
                tdms_list = tdms_list.append(new_row)

        if tdms_list.empty:
            return []
        
        # find all files that are splitted
        for splitted_file,columns in tdms_list[tdms_list['split_folder']!=''].iterrows():
            # remove big file from list
            tdms_list = tdms_list.drop(splitted_file)

            # add all splitted files to list
            sp_fd = columns['split_folder']
            for file in os.listdir(sp_fd):
                if file[-5:] == '.tdms':
                    file_path = os.path.join(sp_fd,file)
                    new_row = pd.DataFrame(index=[file_path])
                    new_row['file_size'] = os.path.getsize(file_path)
                    new_row['split_folder'] = '' 
                    tdms_list = tdms_list.append(new_row)                        
        
        # remove all big files still there with a warning
        big_files = tdms_list[tdms_list['file_size'] > (bigfile_treshold*1024*1024) ]
        for f,column in big_files.iterrows():
            log.error('Big file not splitted %s (%d MB)'%(f,column['file_size']/1024/1024))
            tdms_list = tdms_list.drop(f)
        
        return tdms_list.index.tolist()
        
    def filter_TDMS_to_DMTDMS(self):
        ## generate dmtdms because entire TDMS files
        # are too big to be handled
        # @return Boolean indicating if files were analysed
        log.debug('%s %s filter_TDMS_to_DMTDMS...'%(self.machine,self.date))
        
        erased = self.clean_DM_folder()
        log.debug('Erased %d old files'%erased)
        
        # create a list with files and output path in touple-format
        tdms_list = self.find_tdms()
        output_folder = os.path.join(self.folder,'DM')
        touple_list = [(f,output_folder) for f in tdms_list]

        if tdms_list:
            log.debug('Start parallel processing of {}'.format([x[0] for x in touple_list]))
            pool = multiprocessing.Pool(NUMBER_OF_CORES)
            pool.map(read_TDMS,touple_list)
            pool.terminate()
            log.debug('%d files created'%len(touple_list))
            return True
        else:
            log.error("No TDMS files found")
            return False
        
    def clean_DM_folder(self):
        ## cleans the temporary DM folder
        # return number of erased files
        log.debug('%s %s clean_DM_folder...'%(self.machine,self.date))
        
        dm_folder = os.path.join(self.folder,'DM')
        
        if not os.path.isdir(dm_folder):
            return 0

        erased = 0
        # delete every file
        for f in os.listdir(dm_folder):
            file_path = os.path.join(dm_folder,f)
            try:
                os.remove(file_path)
                erased += 1
            except Exception as e:
                log.error('rmfile: %s'%e)

        try:
            #delete the folder
            os.rmdir(dm_folder)
        except Exception as e:
            log.error('rmdir: %s',e)
            
        return erased
           
    def write_machine_day(self):
        ## Saves the machine day object to its folder
        # and add to the analyzed files list
        log.debug('%s %s write_machine_day...'%(self.machine,self.date))

        # first clean the existing files
        erased = self.clean_machine_day()
        log.debug('%d old file erased'%erased)
        
        mdfile = os.path.normpath(os.path.join(self.folder,
                             self.machine+'_'+self.date+'.obj'))
        
        log.debug('LEN: TDMS %d, CSV %d, PHV %d, UELOG %d'%(
            len(self.TDMSdata),
            len(self.CSVdata),
            len(self.PHVdata),
            len(self.UELOGdata)))
        
        with open(mdfile,'wb') as f:
            pickle.dump(self,f)
            
        log.debug('%s %s Saved to %s'%(self.machine,self.date,mdfile))
        
    def clean_machine_day(self):
        ## cleans the .obj file from the folder, if exists
        # @detail: also cleans temporary 'DM' folder
        # return: number of erased files
        log.debug('%s %s clean obj'%(self.machine,self.date))

        erased = 0
        file_path = os.path.normpath(os.path.join(self.folder,
                             self.machine+'_'+self.date+'.obj'))
        try:
            os.remove(file_path)
            erased = 1
        except:
            pass
        
        return erased
    
    def time_in_state(self,sys_state):
        """
        Return the number of seconds in SystemState State
        if no state then return False
        """
        state_size = self.TDMSdata['System_State'][self.TDMSdata['System_State'] == sys_state].size
        if state_size == 0:
            return False
        else:
            return state_size
        
def load_MD(source,machine,date,load_tdms=True):
    ## Loads a machine day object from the obj file
    file_name = machine+'_'+date+'.obj'
    
    DM_folder = os.path.join(*[source,machine,date,'DM'])
    md_file = os.path.join(*[source,machine,date,file_name])
    max_bytes = 2**20

    try:
        with open(md_file,'rb') as f:
            log.debug('Load %s'%md_file)
            #md = pickle.load(f)
            input_size = os.path.getsize(md_file)
            bytes_in = bytearray(0)
            with open(md_file, 'rb') as f_in:
                for _ in range(0, input_size, max_bytes):
                    bytes_in += f_in.read(max_bytes)
            md = pickle.loads(bytes_in)
                        
            if load_tdms:
                log.debug('Load TDMS')
                md.TDMSdata = pd.DataFrame()
                
                if os.path.isdir(DM_folder):
                    dmtdms_list = get_file_list(DM_folder, 'dmtdms')
                    if dmtdms_list:
                        for file in dmtdms_list:
                            with open(file,'rb') as f:
                                # load the dm_tdms object from the server
                                dmtdms = pickle.load(f)
                                md.TDMSdata = md.TDMSdata.append(dmtdms.dataframe)
                else:
                    log.error('MD not preprocessed - No DM Folder')
                    return False
            else:
                log.debug('Dont load TDMS')
                
                      
    except FileNotFoundError:
        log.warning('{} {} Has not been preprocessed'.format(machine,date))
        return False
    except Exception as e:
        log.error('load_MD - %s'%e,exc_info=True)
        return False
    
    
    log.debug('Finish load %s %s. LEN: TDMS %d, CSV %d, PHV %d, UELOG %d'%(machine,date,
            len(md.TDMSdata),len(md.CSVdata),len(md.PHVdata),len(md.UELOGdata)))
    return md

if __name__ == '__main__':
    
    pass