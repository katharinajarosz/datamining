'''
- Handles the reading and writing of TDMS files
- Defines the channels to read of the tdms file
- Encapsulates pandas dataframe in dm_tdms to include properties
'''
import pickle
import os
import sys
from nptdms import TdmsFile
import pandas as pd

from FileHandling.DMLoggers import log

## this variable stores the channels to read
# it will be set by the main code
tdms_channels =[
            ['ProcessControl','System_State'],['ProcessControl','Startup_State'], ['ProcessControl','Preparation_State'], 
            ['ProcessControl','Treatment_State'], 
            ['ProcessControl','Postprocess_State'],['ProcessControl','Disinfection_State'],
            ['ProcessControl','Service_State'],['ProcessControl','Active Subsequence'],['ProcessControl','Subsequence_State'],
            
            ['Error','ActiveErrors (Double)'],
            ['Error','ActiveErrors (Strings)'],
            
            ['Numeric Inputs','C_PH_BASE_CD'], ['Numeric Inputs','C_PH_ACID_CD'], ['Numeric Inputs','C_PH_RES_CD'],['Numeric Inputs','C_PH_MON_CD'],
            ['Numeric Inputs','C_PH_BASE_R'], ['Numeric Inputs','C_PH_ACID_R'], ['Numeric Inputs','C_PH_RES_R'],['Numeric Inputs','C_PH_MON_R'],
            ['Numeric Inputs','S_P_BLOOD_ART_CD'],['Numeric Inputs','S_P_BLOOD_PRE_CD'],['Numeric Inputs','S_P_BLOOD_VENOUS_CD'],
            ['Numeric Inputs','S_P_DIA_OUT_CD'],['Numeric Inputs','S_P_DIA_IN_CD'],['Numeric Inputs','S_P_HW_ACID_CD'],['Numeric Inputs','S_P_HW_BASE_CD'],
            ['Numeric Inputs','S_F_HW_ACID_CD'],['Numeric Inputs','S_F_HW_BASE_CD'],['Numeric Inputs','S_F_RO_WATER_CD'],['Numeric Inputs','S_F_DIA_CD'],
            ['Numeric Inputs','S_GP_HW_ACID_FB_CD'],['Numeric Inputs','S_GP_HW_BASE_FB_CD'],
            ['Numeric Inputs','S_GP_DIA_FB_CD'],
            ['Numeric Inputs','S_PP_SU_ACID_FB_CD'],['Numeric Inputs','S_PP_SU_BASE_FB_CD'],
            ['Numeric Inputs','S_PP_WA_A_FB_CD'],['Numeric Inputs','S_PP_WA_B_FB_CD'],
            ['Numeric Inputs','S_BLD_CD'], ['Numeric Inputs','S_SCALE_SUM_CD'],['Numeric Inputs','S_SCALE_2_CD'],['Numeric Inputs','S_SCALE_1_CD'],
            ['Numeric Inputs','S_BLOOD_PUMP_FB_CD'],
            ['Numeric Inputs','C_ACID_VOLUME_CD'] , ['Numeric Inputs','C_BASE_VOLUME_CD'],
            
            ['Digital_Outputs','A_V3_CROSS_DEM'],['Digital_Outputs','A_V3_AIRSWITCH_DEM'],
            
            ['Closed Loops','CL_PH_RES_DEM'], ['Closed Loops','CL_SUPPLY_REL_DEM'], 
            ['Closed Loops','CL_SUPPLY_FLOW_E'],['Closed Loops','CL_SUPPLY_FLOW_HMI'], ['Closed Loops','CL_SUPPLY_FLOW_DEM'],
            ['Closed Loops','CL_HEATER_HW_Y'], 
            ['Closed Loops','CL_HEATER_BASE_Y'],
            ['Closed Loops','CL_PERCENTAGE_HEATER_HW_Y'], 
            ['Closed Loops','CL_PERCENTAGE_HEATER_BASE_Y'],
            ['Closed Loops','CL_FLOW_PP_SU_ACID_DEM'],['Closed Loops','CL_FLOW_PP_SU_BASE_DEM'],
            ['Closed Loops','CL_FLOW_PP_SU_ACID_E'],['Closed Loops','CL_FLOW_PP_SU_BASE_E'],
            ['Closed Loops', 'CL_BALANCE_PP_WASTE_Y'],['Closed Loops', 'CL_BALANCE_PP_WASTE_E'],
            ['Closed Loops', 'CL_FLOW_BLOOD_PUMP_DEM'],
            
            ['Digital_Outputs','A_CONT_MOTOR_PWR_DEM'],
            ['Digital Inputs','S_CONT_MOTOR_UP_CD'],
            ['Digital Inputs','S_CONT_MOTOR_DOWN_CD'],
            ['Numeric Inputs','S_CONT_MOTOR_FB_CD'],
            
            ['ProtectiveSystem','Balancing.Scale_Deviation'],
            ['ProtectiveSystem','BLOOD_PUMP.Meas'],
            ['ProtectiveSystem','Sond_Acid.UTemp'], ['ProtectiveSystem','Sond_Reservoir.UTemp'], ['ProtectiveSystem','Sond_Base.UTemp'],['ProtectiveSystem','Sond_Monitor.UTemp'],
            ['ProtectiveSystem','Bld.CD']
            ] 

class dm_tdms():
    ## wrapper of the DataFrame Class
    # to include properties
    def __init__(self,dataframe_,properties_,file_):
        self.dataframe = dataframe_
        self.properties = properties_
        self.file = file_

def read_TDMS(touple_param):
    ## Reads the TDMS file to the Dataframe
    # param: touple in format (TDMS_filename, DMTDMS_outputfolder)
    # OBS: gets only the channels defined in MachineDay.tdms_channels
    # OBS: saves the dataframe as pickle-object
    
    file = touple_param[0]
    dirname = touple_param[1]
    
    log.debug('Read %s'%file)
    if file[-5:] != '.tdms':
        log.error('Type error, file not analysed')
        return 
    
    tdms_file = TdmsFile(file)
    
    df_file = pd.DataFrame()
    
    # Loop through channels
    errors = 0
    for gr,ch in tdms_channels:
        # loop through all needed channels
        try:
            df_file[ch] = tdms_file.channel_data(gr,ch)
        except Exception:
            errors += 1 # count channels not found
            continue
    
    if errors > 0: # log channels not found
        log.error('{} Channels not found in {}'.format(errors,file))
    
    try:
        channel = tdms_file.object('IO','Timestamp')
        df_file['timestamp'] = channel.data
    except:
        log.error('Channel does not exits: Timestamp. {}'.format(file))
        
    try:
        df_file.set_index('timestamp',inplace=True)
    except:
        log.error('Error setting timestamp as index - Data Integrity. {}'.format(file))
    
    # gets the properties of the TDMS
    properties = tdms_file.object().properties
    
    #creates the dm_tdms object with the filtered DF and properties
    dm_tdms_file = dm_tdms(df_file,properties,file)
    
    # save to file
    try:
        os.mkdir(dirname)
    except:
        pass
    
    filename = os.path.basename(file)
    pickefile = os.path.join(dirname,filename[:-5]+'.dmtdms')

    max_bytes = 2**20
    bytes_out = pickle.dumps(dm_tdms_file)
    n_bytes = sys.getsizeof(bytes_out)
    with open(pickefile, 'wb') as f_out:
        for idx in range(0, n_bytes, max_bytes):
            f_out.write(bytes_out[idx:idx+max_bytes])

    log.debug('Finish Read %s'%file)
    