'''
This file manages the loggers of the datamining script
'''

import logging
import sys
import time
import os 

sys.path.append('..')
from Constants import log_fd

fileName = 'datamining'

rootLogger = logging.getLogger('DM')

if not rootLogger.handlers:
    logFormatter = logging.Formatter("%(asctime)s\t%(levelname)s\t%(message)s\t[%(filename)s:%(funcName)s():%(lineno)s]")

    str_day = time.strftime('-%Y-%m-%d')
    fileHandler = logging.FileHandler("{0}/{1}.log".format(log_fd, fileName+str_day))
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)
    
    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)
    
    rootLogger.setLevel(logging.DEBUG)
    rootLogger.propagate = False

def getDMLogger():
    return logging.getLogger('DM')

log = getDMLogger()
