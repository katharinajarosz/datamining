'''
Reads uelog files to df
'''
import pandas as pd
import os

from FileHandling.DMLoggers import log
from Constants import UELOG_bigfileLimit

def read_uelog(file):
    ## Reads one Uelog file to a pandas dataframe
    
    df_uelog = pd.DataFrame()
    
    # if the files is too big, skip
    if (os.path.getsize(file)/1024/1024) > UELOG_bigfileLimit:
        log.error('UELOG file too big: {}'.format(file))
        return df_uelog
    
    with open(file) as uelog_f:
        try:
            for line in uelog_f:
            
                new_row = pd.Series()
    
                line_list = line.split(' ')
                new_row['time'] = line_list[0]
                new_row['date'] = line_list[1]
                new_row['type'] = line_list[2]
                new_row['text'] = ' '.join(line_list[3:])
    
                df_uelog = df_uelog.append(new_row, ignore_index=True)
        except:
            log.error('UELOG read error {}'.format(file))
    return df_uelog


if __name__ == '__main__':
    #
    # This evaluates the size of all UELOG files
    # used to define the limit of the files to read
    
    log.info('START uelog evaluation')
    uelogList = ''
    src = "X:/LogData_LK2001"
    
    # loop to find all UELOG files
    for machine in [m for m in os.listdir(src) if (('LK2001' in m and 'M' not in m))]:
        machineFolder = os.path.join(src,machine)
        
        for date in [d for d in os.listdir(machineFolder) if os.path.isdir(os.path.join(machineFolder,d))]:
            dateFolder = os.path.join(machineFolder,date)
            
            for file in os.listdir(dateFolder):
                if file.endswith(".uelog"):
                    uelogList += ';'+os.path.join(dateFolder, file)
        log.debug(machine)# to show progress
    log.debug('{} UELOG files'.format(len(uelogList.split(';'))) )
    
    # evaluate the size of the files
    sizes = pd.DataFrame()
    cnt = 0; lastPrint = '' # to show progress
    for f in uelogList.split(';'):
        cnt += 1
        if f:
            sizes.loc[f,'size'] = os.path.getsize(f)
        
        if int(cnt / 1000) != lastPrint:
            # to show progress
            lastPrint = int(cnt / 1000)
            log.debug('{}000 files evaluated'.format(lastPrint) )
    
    # show results
    sizesMB = (sizes['size']/1024/1024)
    log.info('Sizes Description:\n{}'.format(sizesMB.describe()) )
    log.info('files > limit of {} MB:\n{}'.format(UELOG_bigfileLimit,sizesMB[sizesMB > UELOG_bigfileLimit]))
