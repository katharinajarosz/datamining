# -*- coding: utf-8 -*-

# File that gathers independent useful functions
#
# @author: Rafael Hellmann
# @copyright: Advitos gmbh
import os

def removeGermanUTF8Chars(uft8String):
    # Replaces special german characters from strings
    #
    # 'ae' for umlaut-a, 'oe' for umlaut-o, etc
    
    if (not uft8String) or (uft8String is None):
        return ''
        
    b = bytes(uft8String, 'utf-8').decode('utf-8','ignore')
    b = b.replace(u'\u00e4',u'ae') #U+00E4 umlaut-a
    b = b.replace(u'\u00f6',u'oe') #U+00F6 umlaut-o 
    b = b.replace(u'\u00fc',u'ue') #U+00FC umlaut-u
    b = b.replace(u'\u00e9',u'e') #U+00E9 akut-e
    b = b.replace(u'\u00df',u'ss') #U+00DF esszett
    return b.encode("utf-8").decode('ascii','ignore')

def getsize_filelist(filelist):
    ## gets the size for a list of files
    # returns NaN if one of the files cannot be evaluated
    total = 0
        
    for f in filelist:
        try:
            total += os.path.getsize(f)
        except:
            return float('nan')
    return total

def try_GetVal(table_,index_, column_val, column_index=''):
    ## Tries to return a velue in a column of a table
    # reuturns float'nan' if not possible
    
    if not column_index:
        try:
            return table_.loc[index_,column_val]
        except:
            return float('nan')
    else:
        try:
            firstEntry = table_[table_[column_index] == index_].iloc[0]
            return firstEntry[column_val]
        except Exception as e:
            return float('nan')
        